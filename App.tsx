import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";
import { StyleSheet } from "react-native";
import { useEffect, useState } from "react";
import * as Font from "expo-font";
import { Login } from "./src/screens/login/Login";
import { EventList } from "./src/screens/event-list/EventList";
import { EventCreate } from "./src/screens/event-create/EventCreate";
import { EventEdit } from "./src/screens/event-edit/EventEdit";
import { EventDeleteDialog } from "./src/screens/event-delete-dialog/EventDeleteDialog";
import { EventCreateCancelDialog } from "./src/screens/event-create-cancel-dialog/EventCreateCancelDialog";
import { EventSaveDialog } from "./src/screens/event-save-dialog/EventSaveDialog";
import { Logout } from "./src/screens/logout/Logout";
import { StatusChangeDialog } from "./src/screens/status-change-dialog/StatusChangeDialog";
import { ReceptionInfoDeleteDialog } from "./src/screens/reception-info-delete-dialog/ReceptionInfoDeleteDialog";
import { ReceptionInfoSaveDialog } from "./src/screens/reception-info-save-dialog/ReceptionInfoSaveDialog";
import { ReceptionInfoEdit } from "./src/screens/reception-info-edit/ReceptionInfoEdit";
import { ReceptionInfoDetail } from "./src/screens/reception-info-detail/ReceptionInfoDetail";
import { EventDetail } from "./src/screens/event-detail/EventDetail";
import { signInWithEmailAndPassword, onAuthStateChanged } from "firebase/auth";
import { userAuth } from "./src/config/firebaseConfig";

const Stack = createNativeStackNavigator();
export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);
  const [user, setUser] = useState<any | null>(null);
  const [initializing, setInitializing] = useState(true);

  useEffect(() => {
    const loadFonts = async () => {
      await Font.loadAsync({
        SpaceMono: require("./src/assets/fonts/SpaceMono-Regular.ttf"),
        HiraginoKaku_GothicPro_Text: require("./src/assets/fonts/Hiragino-Kaku-Gothic-Pro-W3.otf"),
        HiraginoKaku_GothicPro_Text_Bold: require("./src/assets/fonts/Hiragino-Kaku-Gothic-Pro-W6.ttf"),
      });

      setFontLoaded(true);
    };
    loadFonts();

    //FirebaseのsignInWithEmailAndPasswordメソッドを使用し、アプリ起動時に指定ユーザーで自動サインインを行う
    signInWithEmailAndPassword(
      userAuth,
      "city-staff-base@matsusaka.co.jp",
      "Pg37gRm20b58z3D5"
    ).catch((error) => {
      console.log("Auto sign-in failed:", error);
    });

    //ユーザーの認証状態の変更を監視する
    const unsubscribeAuthState = onAuthStateChanged(userAuth, (currentUser) => {
      setUser(currentUser);
      if (initializing) setInitializing(false);
    });

    // コンポーネントのアンマウント時にリスナーを解除
    return unsubscribeAuthState;
  }, []);

  if (!fontLoaded) {
    return null;
  }

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{
          headerShown: false,
          animation: "none",
        }}
      >
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="EventList" component={EventList} />
        <Stack.Screen name="EventDetail" component={EventDetail} />
        <Stack.Screen name="EventEdit" component={EventEdit} />
        <Stack.Screen name="EventCreate" component={EventCreate} />
        <Stack.Screen name="EventDeleteDialog" component={EventDeleteDialog} />
        <Stack.Screen
          name="EventCreatCancelDialog"
          component={EventCreateCancelDialog}
        />
        <Stack.Screen name="EventSaveDialog" component={EventSaveDialog} />
        <Stack.Screen name="Logout" component={Logout} />
        <Stack.Screen
          name="StatusChangeDialog"
          component={StatusChangeDialog}
        />
        <Stack.Screen
          name="ReceptionInfoDeleteDialog"
          component={ReceptionInfoDeleteDialog}
        />
        <Stack.Screen
          name="ReceptionInfoSaveDialog"
          component={ReceptionInfoSaveDialog}
        />
        <Stack.Screen name="ReceptionInfoEdit" component={ReceptionInfoEdit} />
        <Stack.Screen
          name="ReceptionInfoDetail"
          component={ReceptionInfoDetail}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
