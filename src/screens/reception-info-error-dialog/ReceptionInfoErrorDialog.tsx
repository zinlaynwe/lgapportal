import React from 'react';
import { View, Modal, TouchableOpacity, StyleSheet, Pressable } from 'react-native';
import { HiraginoKakuText } from '../../components/StyledText';
import { styles } from './ReceptionInfoErrorDialogStyles';
import { Ionicons } from "@expo/vector-icons";

interface Props {
  onSwitch: () => void;
  onCancel: () => void;
}

const ReceptionInfoErrorDialog: React.FC<Props> = ({ onSwitch, onCancel }) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={true}
    >
      <View style={styles.modalBackground}>
        <View style={styles.modalContainer}>
          <View style={styles.titleContainer}>
            <HiraginoKakuText style={styles.titleText}>
              項目にエラーがあります。{'\n'}
              別の人に切り替える場合は、変更内容を保存できませんが、よろしいですか？
            </HiraginoKakuText>
            <View style={styles.headerButtonContainer}>
              <Pressable
                style={styles.closeButton}
                onPress={onCancel}
              >
                <Ionicons
                  name="close-sharp"
                  size={24}
                  style={styles.closeicon}
                  color="black"
                />
              </Pressable>
            </View>
          </View>
          <View style={styles.messageContainer}>
            <HiraginoKakuText normal style={styles.messageText}>
              項目にエラーがある場合は保存できません。{'\n'}
              別の人に切り替えると、変更内容は破棄されます。
            </HiraginoKakuText>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={[styles.button, styles.cancelButton]}
              onPress={onCancel}
            >
              <HiraginoKakuText style={styles.cancelButtonText}>キャンセル</HiraginoKakuText>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button, styles.acceptButton]}
              onPress={onSwitch}
            >
              <HiraginoKakuText style={styles.buttonText}>切り替える</HiraginoKakuText>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default ReceptionInfoErrorDialog;
