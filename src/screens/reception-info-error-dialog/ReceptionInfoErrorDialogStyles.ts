import { StyleSheet } from "react-native";
import { colors } from "../../styles/color";

export const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalContainer: {
        backgroundColor: '#FFFFFF',
        borderRadius: 12,
        width: 545,
        height: 291,
    },
    titleContainer: {
        width: 545,
        height: 113,
        paddingVertical: 16,
        paddingHorizontal: 24,
        flexDirection: "row",
        justifyContent: "space-between",
    },
    headerButtonContainer: {
        width: 56,
        height: "auto",
        gap: 8,
        backgroundColor: colors.secondary,
        verticalAlign: "middle",
    },
    messageContainer: {
        width: 545,
        height: 102,
        gap: 16,
        paddingVertical: 24,
        paddingHorizontal: 24,
        borderWidth:1,
        borderColor: colors.gray,
    },
    buttonContainer: {
        width: "100%",
        height: 120,
        justifyContent: "space-between",
        flexDirection: 'row',
        paddingVertical: 16,
        paddingHorizontal: 24,
    },
    button: {
        height: 44,
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 4,
        alignItems: "center",
    },
    closeButton: {
        width: 24,
        height: 24,
        alignSelf: "flex-end",
        alignItems: "center",
    },
    acceptButton: {
        backgroundColor: '#346DF4',
    },
    cancelButton: {
        borderWidth: 1,
        borderColor: colors.gray,
    },
    closeicon: {
        height: 24,
    },
    titleText: {
        fontSize: 18,
    },
    messageText: {
        fontSize: 16,
    },
    buttonText: {
        color: '#FFFFFF',
        fontSize: 16,
        textAlign: 'center',
    },
    cancelButtonText: {
        color: "#346DF4",
        fontSize: 16,
    },
});