import React, { useEffect, useState } from "react";
import {
  View,
  Pressable,
  StatusBar,
  SafeAreaView,
  ScrollView,
} from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import { Entypo } from "@expo/vector-icons";
import styles from "./ReceptionInfoDetailStyles";
import { colors } from "../../styles/color";
import { CustomButton } from "../../components/basics/Button";
import { Header } from "../../components/basics/header";
import { MaterialIcons } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { ReceptionInfoDeleteDialog } from "../reception-info-delete-dialog/ReceptionInfoDeleteDialog";
import { ReceptionInfoDetailParams } from "./ReceptionInfoDetailParams";
import {
  fetchEventData,
  fetchMemberSizePerGroup,
  fetchReceptionData,
  insertIntoGroupReceptionHistory,
  insertIntoReceptionHistory,
  updateGroupReceptionFields,
  updateReceptionFields,
} from "./ReceptionInfoDetailService";
import { EventDetailParams } from "../event-detail/EventDetailParams";
import { ReceptionInfoEditParams } from "../reception-info-edit/ReceptionInfoEditParams";
import { format, toZonedTime } from "date-fns-tz";
import { ReceptionInfoDeleteParams } from "../reception-info-delete-dialog/ReceptionInfoDeleteParams";
import { ActivityLogger } from "../../log/ActivityLogger";

type Props = {
  navigation: NavigationProp<any, any>;
};

type Params = {
  receptionInfoDetailParams: ReceptionInfoDetailParams;
};

interface Participant {
  secParticipantLbl: string;
  participantFullName: string;
  participantKanaFullName: string;
  participantDateOfBirth: string;
  participantGender: string;
  participantPostalCode: string;
  participantAddress: string;
  participantRelationship: string;
  participantUserRank: string;
  participantLgapId: string;
  participantReceptionId: number;
  participantMemberId: number;
}

export const ReceptionInfoDetail = ({ navigation }: Props) => {
  const route = useRoute();
  const { receptionInfoDetailParams } = route.params as Params;
  const receptionInfoDeleteParams = new ReceptionInfoDeleteParams();

  const [selectedLayout, setSelectedLayout] = useState("individual");
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
  const [eventName, setEventName] = useState("");
  const [mainParticipantLbl, setMainParticipantLbl] = useState("");

  // PERSON Infos
  const [fullName, setFullName] = useState("");
  const [kanaFullName, setKanaFullName] = useState("");
  const [representative, setRepresentative] = useState("");
  const [acceptedTimeStamp, setAcceptedTimeStamp] = useState("");
  const [dateOfBirth, setDateOfBirth] = useState("");
  const [registeredMethod, setRegisteredMethod] = useState("");
  const [gender, setGender] = useState("");
  const [postalCode, setPostalCode] = useState("");
  const [address, setAddress] = useState("");
  const [relationship, setRelationShip] = useState("");
  const [countOfMember, setCountOfMember] = useState<number>();
  const [userRank, setUserRank] = useState("");
  const [lgapId, setLgapId] = useState("");
  const [receptionId, setReceptionId] = useState("");
  const [memberId, setMemberId] = useState("");
  const [toastMessage, setToastMessage] = useState("");
  const [showToast, setShowToast] = useState(false);
  const [participantData, setParticipantData] = useState<Participant[]>([]);
  const [errMsg, setErrMsg] = useState("");

  // DELETE Dialog
  var [selectedRegisterType, setSelectedRegisterType] = useState("");
  var [selectedRole, setSelectedRole] = useState("");
  var [deletedId, setDeletedId] = useState(0);
  var [deletedName, setDeletedName] = useState("");
  const [deletedNameArray, setDeletedNameArray] = useState<string | string[]>(
    []
  );

  useEffect(() => {
    ActivityLogger.insertInfoLogEntry(receptionInfoDetailParams.user, 'ReceptionInfoDetail', 'useEffect', 'screen open');
  }, []);

  useEffect(() => {
    handleEventDetailData();
    handleCountOfMember();
    handleReceptionDetailData();
    handleMainPersonData();
    handleGroupPersonData();

    // HANDLE Toast Msg
    if(receptionInfoDetailParams.toastMsg){
      setToastMessage(receptionInfoDetailParams.toastMsg);
    }

    if (toastMessage) {
      setShowToast(true);

      const timer = setTimeout(() => {
        setToastMessage("");
        setShowToast(false);
        receptionInfoDetailParams.toastMsg = '';
      }, 3000);

      return () => clearTimeout(timer);
    } 
  }, [countOfMember, showToast, toastMessage,
    receptionInfoDetailParams
  ]);

  // GET Event data
  const handleEventDetailData = async () => {
    if (receptionInfoDetailParams.eventId) {
      const クエリ結果_event = await fetchEventData(
        receptionInfoDetailParams.eventId
      );
      if (クエリ結果_event.data) {
        const eventName = クエリ結果_event.data[0].name;
        setEventName(eventName);
      } else {
        console.log("Error: fetchEventData からデータが返されませんでした");
      }
    } else {
      console.log("Error: イベントIDには値がありません");
    }
  };

  // GET Count of Member
  const handleCountOfMember = async () => {
    if (receptionInfoDetailParams.receptionId) {
      const クエリ結果_size = await fetchMemberSizePerGroup(
        receptionInfoDetailParams.receptionId
      );
      if (クエリ結果_size.data) {
        setCountOfMember(クエリ結果_size.data[0].member_size_per_group);
      } else {
        console.log(
          "Error: fetchMemberSizePerGroup からデータが返されませんでした"
        );
      }
    } else {
      console.log("Error: 受付IDには値がありません");
    }
  };

  // GET Reception data
  const handleReceptionDetailData = async () => {
    if (receptionInfoDetailParams.receptionId) {
      const クエリ結果_reception = await fetchReceptionData(
        receptionInfoDetailParams.receptionId
      );
      if (クエリ結果_reception.data) {
        // For DELETE Dialog
        const names = クエリ結果_reception.data.map(
          (item: any) => `${item.lastname} ${item.firstname}`
        );
        setDeletedNameArray(names);

        const matchedData = クエリ結果_reception.data.find(
          (item: { member_id: number }) =>
            item.member_id === receptionInfoDetailParams.memberId
        );

        if (Object.keys(matchedData).length > 0) {
          // 受付対象
          if (matchedData.member_id == 0) {
            if (countOfMember == 1) {
              setMainParticipantLbl("本人のみ");
            } else {
              setMainParticipantLbl("受付代表");
            }
          } else {
            setMainParticipantLbl("一緒に受付");
          }

          // 氏名
          setFullName(matchedData.lastname + "　" + matchedData.firstname);
          setKanaFullName(
            matchedData.lastname_kana + " " + matchedData.firstname_kana
          );

          // 受付日
          const formatDate = (isoString: any) => {
            const zonedDate = toZonedTime(isoString, "UTC");
            return format(zonedDate, "yyyy/MM/dd HH:mm");
          };
          const formattedTimeStamp = formatDate(matchedData.accepted_timestamp);
          setAcceptedTimeStamp(formattedTimeStamp);

          // 受付方法
          if (matchedData.reception_type_code == 1) {
            if (countOfMember == 1) {
              setRegisteredMethod("アプリ（自己QR）個人受付");
            } else {
              setRegisteredMethod("アプリ（自己QR）同時受付");
            }
          } else if (matchedData.reception_type_code == 2) {
            setRegisteredMethod("手入力");
          } else {
            console.log("Error: 受付タイプコードが無効です。");
          }

          // 受付代表者
          if (matchedData.reception_type_code == 1) {
            if (countOfMember == 1) {
              setRepresentative("");
            } else {
              const representativeData = クエリ結果_reception.data.find(
                (item: { member_id: number }) => item.member_id === 0
              );
              if (representativeData) {
                setRepresentative(
                  representativeData.lastname +
                    " " +
                    representativeData.firstname
                );
              } else {
                setRepresentative("");
                console.log("Error: メンバーID 0の受付代表者はいません。");
              }
            }
          } else if (matchedData.reception_type_code == 2) {
            setRepresentative("");
          } else {
            console.log("Error: 受付タイプコードが無効です。");
          }
        } else {
          console.log("Error: No matching member_id found in the data");
        }
      } else {
        console.log("Error: fetchReceptionData からデータが返されませんでした");
        setErrMsg("受付テーブル からデータが返されませんでした");
      }
    } else {
      console.log("Error: 受付IDには値がありません");
    }
  };

  const handleMainPersonData = async () => {
    if (receptionInfoDetailParams.receptionId) {
      const クエリ結果_reception = await fetchReceptionData(
        receptionInfoDetailParams.receptionId
      );
      if (クエリ結果_reception.data) {
        const matchedData = クエリ結果_reception.data.find(
          (item: { member_id: number }) =>
            item.member_id === receptionInfoDetailParams.memberId
        );

        if (Object.keys(matchedData).length > 0) {
          // 氏名
          setFullName(matchedData.lastname + "　" + matchedData.firstname);
          setKanaFullName(
            matchedData.lastname_kana + " " + matchedData.firstname_kana
          );

          //　誕生日
          const formattedDateOfBirth = format(
            new Date(matchedData.date_of_birth),
            "yyyy/MM/dd"
          );
          setDateOfBirth(formattedDateOfBirth);

          // 性別
          setGender(matchedData.gender_name);

          // 住所
          // CHANGE Postal Code Format
          const firstPart: string = matchedData.postal_code.substring(0, 3);
          const secondPart: string = matchedData.postal_code.substring(3, 7);
          const formattedPostalCode: string = `${firstPart}-${secondPart}`;
          setPostalCode(formattedPostalCode);
          setAddress(matchedData.address);

          //　関係
          if (matchedData.member_id == 0) {
            if (countOfMember == 1) {
              setRelationShip("");
            } else {
              setRelationShip(matchedData.relationship);
            }
          } else {
            setRelationShip(matchedData.relationship);
          }

          // 本人確認
          if (matchedData.user_rank == null) {
            setUserRank("");
          } else if (matchedData.user_rank == 4) {
            setUserRank("済み");
          } else {
            setUserRank("未");
          }

          // LGaP_ID
          if (matchedData.lgap_id == null) {
            setLgapId("");
          } else {
            setLgapId(matchedData.lgap_id);
          }

          // その他
          setReceptionId(matchedData.reception_id);
          setMemberId(matchedData.member_id);
        } else {
          console.log("Error: No matching member_id found in the data");
        }
      } else {
        console.log("Error: fetchReceptionData からデータが返されませんでした");
        setErrMsg("受付テーブル からデータが返されませんでした");
      }
    } else {
      console.log("Error: 受付IDには値がありません");
    }
  };

  // [同時受付]タブに表示するデータ
  const handleGroupPersonData = async () => {
    if (receptionInfoDetailParams.receptionId) {
      const クエリ結果_reception = await fetchReceptionData(
        receptionInfoDetailParams.receptionId
      );
      if (クエリ結果_reception.data) {
        const nonMatchedData = クエリ結果_reception.data.filter(
          (item: { member_id: number }) =>
            item.member_id !== receptionInfoDetailParams.memberId
        );

        if (nonMatchedData.length > 0) {
          const participantData = nonMatchedData.map((item: any) => {
            const formattedDateOfBirth = format(
              new Date(item.date_of_birth),
              "yyyy/MM/dd"
            );
            const firstPart = item.postal_code.substring(0, 3);
            const secondPart = item.postal_code.substring(3, 7);
            const formattedPostalCode = `${firstPart}-${secondPart}`;
            return {
              secParticipantLbl:
                item.member_id === 0 && countOfMember === 1
                  ? ""
                  : item.member_id === 0
                  ? "受付代表"
                  : "一緒に受付",
              participantFullName: item.lastname + " " + item.firstname,
              participantKanaFullName:
                item.lastname_kana + " " + item.firstname_kana,
              participantDateOfBirth: formattedDateOfBirth,
              participantGender: item.gender_name,
              participantPostalCode: formattedPostalCode,
              participantAddress: item.address,
              participantRelationship:
                item.member_id === 0 && countOfMember === 1
                  ? ""
                  : item.relationship,
              participantUserRank:
                item.user_rank === null
                  ? ""
                  : item.user_rank === 4
                  ? "済み"
                  : "未",
              participantLgapId: item.lgap_id === null ? "" : item.lgap_id,
              participantReceptionId: item.reception_id,
              participantMemberId: item.member_id,
            } as Participant;
          });

          setParticipantData(participantData);
        } else {
          console.log("Error: All member_id matched the given memberId");
        }
      } else {
        console.log("Error: fetchReceptionData からデータが返されませんでした");
        setErrMsg("受付テーブル からデータが返されませんでした");
      }
    } else {
      console.log("Error: 受付IDには値がありません");
    }
  };

  const handleReceptionInfoEdit = (receptionId: number, memberId: number) => {
    const receptionInfoEditParams = new ReceptionInfoEditParams();
    receptionInfoEditParams.user = receptionInfoDetailParams.user;
    receptionInfoEditParams.eventId = receptionInfoDetailParams.eventId;
    receptionInfoEditParams.receptionId = receptionId;
    receptionInfoEditParams.memberId = memberId;
    receptionInfoEditParams.pgType = receptionInfoDetailParams.pgType;
    receptionInfoEditParams.eventDetailMemberId = receptionInfoDetailParams.memberId;

    if (countOfMember && countOfMember > 1) {
      receptionInfoEditParams.displayMode = 'group';
    } else {
      receptionInfoEditParams.displayMode = 'single';
    }

    navigation.navigate("ReceptionInfoEdit", {
      receptionInfoEditParams,
    });
    ActivityLogger.insertInfoLogEntry(receptionInfoDetailParams.user, 'ReceptionInfoDetail', 'handleReceptionInfoEdit', 'transition', 'ReceptionInfoEdit', receptionInfoEditParams);
  };

  const handleBacktoEventDetail = () => {
    const eventDetailParams = new EventDetailParams();
    eventDetailParams.user = receptionInfoDetailParams.user;
    eventDetailParams.eventId = receptionInfoDetailParams.eventId;
    eventDetailParams.receptionId = receptionInfoDetailParams.receptionId;

    navigation.navigate("EventDetail", {
      eventDetailParams,
    });
    ActivityLogger.insertInfoLogEntry(receptionInfoDetailParams.user, 'ReceptionInfoDetail', 'handleBacktoEventDetail', 'transition', 'EventDetail', eventDetailParams);
  };

  /// delete Dialog
  const handleReceptionInfoDeleteIcon = (
    rolename: string,
    id: number,
    name: string
  ) => {
    setDeletedId(id);
    setDeletedName(name);

    if (id == 0 && countOfMember && countOfMember > 1) {
      receptionInfoDeleteParams.deleteMode = "group";
    } else {
      receptionInfoDeleteParams.deleteMode = "single";
    }

    if (receptionInfoDeleteParams.deleteMode == "single") {
      setDeletedNameArray(name);
      setIsDeleteModalVisible(true);
      setSelectedRegisterType("individual");
      setSelectedRole(rolename);
    } else {
      setIsDeleteModalVisible(true);
      setSelectedRegisterType("group");
      setSelectedRole(rolename);
    }
  };

  const handleDeleteCancelButton = () => {
    setIsDeleteModalVisible(false);
    navigation.navigate("ReceptionInfoDetail", {
      receptionInfoDetailParams,
    });
    ActivityLogger.insertInfoLogEntry(receptionInfoDetailParams.user, 'ReceptionInfoDetail', 'handleDeleteCancelButton', 'transition', 'ReceptionInfoDetail', receptionInfoDetailParams);
  };

  const handleDeleteButton = async (id: number, name: string) => {
    if (id == 0 && countOfMember && countOfMember > 1) {
      receptionInfoDeleteParams.deleteMode = "group";
    } else {
      receptionInfoDeleteParams.deleteMode = "single";
    }
    setIsDeleteModalVisible(false);

    // INSERT & UPDATE
    if (receptionInfoDeleteParams.deleteMode == "single") {
      await insertIntoReceptionHistory(
        receptionInfoDetailParams.receptionId,
        id
      );
      await updateReceptionFields(
        receptionInfoDetailParams.receptionId,
        id,
        receptionInfoDetailParams.user.userId
      );
    } else {
      await insertIntoGroupReceptionHistory(
        receptionInfoDetailParams.receptionId
      );
      await updateGroupReceptionFields(
        receptionInfoDetailParams.receptionId,
        receptionInfoDetailParams.user.userId
      );
    }

    // HANDLE Toast Message
    const eventDetailParams = new EventDetailParams();
    if (id == receptionInfoDetailParams.memberId) {
      if (id == 0) {
        eventDetailParams.statusToastMsg = `${name}と同時に受け付けた人を削除しました`;
      } else {
        eventDetailParams.statusToastMsg = `${name}を削除しました`;
      }
      eventDetailParams.user = receptionInfoDetailParams.user;
      eventDetailParams.eventId = receptionInfoDetailParams.eventId;

      navigation.navigate("EventDetail", {
        eventDetailParams,
      });
      ActivityLogger.insertInfoLogEntry(receptionInfoDetailParams.user, 'ReceptionInfoDetail', 'handleDeleteButton', 'transition', 'EventDetail', eventDetailParams);
    } else {
      if (id == 0) {
        eventDetailParams.statusToastMsg = `${name}と同時に受け付けた人を削除しました`;
        eventDetailParams.user = receptionInfoDetailParams.user;
        eventDetailParams.eventId = receptionInfoDetailParams.eventId;

        navigation.navigate("EventDetail", {
          eventDetailParams,
        });
        ActivityLogger.insertInfoLogEntry(receptionInfoDetailParams.user, 'ReceptionInfoDetail', 'handleDeleteButton', 'transition', 'EventDetail', eventDetailParams);
      } else {
        setToastMessage(`${name}を削除しました`);
      }
    }
  };

  // BadgeStyles
  const getBadgeStyle = (label: string) => {
    switch (label) {
      case "一緒に受付":
        return styles.badgeLabel;
      case "受付代表":
        return styles.badgeLabelRepresentative;
      case "本人のみ":
        return styles.badgeLabelOne;
      default:
        return styles.badgeLabel;
    }
  };

  var handleTabClick = (layout: string) => {
    setSelectedLayout(layout);
  };

  const renderLayout = () => {
    if (selectedLayout === "individual") {
      return (
        <View style={styles.bodyContainer}>
          <View style={styles.innerBodyContainer}>
            <View style={styles.innerTitleContainer}>
              <View style={styles.innerTitleTextContainer}>
                <View style={countOfMember == 1 ? styles.innerTitle : null}>
                  <HiraginoKakuText style={styles.innerTitleText}>
                    個人情報
                  </HiraginoKakuText>
                </View>
              </View>

              <View style={styles.btnGroup}>
                <CustomButton
                  text=""
                  style={styles.actionBtn}
                  type="ButtonMGray"
                  icon={
                    <MaterialIcons
                      name="mode-edit"
                      size={24}
                      color="black"
                      style={styles.actionIcon}
                    />
                  }
                  iconPosition="center"
                  onPress={() =>
                    handleReceptionInfoEdit(+receptionId, +memberId)
                  }
                ></CustomButton>
                <CustomButton
                  text=""
                  style={styles.actionBtn}
                  type="ButtonMGray"
                  icon={
                    <MaterialIcons
                      name="delete"
                      size={24}
                      color="black"
                      style={styles.actionIcon}
                    />
                  }
                  iconPosition="center"
                  onPress={() =>
                    handleReceptionInfoDeleteIcon(
                      mainParticipantLbl,
                      +memberId,
                      fullName
                    )
                  }
                ></CustomButton>
              </View>
            </View>
            <View style={styles.line}></View>
            <View style={styles.personalRow}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  お名前
                </HiraginoKakuText>
              </View>
              <View style={styles.second}>
                <HiraginoKakuText style={styles.secondText} normal>
                  {fullName}
                </HiraginoKakuText>
              </View>
            </View>
            <View style={styles.personalRow}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  お名前（カナ）
                </HiraginoKakuText>
              </View>
              <View style={styles.second}>
                <HiraginoKakuText style={styles.secondText} normal>
                  {kanaFullName}
                </HiraginoKakuText>
              </View>
            </View>
            <View style={styles.personalRow}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  生年月日
                </HiraginoKakuText>
              </View>
              <View style={styles.second}>
                <HiraginoKakuText style={styles.secondText} normal>
                  {dateOfBirth}
                </HiraginoKakuText>
              </View>
            </View>
            <View style={styles.personalRow}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  性別
                </HiraginoKakuText>
              </View>
              <View style={styles.second}>
                <HiraginoKakuText style={styles.secondText} normal>
                  {gender}
                </HiraginoKakuText>
              </View>
            </View>
            <View style={styles.personalRowAddress}>
              <View style={styles.first}>
                <HiraginoKakuText style={styles.firstText}>
                  住所
                </HiraginoKakuText>
              </View>
              <View style={styles.secondAddress}>
                <HiraginoKakuText style={styles.secondText} normal>
                  {postalCode}
                </HiraginoKakuText>
                <HiraginoKakuText style={styles.secondText} normal>
                  {address}
                </HiraginoKakuText>
              </View>
            </View>
            {countOfMember && countOfMember > 1 && (
              <View style={styles.personalRow}>
                <View style={styles.first}>
                  <HiraginoKakuText style={styles.firstText}>
                    代表者との関係
                  </HiraginoKakuText>
                </View>
                <View style={styles.second}>
                  <HiraginoKakuText style={styles.secondText} normal>
                    {relationship}
                  </HiraginoKakuText>
                </View>
              </View>
            )}

            {(userRank.length > 0 || lgapId.length > 0) && (
              <View style={styles.line} />
            )}

            {userRank.length > 0 && (
              <View style={styles.personalRow}>
                <View style={styles.first}>
                  <HiraginoKakuText style={styles.firstText}>
                    本人確認
                  </HiraginoKakuText>
                </View>
                <View style={styles.second}>
                  <HiraginoKakuText style={styles.secondText} normal>
                    {userRank}
                  </HiraginoKakuText>
                </View>
              </View>
            )}

            {lgapId.length > 0 && (
              <View style={styles.personalRow}>
                <View style={styles.first}>
                  <HiraginoKakuText style={styles.firstText}>
                    LGaP_ID
                  </HiraginoKakuText>
                </View>
                <View style={styles.second}>
                  <HiraginoKakuText style={styles.secondText} normal>
                    {lgapId}
                  </HiraginoKakuText>
                </View>
              </View>
            )}
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.bodyContainer}>
          {countOfMember &&
            countOfMember > 1 &&
            participantData.map((data, index) => (
              <View key={index} style={styles.innerBodyContainer}>
                <View style={styles.innerTitleContainer}>
                  <View style={styles.innerTitle}>
                    <View style={getBadgeStyle(data.secParticipantLbl)}>
                      <HiraginoKakuText style={styles.badgeLabelText}>
                        {data.secParticipantLbl}
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.innerTitleTextContainer}>
                      <HiraginoKakuText style={styles.innerTitleText}>
                        個人情報
                      </HiraginoKakuText>
                    </View>
                  </View>

                  <View style={styles.btnGroup}>
                    <CustomButton
                      text=""
                      style={styles.actionBtn}
                      type="ButtonMGray"
                      icon={
                        <MaterialIcons
                          name="mode-edit"
                          size={24}
                          color="black"
                          style={styles.actionIcon}
                        />
                      }
                      iconPosition="center"
                      onPress={() =>
                        handleReceptionInfoEdit(
                          data.participantReceptionId,
                          data.participantMemberId
                        )
                      }
                    />
                    <CustomButton
                      text=""
                      style={styles.actionBtn}
                      type="ButtonMGray"
                      icon={
                        <MaterialIcons
                          name="delete"
                          size={24}
                          color="black"
                          style={styles.actionIcon}
                        />
                      }
                      iconPosition="center"
                      onPress={() =>
                        handleReceptionInfoDeleteIcon(
                          data.secParticipantLbl,
                          data.participantMemberId,
                          data.participantFullName
                        )
                      }
                    />
                  </View>
                </View>
                <View style={styles.line} />
                <View style={styles.personalRow}>
                  <View style={styles.first}>
                    <HiraginoKakuText style={styles.firstText}>
                      お名前
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.second}>
                    <HiraginoKakuText style={styles.secondText} normal>
                      {data.participantFullName}
                    </HiraginoKakuText>
                  </View>
                </View>
                <View style={styles.personalRow}>
                  <View style={styles.first}>
                    <HiraginoKakuText style={styles.firstText}>
                      お名前（カナ）
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.second}>
                    <HiraginoKakuText style={styles.secondText} normal>
                      {data.participantKanaFullName}
                    </HiraginoKakuText>
                  </View>
                </View>
                <View style={styles.personalRow}>
                  <View style={styles.first}>
                    <HiraginoKakuText style={styles.firstText}>
                      生年月日
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.second}>
                    <HiraginoKakuText style={styles.secondText} normal>
                      {data.participantDateOfBirth}
                    </HiraginoKakuText>
                  </View>
                </View>
                <View style={styles.personalRow}>
                  <View style={styles.first}>
                    <HiraginoKakuText style={styles.firstText}>
                      性別
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.second}>
                    <HiraginoKakuText style={styles.secondText} normal>
                      {data.participantGender}
                    </HiraginoKakuText>
                  </View>
                </View>
                <View style={styles.personalRowAddress}>
                  <View style={styles.first}>
                    <HiraginoKakuText style={styles.firstText}>
                      住所
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.secondAddress}>
                    <HiraginoKakuText style={[styles.secondText]} normal>
                      {data.participantPostalCode}
                    </HiraginoKakuText>
                    <HiraginoKakuText style={[styles.secondText]} normal>
                      {data.participantAddress}
                    </HiraginoKakuText>
                  </View>
                </View>
                {countOfMember && countOfMember > 1 ? (
                  <View style={styles.personalRow}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        代表者との関係
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        {data.participantRelationship}
                      </HiraginoKakuText>
                    </View>
                  </View>
                ) : null}
                {(data.participantUserRank.length > 0 ||
                  data.participantLgapId.length > 0) && (
                  <View style={styles.line} />
                )}

                {data.participantUserRank.length > 0 && (
                  <View style={styles.personalRow}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        本人確認
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        {data.participantUserRank}
                      </HiraginoKakuText>
                    </View>
                  </View>
                )}

                {data.participantLgapId.length > 0 && (
                  <View style={styles.personalRow}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        LGaP_ID
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        {data.participantLgapId}
                      </HiraginoKakuText>
                    </View>
                  </View>
                )}
              </View>
            ))}
        </View>
      );
    }
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        titleName="もどる"
        middleTitleName={fullName}
        buttonName=""
        hasButton={false}
        icon={<Entypo name="chevron-left" size={24} color={colors.primary} />}
        iconPosition="front"
        onPressLeft={handleBacktoEventDetail}
      ></Header>
      <ScrollView>
        <View style={styles.overallContainer}>
          {countOfMember == 1 ? (
            <View style={[styles.topbodyContainerOne]}>
              <View style={styles.topinnerContainerCover}>
                <View style={styles.topinnerContainer}>
                  <View style={styles.bigIconContainer}>
                    <Ionicons
                      name="person"
                      color="white"
                      style={styles.bigIcon}
                      size={40}
                    />
                  </View>
                  <View style={styles.topInnerRightInfo}>
                    <View style={getBadgeStyle(mainParticipantLbl)}>
                      <HiraginoKakuText style={styles.badgeLabelText}>
                        {mainParticipantLbl}
                      </HiraginoKakuText>
                    </View>

                    <View style={styles.nameInfo}>
                      <HiraginoKakuText style={styles.nameInfoText}>
                        {fullName}
                      </HiraginoKakuText>
                    </View>
                  </View>
                </View>

                <View style={styles.topLine} />

                <View style={styles.bottominnerContainer}>
                  <View style={styles.row}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        イベント名
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        {eventName}
                      </HiraginoKakuText>
                    </View>
                  </View>

                  <View style={styles.row}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        受付日
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        {acceptedTimeStamp}
                      </HiraginoKakuText>
                    </View>
                  </View>

                  <View style={styles.row}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        受付方法
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        {registeredMethod}
                      </HiraginoKakuText>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          ) : (
            <View style={[styles.topbodyContainer]}>
              <View style={styles.topinnerContainerCover}>
                <View style={styles.topinnerContainer}>
                  <View style={styles.bigIconContainer}>
                    <Ionicons
                      name="person"
                      color="white"
                      style={styles.bigIcon}
                      size={40}
                    />
                  </View>
                  <View style={styles.topInnerRightInfo}>
                    {countOfMember == 1 ? (
                      <View style={getBadgeStyle(mainParticipantLbl)}>
                        <HiraginoKakuText style={styles.badgeLabelText}>
                          {mainParticipantLbl}
                        </HiraginoKakuText>
                      </View>
                    ) : (
                      <View style={getBadgeStyle(mainParticipantLbl)}>
                        <HiraginoKakuText style={styles.badgeLabelText}>
                          {mainParticipantLbl}
                        </HiraginoKakuText>
                      </View>
                    )}

                    <View style={styles.nameInfo}>
                      {countOfMember == 1 ? (
                        <HiraginoKakuText style={styles.nameInfoText}>
                          {fullName}
                        </HiraginoKakuText>
                      ) : (
                        <HiraginoKakuText style={styles.nameInfoText}>
                          {fullName}
                        </HiraginoKakuText>
                      )}
                    </View>
                  </View>
                </View>

                <View style={styles.topLine} />

                <View style={styles.bottominnerContainer}>
                  <View style={styles.row}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        イベント名
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        {eventName}
                      </HiraginoKakuText>
                    </View>
                  </View>

                  <View style={styles.row}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        受付日
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        {acceptedTimeStamp}
                      </HiraginoKakuText>
                    </View>
                  </View>

                  <View style={styles.row}>
                    <View style={styles.first}>
                      <HiraginoKakuText style={styles.firstText}>
                        受付方法
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.second}>
                      <HiraginoKakuText style={styles.secondText} normal>
                        {registeredMethod}
                      </HiraginoKakuText>
                    </View>
                  </View>

                  {typeof representative == "string" &&
                    representative.length > 0 && (
                      <View style={styles.rowLink}>
                        <View style={styles.first}>
                          <HiraginoKakuText style={styles.firstText}>
                            受付代表者
                          </HiraginoKakuText>
                        </View>
                        <View style={styles.secondLink}>
                          <HiraginoKakuText
                            style={[styles.secondLinkText, styles.linkText]}
                            normal
                          >
                            {representative}
                          </HiraginoKakuText>
                        </View>
                      </View>
                    )}
                </View>
              </View>
              {countOfMember && countOfMember > 1 && (
                <View style={styles.separateTab}>
                  <Pressable onPress={() => handleTabClick("individual")}>
                    {selectedLayout === "individual" ? (
                      <View style={styles.tabActive}>
                        <HiraginoKakuText style={styles.tabTextActive}>
                          受付情報
                        </HiraginoKakuText>
                      </View>
                    ) : (
                      <View style={styles.tab}>
                        <HiraginoKakuText style={styles.tabText} normal>
                          受付情報
                        </HiraginoKakuText>
                      </View>
                    )}
                  </Pressable>

                  <Pressable onPress={() => handleTabClick("group")}>
                    {selectedLayout === "group" ? (
                      <View style={styles.tabActive}>
                        <HiraginoKakuText style={styles.tabTextActive}>
                          同時受付
                        </HiraginoKakuText>
                      </View>
                    ) : (
                      <View style={styles.tab}>
                        <HiraginoKakuText style={styles.tabText} normal>
                          同時受付
                        </HiraginoKakuText>
                      </View>
                    )}
                  </Pressable>
                </View>
              )}
            </View>
          )}
          {errMsg ? (
            <View style={styles.errMsgBox}>
              <HiraginoKakuText>{errMsg}</HiraginoKakuText>
            </View>
          ) : (
            renderLayout()
          )}
        </View>
        {isDeleteModalVisible && (
          <ReceptionInfoDeleteDialog
            onCancelButtonPress={handleDeleteCancelButton}
            onDeleteButtonPress={() =>
              handleDeleteButton(deletedId, deletedName)
            }
            registerType={selectedRegisterType}
            roleName={selectedRole}
            personNames={deletedNameArray}
          />
        )}
      </ScrollView>      
      {showToast && (
          <View style={styles.modalBackground}>
            <View style={styles.toastMessage}>
              <HiraginoKakuText style={styles.toastText}>
                {toastMessage}
              </HiraginoKakuText>
            </View>
          </View>
        )}
    </SafeAreaView>
  );
};
