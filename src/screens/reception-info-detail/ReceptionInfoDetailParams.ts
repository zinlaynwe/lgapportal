import { User } from "../../model/User";

export class ReceptionInfoDetailParams implements Params {
  private _user: User = new User();
  private _eventId: number = 0;
  private _receptionId: number = 0;
  private _memberId: number = 0;
  private _pgType: string = "";
  private _toastMsg: string = "";

  get user(): User {
    return this._user.clone();
  }

  set user(value: User) {
    this._user = value;
  }

  get eventId(): number {
    return this._eventId;
  }

  set eventId(value: number) {
    this._eventId = value;
  }

  get receptionId(): number {
    return this._receptionId;
  }

  set receptionId(value: number) {
    this._receptionId = value;
  }

  get memberId(): number {
    return this._memberId;
  }

  set memberId(value: number) {
    this._memberId = value;
  }

  get pgType(): string {
    return this._pgType;
  }

  set pgType(value: string) {
    this._pgType = value;
  }

  get toastMsg(): string {
    return this._toastMsg;
  }

  set toastMsg(value: string) {
    this._toastMsg = value;
  }

  getAllValuesAsString(): string {
    let paramsString = '[ClassName=ReceptionInfoDetailParams';
    paramsString += ',_user=' + this._user.getAllValuesAsString();
    paramsString += ',_eventId=' + this._eventId;
    paramsString += ',_receptionId=' + this._receptionId;
    paramsString += ',_memberId=' + this._memberId;
    paramsString += ',_pgType=' + this._pgType;
    paramsString += ',_toastMsg=' + this._toastMsg;
    paramsString += ']';
    return paramsString
  }
}
