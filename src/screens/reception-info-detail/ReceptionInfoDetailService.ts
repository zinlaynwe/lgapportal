import { executeQuery } from "../../aws/db/dbOperation";
import { getCurrentJapanTime } from "../../environments/TimeUtils";
import { ActivityLogger } from "../../log/ActivityLogger";
import { User } from "../../model/User";

export const fetchEventData = async (eventid: number) => {
  const method = "POST";
  const queryString = `
        SELECT
            name,
            start_date,
            end_date,
            event_status_code
        FROM
            event
        WHERE
            city_code ='242152'
            AND event_id = ${eventid}
        `;
  ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoDetail', 'fetchEventData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const fetchReceptionData = async (receptionId: number) => {
  const method = "POST";
  const queryString = `
      SELECT
        a.reception_id,
        a.member_id,
        a.accepted_timestamp,
        a.lgap_id,
        a.user_rank,
        a.lastname,
        a.firstname,
        a.lastname_kana,
        a.firstname_kana,
        a.date_of_birth,
        a.gender_code,
        a.postal_code,
        a.address,
        a.relationship,
        a.reception_type_code,
        b.name AS gender_name
      FROM
        reception a
        LEFT JOIN gender b ON a.gender_code = b.gender_code
      WHERE
        city_code = '242152'
        AND reception_id = ${receptionId}
        AND NOT is_deleted
      ORDER BY
        family_order_number ASC;
    `;
  ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoDetail', 'fetchReceptionData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const fetchMemberSizePerGroup = async (receptionId: number) => {
  const method = "POST";
  const queryString = `
      SELECT
        COUNT(*) AS member_size_per_group
      FROM
        reception
      WHERE
        city_code = '242152'
        AND reception_id = ${receptionId};
    `;
  ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoDetail', 'fetchMemberSizePerGroup', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const insertIntoReceptionHistory = async (receptionId: number, memberId: number) => {
  const method = "POST";
  const queryString = `
      INSERT INTO
        reception_history
      SELECT
        *
      FROM
        reception
      WHERE
        city_code = '242152'
        AND reception_id = ${receptionId}
        AND member_id = ${memberId};
    `;
  ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoDetail', 'insertIntoReceptionHistory', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const updateReceptionFields = async (receptionId: number, memberId: number, modifierId: string) => {
  const currentJapanTime = getCurrentJapanTime();
  const method = "POST";
  const queryString = `
      UPDATE
        reception
      SET
        history_number = history_number + 1,
        modifier_id = '${modifierId}',
        modification_timestamp = '${currentJapanTime}',
        is_deleted = true
      WHERE
        city_code = '242152'
        AND reception_id = ${receptionId}
        AND member_id = ${memberId};
    `;

  ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoDetail', 'updateReceptionFields', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const insertIntoGroupReceptionHistory = async (receptionId: number) => {
  const method = "POST";
  const queryString = `
    INSERT INTO
      reception_history
    SELECT
      *
    FROM
      reception
    WHERE
      city_code = '242152'
      AND reception_id = ${receptionId};
  `;
  ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoDetail', 'insertIntoGroupReceptionHistory', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const updateGroupReceptionFields = async (receptionId: number, modifierId: string) => {
  const currentJapanTime = getCurrentJapanTime();
  const method = "POST";
  const queryString = `
      UPDATE
        reception
      SET
        history_number = history_number + 1,
        modifier_id = '${modifierId}',
        modification_timestamp = '${currentJapanTime}',
        is_deleted = true
      WHERE
        city_code = '242152'
        AND reception_id = ${receptionId}
    `;

  ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoDetail', 'updateGroupReceptionFields', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};