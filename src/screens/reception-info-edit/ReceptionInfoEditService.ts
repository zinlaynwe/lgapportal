import { executeQuery } from "../../aws/db/dbOperation";
import { getCurrentJapanTime } from "../../environments/TimeUtils";
import { ActivityLogger } from "../../log/ActivityLogger";
import { User } from "../../model/User";

export const fetchSingleReceptionData = async (receptionId: number, memberId: number) => {
    const method = "POST";
    const queryString = `
        SELECT
            reception_id,
            member_id,
            lastname,
            firstname,
            lastname_kana,
            firstname_kana,
            date_of_birth,
            gender_code,
            postal_code,
            address,
            relationship
        FROM
            reception
        WHERE
            city_code='242152'
            AND reception_id = ${receptionId}
            AND member_id = ${memberId}
      `;
    ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoEdit', 'fetchSingleReceptionData', 'execute query', '', null, queryString);
    return executeQuery(method, queryString);
};

export const fetchGroupReceptionData = async (receptionId: number) => {
    const method = "POST";
    const queryString = `
        SELECT
            reception_id,
            member_id,
            lastname,
            firstname,
            lastname_kana,
            firstname_kana,
            date_of_birth,
            gender_code,
            postal_code,
            address,
            relationship
        FROM
            reception
        WHERE
            city_code='242152'
            AND reception_id= '${receptionId}'
            AND NOT is_deleted
        ORDER BY
            family_order_number;
    `;

    ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoEdit', 'fetchGroupReceptionData', 'execute query', '', null, queryString);
    return executeQuery(method, queryString);
};

export const updateReceptionData = async (
    modifierId: string,
    lastName: string,
    firstName: string,
    lastNameKana: string,
    firstNameKana: string,
    dateOfBirth: string,
    genderCode: string,
    postalCode: string,
    address: string,
    relationship: string,
    receptionId: number,
    memberId: number,
  ) => {
    const method = "POST";
    const currentJapanTime = getCurrentJapanTime();
  
    const queryString = `UPDATE reception 
      SET 
        history_number = history_number + 1,
        modifier_id= '${modifierId}',
        modification_timestamp = '${currentJapanTime}',
        lastname = '${lastName}', 
        firstname = '${firstName}', 
        lastname_kana = '${lastNameKana}', 
        firstname_kana = '${firstNameKana}', 
        date_of_birth = '${dateOfBirth}', 
        gender_code = '${genderCode}', 
        postal_code = '${postalCode}', 
        address = '${address}', 
        relationship = '${relationship}'
       
      WHERE
      city_code = '242152'
      AND reception_id = ${receptionId}
      AND member_id = ${memberId}`;
  
    ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoEdit', 'updateReceptionData', 'execute query', '', null, queryString);
    return executeQuery(method, queryString);
  };

  export const insertReceptionHistory = async (
    receptionId: number,
    memberId: number
  ) => {
    const method = "POST";
  
    const queryString = `
      INSERT INTO 
        reception_history 
      SELECT 
        * 
      FROM 
      reception 
      WHERE 
        city_code = '242152'
         AND reception_id = ${receptionId}
         AND member_id = ${memberId}`;
  
    ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoEdit', 'insertReceptionHistory', 'execute query', '', null, queryString);
    return executeQuery(method, queryString);
  };