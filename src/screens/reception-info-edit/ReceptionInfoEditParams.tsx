import { User } from "../../model/User";

export class ReceptionInfoEditParams implements Params {
  private _user: User = new User();
  private _eventId: number = 0;
  private _receptionId: number = 0;
  private _memberId: number = 0;
  private _eventDetailMemberId: number = 0;
  private _pgType: string = "";
  private _displayMode: string = "";

  get user(): User {
    return this._user.clone();
  }

  set user(value: User) {
    this._user = value;
  }

  get eventId(): number {
    return this._eventId;
  }

  set eventId(value: number) {
    this._eventId = value;
  }

  get receptionId(): number {
    return this._receptionId;
  }

  set receptionId(value: number) {
    this._receptionId = value;
  }

  get memberId(): number {
    return this._memberId;
  }

  set memberId(value: number) {
    this._memberId = value;
  }

  get eventDetailMemberId(): number {
    return this._eventDetailMemberId;
  }

  set eventDetailMemberId(value: number) {
    this._eventDetailMemberId = value;
  }
  
  get pgType(): string {
    return this._pgType;
  }

  set pgType(value: string) {
    this._pgType = value;
  }
  
  get displayMode(): string {
    return this._displayMode;
  }

  set displayMode(value: string) {
    this._displayMode = value;
  }

  getAllValuesAsString(): string {
    let paramsString = '[ClassName=ReceptionInfoEditParams';
    paramsString += ',_user=' + this._user.getAllValuesAsString();
    paramsString += ',_eventId=' + this._eventId;
    paramsString += ',_receptionId=' + this._receptionId;
    paramsString += ',_memberId=' + this._memberId;
    paramsString += ',_eventDetailMemberId=' + this._eventDetailMemberId;
    paramsString += ',_pgType=' + this._pgType;
    paramsString += ',_displayMode=' + this._displayMode;
    paramsString += ']';
    return paramsString
  }
}
