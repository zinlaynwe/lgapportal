import React, { useState, useRef, useEffect } from "react";
import {
  SafeAreaView,
  StatusBar,
  View,
  TouchableOpacity,
  TextInput,
  Pressable,
  Keyboard,
  TouchableWithoutFeedback,
  ScrollView,
} from "react-native";
import { Header } from "../../components/basics/header";
import { CustomButton } from "../../components/basics/Button";
import { MaterialIcons, Entypo } from "@expo/vector-icons";
import { colors } from "../../styles/color";
import styles from "./ReceptionInfoEditStyles";
import { HiraginoKakuText } from "../../components/StyledText";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { ReceptionInfoSaveDialog } from "../reception-info-save-dialog/ReceptionInfoSaveDialog";
import { format, parse } from "date-fns";
import { CustomCalendar } from "../../components/basics/Calendar";
import { ReceptionInfoEditParams } from "./ReceptionInfoEditParams";
import { ReceptionInfoDetailParams } from "../reception-info-detail/ReceptionInfoDetailParams";
import { handleZipCodeChange, isKatakanaText, isValidPostalCode } from "../../environments/InputValidationMethods";
import { searchAddressFromPostalCode } from "../../environments/InputMethods";
import { fetchGroupReceptionData, fetchSingleReceptionData, insertReceptionHistory, updateReceptionData } from "./ReceptionInfoEditService";
import { ActivityLogger } from "../../log/ActivityLogger";
import isEqual from "lodash/isEqual";
import ReceptionInfoErrorDialog from "../reception-info-error-dialog/ReceptionInfoErrorDialog";
import { ReceptionInfoNoErrorDialog } from "../reception-info-no-error-dialog/ReceptionInfoNoErrorDialog";

type Props = {
  navigation: NavigationProp<any, any>;
};

type Params = {
  receptionInfoEditParams: ReceptionInfoEditParams;
};

type ErrorMessages = {
  [participantId: number]: {
    participantLastName?: string;
    participantFirstName?: string;
    participantKanaLastName?: string;
    participantKanaFirstName?: string;
    participantAddress?: string;
    participantPostalCode?: string;
    participantRelationship?: string;
  };
};

interface Participant {
  participantLastName: string;
  participantFirstName: string;
  participantKanaLastName: string;
  participantKanaFirstName: string;
  participantDateOfBirth: string;
  participantGendarCode: string;
  participantPostalCode: string;
  participantAddress: string;
  participantRelationship: string;
  participantReceptionId: number;
  participantMemberId: number;
}

export const ReceptionInfoEdit = ({ navigation }: Props) => {
  const route = useRoute();

  const { receptionInfoEditParams } = route.params as Params;
  // MODAL Dialog
  const [isSaveModalVisible, setIsSaveModalVisible] = useState(false);
  const [isErrorModalVisible, setIsErrorModalVisible] = useState(false);
  const [isNoErrorModalVisible, setIsNoErrorModalVisible] = useState(false);

  const [isBirthDateCalendarVisible, setBirthDateCalendarVisible] = useState(false);
  const birthDateInputRef = useRef(null);
  const birthDateRef = useRef(null);

  // RIGHT View Data
  const [selectedParticipant, setSelectedParticipant] = useState<Participant | null>(null);
  const [currentSelectedParticipant, setCurrentSelectedParticipant] = useState<Participant | null>(null);
  const [participantData, setParticipantData] = useState<Participant[]>([]);
  const [originalParticipantData, setOriginalParticipantData] = useState<Participant[]>([]);
  const [updatedParticipantData, setUpdatedParticipantData] = useState<Participant[]>([]);

  const [selectedGenderCode, setSelectedGenderCode] = useState('');
  const [relationship, setRelationship] = useState<string[]>([]);
  const [toastMsg, setToastMsg] = useState('');
  const [showToast, setShowToast] = useState(false);

  // ERR Msg
  const [searchErrMsg, setSearchErrMsg] = useState(false);
  const [isSearchBtnClick, setIsSearchBtnClick] = useState(false);
  const [errorMessages, setErrorMessages] = useState<ErrorMessages>({});

  useEffect(() => {
    ActivityLogger.insertInfoLogEntry(receptionInfoEditParams.user, 'ReceptionInfoEdit', 'useEffect', 'screen open');
    handleReceptionEditData();
  }, []);

  useEffect(() => {
    // GENDER Code
    if (selectedParticipant && selectedParticipant.participantGendarCode) {
      setSelectedGenderCode(selectedParticipant.participantGendarCode);
    }

    // TOOGLE Msg
    if (toastMsg) {
      setShowToast(true);

      const timer = setTimeout(() => {
        setToastMsg("");
        setShowToast(false);
      }, 1300);

      return () => clearTimeout(timer);
    }

  }, [selectedParticipant, toastMsg]);

  const handleReceptionEditData = async () => {
    // 1人で受付した場合
    if (receptionInfoEditParams.displayMode == 'single') {
      if (receptionInfoEditParams.receptionId) {
        const クエリ結果_reception = await fetchSingleReceptionData(
          receptionInfoEditParams.receptionId, receptionInfoEditParams.memberId
        );

        if (クエリ結果_reception.data) {
          const participantData = クエリ結果_reception.data.map((item: any) => {
            //生年月日
            const formattedDateOfBirth = format(
              new Date(item.date_of_birth),
              "yyyy/MM/dd"
            );
            // 郵便番号
            const firstPart = item.postal_code.substring(0, 3);
            const secondPart = item.postal_code.substring(3, 7);
            const formattedPostalCode = `${firstPart}-${secondPart}`;

            return {
              participantLastName: item.lastname,
              participantFirstName: item.firstname,
              participantKanaLastName: item.lastname_kana,
              participantKanaFirstName: item.firstname_kana,
              participantDateOfBirth: formattedDateOfBirth,
              participantGendarCode: item.gender_code,
              participantPostalCode: formattedPostalCode,
              participantAddress: item.address,
              participantRelationship: item.relationship,
              participantMemberId: item.member_id,
              participantReceptionId: item.reception_id,
            } as Participant;
          });

          setParticipantData(participantData);
          setOriginalParticipantData(participantData);

          setSelectedParticipant(participantData[0]);
        } else {
          console.log("Error: fetchSingleReceptionData からデータが返されませんでした");
        }

      } else {
        console.log("Error: 受付IDには値がありません");
      }
    } else if (receptionInfoEditParams.displayMode == 'group') {
      if (receptionInfoEditParams.receptionId) {
        const クエリ結果_reception = await fetchGroupReceptionData(
          receptionInfoEditParams.receptionId
        );

        if (クエリ結果_reception.data) {
          const participantData = クエリ結果_reception.data.map((item: any) => {
            //生年月日
            const formattedDateOfBirth = format(
              new Date(item.date_of_birth),
              "yyyy/MM/dd"
            );
            // 郵便番号
            const firstPart = item.postal_code.substring(0, 3);
            const secondPart = item.postal_code.substring(3, 7);
            const formattedPostalCode = `${firstPart}-${secondPart}`;

            // 代表者との関係 Array Values
            const relationshipValue: string = item.relationship as string;
            if (relationshipValue && !relationship.includes(relationshipValue)) {
              setRelationship(prev => [...prev, relationshipValue]);
            }

            return {
              participantLastName: item.lastname,
              participantFirstName: item.firstname,
              participantKanaLastName: item.lastname_kana,
              participantKanaFirstName: item.firstname_kana,
              participantDateOfBirth: formattedDateOfBirth,
              participantGendarCode: item.gender_code,
              participantPostalCode: formattedPostalCode,
              participantAddress: item.address,
              participantRelationship: item.relationship,
              participantMemberId: item.member_id,
              participantReceptionId: item.reception_id,
            } as Participant;
          });

          setParticipantData(participantData);
          setOriginalParticipantData(participantData);

          setSelectedParticipant(participantData.find((item: { participantMemberId: number; }) => item.participantMemberId === receptionInfoEditParams.memberId));

        } else {
          console.log("Error: fetchGroupReceptionData からデータが返されませんでした");
        }
      } else {
        console.log("Error: 受付IDには値がありません");
      }

    } else {
      console.log("Error: 表示モード値が無効です");
    }
  }

  // HANDLE Radio Btn
  const handleGenderOption = (code: string, value: string) => {
    setSelectedGenderCode(code);
    handleParticipantChange("participantGendarCode", code);
  };

  // HANDLE Updated Data
  const handleParticipantChange = (field: keyof Participant, value: any) => {
    const participantId = selectedParticipant?.participantMemberId;
    if (participantId === undefined) return;

    if (typeof value === 'string') {
      value = value.trim();
    }
    const updatedParticipants = participantData.map(participant => {
      if (participant.participantMemberId === participantId) {
        return { ...participant, [field]: value };
      }
      return participant;
    });

    setParticipantData(updatedParticipants);

    if (selectedParticipant?.participantMemberId === participantId) {
      setSelectedParticipant(prevSelected => {
        if (prevSelected) {
          return {
            ...prevSelected,
            [field]: value,
          };
        }
        return prevSelected;
      });
    }

    // ADD Updated Data
    const updatedData = getUpdatedData(
      originalParticipantData,
      updatedParticipants
    );

    setUpdatedParticipantData(updatedData);
  };

  const getUpdatedData = (
    originalParticipantData: any[],
    updateParticipantData: any[]
  ) => {
    const updatedParticipants: any[] = [];

    originalParticipantData.forEach((orginalParticipant, index) => {
      const updParticipant = updateParticipantData[index];
      if (updParticipant) {

        if (!isEqual(orginalParticipant, updParticipant)) {
          updatedParticipants.push(updParticipant);
        }
      }
    });
    return updatedParticipants;
  };

  const updateReceptionInfo = async () => {
    let allResults = [];
    let updateReceptionDatas = updatedParticipantData;

    try {
      for (const participant of updateReceptionDatas) {
        const result = await insertReceptionHistory(
          participant.participantReceptionId,
          participant.participantMemberId
        );

        const message = result.message;

        if (message == "success") {
          const formattedPostCode = participant.participantPostalCode.replace(
            "-",
            ""
          );

          const resultUpd = await updateReceptionData(
            receptionInfoEditParams.user.userId,
            participant.participantLastName,
            participant.participantFirstName,
            participant.participantKanaLastName,
            participant.participantKanaFirstName,
            participant.participantDateOfBirth,
            participant.participantGendarCode,
            formattedPostCode,
            participant.participantAddress,
            participant.participantRelationship,
            participant.participantReceptionId,
            participant.participantMemberId,
          );
          const updmessage = resultUpd.message;

          if (updmessage == "success") {
            allResults.push(resultUpd);
          }
        } else {
          console.log("Unknown error occurred");
        }
      }
      const success = allResults.every(
        (result) => result.message === "success"
      );
      return success;

    } catch (error) {
      console.error("Error from ReceptionInfoEditService:", error);
      return false;
    }
  };

  // HANDLE Left Btn 
  const handleButtonPress = async (participant: any) => {
    setCurrentSelectedParticipant(participant);
    if (handleInputBlur('participantLastName') ||
      handleInputBlur('participantFirstName') ||
      handleInputBlur('participantKanaLastName') ||
      handleInputBlur('participantKanaFirstName') ||
      handleInputBlur('participantAddress') ||
      handleInputBlur('participantPostalCode') ||
      handleInputBlur('participantRelationship')
    ) {
      setIsErrorModalVisible(true);
      setSelectedParticipant(selectedParticipant);
    } else {
      // UPDATE into DB
      if (updatedParticipantData.length > 0) {
        setIsNoErrorModalVisible(true);
        setCurrentSelectedParticipant(participant);
      } else {
        setSelectedParticipant(participant);
      }
    }
  };

  // Footer SAVE
  const handleCreateButtonPress = async () => {
    if (handleInputBlur('participantLastName') === undefined &&
      handleInputBlur('participantFirstName') === undefined &&
      handleInputBlur('participantKanaLastName') === undefined &&
      handleInputBlur('participantKanaFirstName') === undefined &&
      handleInputBlur('participantAddress') === undefined &&
      (handleInputBlur('participantPostalCode') === undefined || handleInputBlur('participantPostalCode') === '') &&
      handleInputBlur('participantRelationship') === undefined
    ) {
      if (updatedParticipantData.length > 0) {
        const isUpdated = updateReceptionInfo();

        if (await isUpdated) {
          let receptionInfoDetailParams = new ReceptionInfoDetailParams();
          receptionInfoDetailParams.user = receptionInfoEditParams.user;
          receptionInfoDetailParams.eventId = receptionInfoEditParams.eventId;
          receptionInfoDetailParams.receptionId = receptionInfoEditParams.receptionId;
          receptionInfoDetailParams.memberId = receptionInfoEditParams.eventDetailMemberId;
          receptionInfoDetailParams.pgType = receptionInfoEditParams.pgType;
          receptionInfoDetailParams.toastMsg = "受付情報を更新しました";

          navigation.navigate("ReceptionInfoDetail", {
            receptionInfoDetailParams,
          });
          ActivityLogger.insertInfoLogEntry(receptionInfoEditParams.user, 'ReceptionInfoEdit', 'updateReceptionInfo', 'transition', 'ReceptionInfoDetail', receptionInfoDetailParams);

          console.log("Inserted Successfully!!");
        }

      } else {
        let receptionInfoDetailParams = new ReceptionInfoDetailParams();
        receptionInfoDetailParams.user = receptionInfoEditParams.user;
        receptionInfoDetailParams.eventId = receptionInfoEditParams.eventId;
        receptionInfoDetailParams.receptionId = receptionInfoEditParams.receptionId;
        receptionInfoDetailParams.memberId = receptionInfoEditParams.eventDetailMemberId;
        receptionInfoDetailParams.pgType = receptionInfoEditParams.pgType;
        navigation.navigate("ReceptionInfoDetail", {
          receptionInfoDetailParams,
        });
        ActivityLogger.insertInfoLogEntry(receptionInfoEditParams.user, 'ReceptionInfoEdit', 'handleCreateButtonPress', 'transition', 'ReceptionInfoDetail', receptionInfoDetailParams);

      }
    }
  };

  //Reception Save Dialog
  const handleCloseButton = () => {
    setBirthDateCalendarVisible(false);
    setIsSaveModalVisible(true);
  };

  // SAVE Dialog
  const handleCancelButton = () => {
    setIsSaveModalVisible(false);
    setIsErrorModalVisible(false);
    setIsNoErrorModalVisible(false);
  };

  const handleNotSaveButton = () => {
    setIsSaveModalVisible(false);

    let receptionInfoDetailParams = new ReceptionInfoDetailParams();
    receptionInfoDetailParams.user = receptionInfoEditParams.user;
    receptionInfoDetailParams.eventId = receptionInfoEditParams.eventId;
    receptionInfoDetailParams.receptionId = receptionInfoEditParams.receptionId;
    receptionInfoDetailParams.memberId = receptionInfoEditParams.memberId;
    navigation.navigate("ReceptionInfoDetail", {
      receptionInfoDetailParams,
    });
    ActivityLogger.insertInfoLogEntry(receptionInfoEditParams.user, 'ReceptionInfoEdit', 'handleNotSaveButton', 'transition', 'ReceptionInfoDetail', receptionInfoDetailParams);
  };

  const handleSaveButton = async () => {
    setIsSaveModalVisible(false);

    if (handleInputBlur('participantLastName') === undefined &&
      handleInputBlur('participantFirstName') === undefined &&
      handleInputBlur('participantKanaLastName') === undefined &&
      handleInputBlur('participantKanaFirstName') === undefined &&
      handleInputBlur('participantAddress') === undefined &&
      (handleInputBlur('participantPostalCode') === undefined || handleInputBlur('participantPostalCode') === '') &&
      handleInputBlur('participantRelationship') === undefined
    ) {
      if (updatedParticipantData.length > 0) {
        updateReceptionInfo();
        const isUpdated = updateReceptionInfo();
        if (await isUpdated) {
          let receptionInfoDetailParams = new ReceptionInfoDetailParams();
          receptionInfoDetailParams.user = receptionInfoEditParams.user;
          receptionInfoDetailParams.eventId = receptionInfoEditParams.eventId;
          receptionInfoDetailParams.receptionId = receptionInfoEditParams.receptionId;
          receptionInfoDetailParams.memberId = receptionInfoEditParams.eventDetailMemberId;
          receptionInfoDetailParams.pgType = receptionInfoEditParams.pgType;
          receptionInfoDetailParams.toastMsg = "受付情報を更新しました";

          navigation.navigate("ReceptionInfoDetail", {
            receptionInfoDetailParams,
          });
          ActivityLogger.insertInfoLogEntry(receptionInfoEditParams.user, 'ReceptionInfoEdit', 'updateReceptionInfo', 'transition', 'ReceptionInfoDetail', receptionInfoDetailParams);

          console.log("Inserted Successfully!!");
        }

      } else {
        let receptionInfoDetailParams = new ReceptionInfoDetailParams();
        receptionInfoDetailParams.user = receptionInfoEditParams.user;
        receptionInfoDetailParams.eventId = receptionInfoEditParams.eventId;
        receptionInfoDetailParams.receptionId = receptionInfoEditParams.receptionId;
        receptionInfoDetailParams.memberId = receptionInfoEditParams.eventDetailMemberId;
        receptionInfoDetailParams.pgType = receptionInfoEditParams.pgType;
        navigation.navigate("ReceptionInfoDetail", {
          receptionInfoDetailParams,
        });
        ActivityLogger.insertInfoLogEntry(receptionInfoEditParams.user, 'ReceptionInfoEdit', 'handleSaveButton', 'transition', 'ReceptionInfoDetail', receptionInfoDetailParams);
      }
    }

  };

  // ERROR Dialog
  const handleSwitchButton = () => {
    const oldParticipant = selectedParticipant as Participant;
    const participantIndex = originalParticipantData.findIndex(p => p.participantMemberId === oldParticipant.participantMemberId);

    if (participantIndex !== -1) {
      // RESET to Original Data
      const originalData = originalParticipantData[participantIndex];

      const updatedParticipantData = [...participantData];
      const updateIndex = updatedParticipantData.findIndex(p => p.participantMemberId === oldParticipant.participantMemberId);

      if (updateIndex !== -1) {
        updatedParticipantData[updateIndex] = { ...originalData };
        setParticipantData(updatedParticipantData);
      }

      // CLEAN UpdatedDataSet
      setUpdatedParticipantData([]);

      // SWITCH to Selected Left button
      setSelectedParticipant(currentSelectedParticipant);

      // CLEAN Err Msg
      setErrorMessages(prevErrorMessages => {
        const updatedErrorMessages = { ...prevErrorMessages };
        if (updatedErrorMessages[oldParticipant.participantMemberId]) {
          updatedErrorMessages[oldParticipant.participantMemberId] = {};
        }
        return updatedErrorMessages;
      });

      setIsErrorModalVisible(false);
    }
  }

  // NO-ERROR Dialog
  const handleNoErrSaveButton = async () => {
    setIsNoErrorModalVisible(false);

    if (updatedParticipantData.length > 0) {
      const isUpdated = updateReceptionInfo();

      if (await isUpdated) {
        setSelectedParticipant(currentSelectedParticipant);
        setToastMsg('受付情報を更新しました');

        // UPDATE OrigianlParticipant as DB
        const updateParticipant = selectedParticipant as Participant;
        const participantIndex = originalParticipantData.findIndex(p => p.participantMemberId === updateParticipant.participantMemberId);
        if (participantIndex !== -1) {
          const updatedParticipantData = [...originalParticipantData];
          updatedParticipantData[participantIndex] = { ...updateParticipant };
          setParticipantData(updatedParticipantData);
          setOriginalParticipantData(updatedParticipantData);
        }

        // CLEAN
        setUpdatedParticipantData([]);
        console.log("Inserted Successfully!!");
      }
    } else {
      setSelectedParticipant(currentSelectedParticipant);
    }
  };

  const handleNoErrNotSaveButton = () => {
    const oldParticipant = selectedParticipant as Participant;
    const participantIndex = originalParticipantData.findIndex(p => p.participantMemberId === oldParticipant.participantMemberId);

    if (participantIndex !== -1) {
      // RESET to Original Data
      const originalData = originalParticipantData[participantIndex];

      const updatedParticipantData = [...participantData];
      const updateIndex = updatedParticipantData.findIndex(p => p.participantMemberId === oldParticipant.participantMemberId);

      if (updateIndex !== -1) {
        updatedParticipantData[updateIndex] = { ...originalData };
        setParticipantData(updatedParticipantData);
      }

      // CLEAN UpdatedDataSet
      setUpdatedParticipantData([]);

      // SWITCH to Selected Left button
      setSelectedParticipant(currentSelectedParticipant);

      // CLEAN Err Msg
      setErrorMessages(prevErrorMessages => {
        const updatedErrorMessages = { ...prevErrorMessages };
        if (updatedErrorMessages[oldParticipant.participantMemberId]) {
          updatedErrorMessages[oldParticipant.participantMemberId] = {};
        }
        return updatedErrorMessages;
      });
      setIsNoErrorModalVisible(false);
    }
  };

  const handleBirthDateCalendarPress = (event: any) => {
    (birthDateInputRef.current as any).focus();
    setBirthDateCalendarVisible(!isBirthDateCalendarVisible);
  };

  const handleBirthDateSelect = (date: any) => {
    const formattedDate = format(date, "yyyy/MM/dd");
    setSelectedParticipant((prevSelectedParticipant) =>
      prevSelectedParticipant ? {
        ...prevSelectedParticipant,
        participantDateOfBirth: formattedDate,
      } : null
    );
    handleParticipantChange('participantDateOfBirth', formattedDate);
    setBirthDateCalendarVisible(false);
  };

  const closeCalendar = (event: any) => {
    if (
      event.nativeEvent.target != birthDateInputRef.current &&
      event.nativeEvent.target != birthDateRef
    ) {
      if (isBirthDateCalendarVisible) {
        setBirthDateCalendarVisible(false);
      }
    }
  };

  const handleInputBlur = (field: keyof Participant): string | undefined => {
    const participantId = selectedParticipant?.participantMemberId;
    if (participantId === undefined) return undefined;

    let errorMessage = '';

    if (field === 'participantLastName') {
      if (!selectedParticipant?.participantLastName) {
        errorMessage = "お名前を入力してください";
      } else {
        return;
      }
    } else if (field === 'participantFirstName') {
      if (!selectedParticipant?.participantFirstName) {
        errorMessage = "お名前を入力してください";
      } else {
        return;
      }
    } else if (field === 'participantKanaLastName') {
      if (!selectedParticipant?.participantKanaLastName) {
        errorMessage = "お名前（カナ）を入力してください";
      } else if (!isKatakanaText(selectedParticipant.participantKanaLastName.trim())) {
        errorMessage = "カタカナで入力してください";
      } else {
        return;
      }
    } else if (field === 'participantKanaFirstName') {
      if (!selectedParticipant?.participantKanaFirstName) {
        errorMessage = "お名前（カナ）を入力してください";
      } else if (!isKatakanaText(selectedParticipant.participantKanaFirstName.trim())) {
        errorMessage = "カタカナで入力してください";
      } else {
        return;
      }
    } else if (field === 'participantAddress') {
      if (!selectedParticipant?.participantAddress) {
        errorMessage = "住所を入力してください";
      } else {
        return;
      }
    } else if (field === 'participantPostalCode') {
      if (selectedParticipant) {
        if (selectedParticipant?.participantPostalCode) {

          if (searchErrMsg) {
            errorMessage = "現在、郵便番号による住所検索はご利用いただけません";
          } else if (!/^\d{3}-\d{4}$/.test(selectedParticipant.participantPostalCode)) {
            errorMessage = "郵便番号は数字7桁で入力してください";
          } else if (!isSearchBtnClick) {
            errorMessage = isValidPostalCode(selectedParticipant.participantPostalCode);
          } else {
            if (isSearchBtnClick) {
              if (isValidPostalCode(selectedParticipant.participantPostalCode)) {
                errorMessage = isValidPostalCode(selectedParticipant.participantPostalCode);
              } else {
                if (selectedParticipant.participantAddress) {
                  errorMessage = "";
                } else {
                  errorMessage = "この郵便番号は使用できません";
                }
              }

            } else {
              errorMessage = "";
            }
          }
        } else {
          if (!/^\d{3}-\d{4}$/.test(selectedParticipant.participantPostalCode)) {
            errorMessage = "郵便番号は数字7桁で入力してください";
          } else {
            errorMessage = "郵便番号を入力してください";
          }
        }
      }
    } else if (field === 'participantRelationship') {
      if (!selectedParticipant?.participantRelationship) {
        errorMessage = "代表者との関係性を入力してください";
      } else {
        return;
      }
    } else {
      return;
    }

    setErrorMessages(prevErrors => ({
      ...prevErrors,
      [participantId]: {
        ...prevErrors[participantId],
        [field]: errorMessage
      }
    }));
    console.log(errorMessage, " REL:::");
    return errorMessage;
  };

  return (
    <TouchableWithoutFeedback onPress={closeCalendar}>
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <Header middleTitleName="受付情報編集" buttonName="">
          <CustomButton
            text="閉じる"
            type="ButtonSDefault"
            icon={<Entypo name="cross" size={24} color={colors.primary} />}
            iconPosition="front"
            onPress={handleCloseButton}
          />
        </Header>
        <ScrollView style={styles.container} scrollEnabled={true}>
          <View style={styles.bodyContainer}>
            {/* Conditionally render the left layout //acceptedPersons > 1 */}
            {receptionInfoEditParams.displayMode === "group" && (
              <View style={styles.leftContainer}>
                <View style={styles.leftTitleContainer}>
                  <HiraginoKakuText style={[styles.bodyText, styles.titleText]}>
                    受付人数
                  </HiraginoKakuText>
                  <HiraginoKakuText style={[styles.bodyText, styles.titleText]}>
                    {participantData.length}人
                  </HiraginoKakuText>
                </View>
                <View style={styles.leftButtonsContainer}>
                  <View style={styles.leftSubButtonContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.subTitleText]}
                    >
                      受付代表
                    </HiraginoKakuText>
                    {participantData.filter(item => item.participantMemberId == 0).map(participant => (
                      <TouchableOpacity
                        key={participant.participantMemberId}
                        style={[
                          styles.leftButton,
                          selectedParticipant?.participantMemberId === participant.participantMemberId && styles.btnSelected,
                        ]}
                        onPress={() => {
                          handleButtonPress(participant);
                        }}
                      >
                        <HiraginoKakuText style={[styles.bodyText]} normal numberOfLines={1}>
                          {originalParticipantData[0].participantLastName} {originalParticipantData[0].participantFirstName}
                        </HiraginoKakuText>
                      </TouchableOpacity>
                    ))}
                  </View>
                  <View style={styles.leftSubButtonContainer}>
                    <HiraginoKakuText style={[styles.bodyText, styles.subTitleText]}>
                      一緒に受付
                    </HiraginoKakuText>
                    {participantData.filter(item => item.participantMemberId > 0).map(participant => (
                      <TouchableOpacity
                        key={participant.participantMemberId}
                        style={[
                          styles.leftButton,
                          selectedParticipant?.participantMemberId === participant.participantMemberId && styles.btnSelected,
                        ]}
                        onPress={() => {
                          handleButtonPress(participant);
                        }}
                      >
                        <HiraginoKakuText style={[styles.bodyText]} normal numberOfLines={1}>
                          {`${originalParticipantData.find(data =>
                            data.participantMemberId === participant.participantMemberId)?.participantLastName} 
                          ${originalParticipantData.find(data =>
                              data.participantMemberId === participant.participantMemberId)?.participantFirstName}`}
                        </HiraginoKakuText>
                      </TouchableOpacity>
                    ))}
                  </View>
                </View>
              </View>
            )}


            {/* Right layout */}
            <View style={styles.rightContainer}>
              {selectedParticipant && (
                <View style={styles.rightBodyContainer}>
                  <View style={styles.nameContainer}>
                    <View style={styles.rightLabelContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.subTitleText]}
                      >
                        お名前
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.nameInputBoxContainer}>
                      <View style={styles.inputWithErrBoxes}>
                        <TextInput
                          style={[styles.textInput]}
                          placeholder="出茂"
                          placeholderTextColor={colors.placeholderTextColor}
                          value={selectedParticipant.participantLastName}
                          onChangeText={(text) => {
                            setSelectedParticipant({ ...selectedParticipant, participantLastName: text })
                          }}
                          onFocus={() => {
                            setBirthDateCalendarVisible(false);
                            const participantId = selectedParticipant?.participantMemberId;
                            if (participantId !== undefined) {
                              setErrorMessages(prevErrors => ({
                                ...prevErrors,
                                [participantId]: {
                                  ...prevErrors[participantId],
                                  participantLastName: ''
                                }
                              }));
                            }
                          }}
                          onBlur={() => {
                            handleInputBlur('participantLastName');
                            handleParticipantChange(
                              'participantLastName',
                              selectedParticipant.participantLastName
                            );
                          }}
                        />
                        {errorMessages[selectedParticipant?.participantMemberId]?.participantLastName?.length != 0 && (
                          <HiraginoKakuText style={styles.errorText} normal>
                            {errorMessages[selectedParticipant.participantMemberId]?.participantLastName}
                          </HiraginoKakuText>
                        )}
                      </View>
                      <View style={styles.inputWithErrBoxes}>
                        <TextInput
                          style={[styles.textInput]}
                          placeholder="進次郎"
                          placeholderTextColor={colors.placeholderTextColor}
                          value={selectedParticipant.participantFirstName}
                          onChangeText={(text) => {
                            setSelectedParticipant({ ...selectedParticipant, participantFirstName: text })
                          }}
                          onFocus={() => {
                            setBirthDateCalendarVisible(false);
                            const participantId = selectedParticipant?.participantMemberId;
                            if (participantId !== undefined) {
                              setErrorMessages(prevErrors => ({
                                ...prevErrors,
                                [participantId]: {
                                  ...prevErrors[participantId],
                                  participantFirstName: ''
                                }
                              }));
                            }
                          }}
                          onBlur={() => {
                            handleInputBlur('participantFirstName');
                            handleParticipantChange(
                              'participantFirstName',
                              selectedParticipant.participantFirstName
                            );
                          }}
                        />
                        {errorMessages[selectedParticipant?.participantMemberId]?.participantFirstName?.length != 0 && (
                          <HiraginoKakuText style={styles.errorText} normal>
                            {errorMessages[selectedParticipant.participantMemberId]?.participantFirstName}
                          </HiraginoKakuText>
                        )}
                      </View>
                    </View>
                  </View>
                  <View style={styles.nameContainer}>
                    <View style={styles.rightLabelContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.subTitleText]}
                      >
                        お名前（カナ）
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.nameInputBoxContainer}>
                      <View style={styles.inputWithErrBoxes}>
                        <TextInput
                          style={[styles.textInput]}
                          placeholder="イズモ"
                          placeholderTextColor={colors.placeholderTextColor}
                          value={selectedParticipant.participantKanaLastName}
                          onChangeText={(text) => {
                            setSelectedParticipant({ ...selectedParticipant, participantKanaLastName: text });
                          }}
                          onFocus={() => {
                            setBirthDateCalendarVisible(false);
                            const participantId = selectedParticipant?.participantMemberId;
                            if (participantId !== undefined) {
                              setErrorMessages(prevErrors => ({
                                ...prevErrors,
                                [participantId]: {
                                  ...prevErrors[participantId],
                                  participantKanaLastName: ''
                                }
                              }));
                            }
                          }}
                          onBlur={() => {
                            handleInputBlur('participantKanaLastName');
                            handleParticipantChange(
                              'participantKanaLastName',
                              selectedParticipant.participantKanaLastName
                            );
                          }}
                        />
                        {errorMessages[selectedParticipant?.participantMemberId]?.participantKanaLastName?.length != 0 && (
                          <HiraginoKakuText style={styles.errorText} normal>
                            {errorMessages[selectedParticipant.participantMemberId]?.participantKanaLastName}
                          </HiraginoKakuText>
                        )}
                      </View>
                      <View style={styles.inputWithErrBoxes}>
                        <TextInput
                          style={[styles.textInput]}
                          placeholder="進次郎シンジロウ"
                          placeholderTextColor={colors.placeholderTextColor}
                          value={selectedParticipant.participantKanaFirstName}
                          onChangeText={(text) => {
                            setSelectedParticipant({ ...selectedParticipant, participantKanaFirstName: text });
                          }}
                          onFocus={() => {
                            setBirthDateCalendarVisible(false);
                            const participantId = selectedParticipant?.participantMemberId;
                            if (participantId !== undefined) {
                              setErrorMessages(prevErrors => ({
                                ...prevErrors,
                                [participantId]: {
                                  ...prevErrors[participantId],
                                  participantKanaFirstName: ''
                                }
                              }));
                            }
                          }}
                          onBlur={() => {
                            handleInputBlur('participantKanaFirstName');
                            handleParticipantChange(
                              'participantKanaFirstName',
                              selectedParticipant.participantKanaFirstName
                            );
                          }}
                        />
                        {errorMessages[selectedParticipant?.participantMemberId]?.participantKanaFirstName?.length != 0 && (
                          <HiraginoKakuText style={styles.errorText} normal>
                            {errorMessages[selectedParticipant.participantMemberId]?.participantKanaFirstName}
                          </HiraginoKakuText>
                        )}
                      </View>
                    </View>
                  </View>
                  <View style={styles.datePickerContainer}>
                    <View style={styles.rightLabelContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.subTitleText]}
                      >
                        生年月日
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.datePickerContainer}>
                      <TextInput
                        ref={birthDateInputRef}
                        style={styles.textInput}
                        placeholder="日付を選択"
                        placeholderTextColor={colors.placeholderTextColor}
                        value={
                          selectedParticipant.participantDateOfBirth !== ""
                            ? format(new Date(selectedParticipant.participantDateOfBirth), "yyyy/MM/dd")
                            : ""
                        }
                        onPressIn={handleBirthDateCalendarPress}
                        onPointerDown={handleBirthDateCalendarPress}
                        showSoftInputOnFocus={false}
                        onTouchStart={() => Keyboard.dismiss()}
                        editable={false}
                      />
                      <Pressable
                        style={styles.calendarIconContainer}
                        ref={birthDateRef}
                        onPress={handleBirthDateCalendarPress}
                      >
                        <MaterialIcons
                          name="calendar-today"
                          size={22}
                          color={colors.activeCarouselColor}
                          style={styles.calendarIcon}
                        />
                      </Pressable>
                      {isBirthDateCalendarVisible && (
                        <CustomCalendar
                          selectedDate={format(new Date(selectedParticipant.participantDateOfBirth), "yyyy-MM-dd")}
                          onDateSelect={handleBirthDateSelect}
                        />
                      )}
                    </View>
                  </View>
                  <View style={styles.genderContainer}>
                    <View style={styles.rightLabelContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.subTitleText]}
                      >
                        性別
                      </HiraginoKakuText>
                    </View>

                    <View style={styles.genderRadioPanel}>
                      <View style={styles.radioBtnContainer}>
                        <RadioPanel
                          selected={selectedGenderCode === "1"}
                          onPress={() => handleGenderOption("1", "男性")}
                          radioBtnText="男性"
                        />
                      </View>
                      <View style={styles.radioBtnContainer}>
                        <RadioPanel
                          selected={selectedGenderCode === "2"}
                          onPress={() => handleGenderOption("2", "女性")}
                          radioBtnText="女性"
                        />
                      </View>
                      <View
                        style={[styles.radioBtnContainer]}
                      >
                        <RadioPanel
                          selected={selectedGenderCode === "0"}
                          onPress={() => handleGenderOption("0", "回答しない")}
                          radioBtnText="回答しない"
                        />
                      </View>
                    </View>
                  </View>
                  <View style={styles.postCodeContainer}>
                    <View style={styles.rightLabelContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.subTitleText]}
                      >
                        郵便番号
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.postCodeBoxContainer}>
                      <TextInput
                        style={[styles.textInput, styles.postCodeInput]}
                        value={selectedParticipant.participantPostalCode}
                        onFocus={() => {
                          setBirthDateCalendarVisible(false);
                          setIsSearchBtnClick(false);
                          const participantId = selectedParticipant?.participantMemberId;
                          if (participantId !== undefined) {
                            setErrorMessages(prevErrors => ({
                              ...prevErrors,
                              [participantId]: {
                                ...prevErrors[participantId],
                                participantPostalCode: ''
                              }
                            }));
                          }
                        }}
                        onChangeText={(text) => {
                          const formattedText = handleZipCodeChange(text);
                          if (formattedText !== null) {
                            setSelectedParticipant({
                              ...selectedParticipant,
                              participantPostalCode: formattedText
                            });
                          } else {
                            handleInputBlur('participantPostalCode');
                          }
                        }}
                        inputMode="numeric"
                        maxLength={8}
                        onBlur={() => {
                          handleInputBlur('participantPostalCode');
                          handleParticipantChange("participantPostalCode", selectedParticipant.participantPostalCode);
                        }}
                      />
                      <TouchableOpacity
                        style={styles.searchBtn}
                        onPress={() => {
                          setIsSearchBtnClick(true);
                          const oldAddress = selectedParticipant.participantAddress;
                          setSelectedParticipant(prevSelectedParticipant => prevSelectedParticipant ? {
                            ...prevSelectedParticipant,
                            participantAddress: ` `,
                          } : null);

                          if (selectedParticipant.participantPostalCode) {
                            searchAddressFromPostalCode(selectedParticipant.participantPostalCode)
                              .then(([state, city]) => {
                                setSelectedParticipant(prevSelectedParticipant => prevSelectedParticipant ? {
                                  ...prevSelectedParticipant,
                                  participantAddress: `${state} ${city}`,
                                } : null);
                                handleInputBlur("participantPostalCode");
                              })
                              .catch(error => {
                                if (error.message === 'Postal code not found') {
                                  setSelectedParticipant(prevSelectedParticipant => prevSelectedParticipant ? {
                                    ...prevSelectedParticipant,
                                    participantAddress: '',
                                  } : null);
                                  handleInputBlur("participantPostalCode");
                                } else {
                                  console.error('Other error occurred:', error);
                                  handleInputBlur("participantPostalCode");
                                  setSearchErrMsg(true);
                                }
                              });
                          } else {
                            setSelectedParticipant(prevSelectedParticipant => prevSelectedParticipant ? {
                              ...prevSelectedParticipant,
                              participantAddress: oldAddress,
                            } : null);
                          }
                        }}
                        onBlur={() => {
                          handleParticipantChange("participantAddress", selectedParticipant.participantAddress);
                        }}
                      >
                        <HiraginoKakuText style={[styles.LabelLargeBold, styles.searchBtnText]}>
                          住所検索
                        </HiraginoKakuText>
                      </TouchableOpacity>

                    </View>
                    {errorMessages[selectedParticipant?.participantMemberId]?.participantPostalCode?.length != 0 && (
                      <HiraginoKakuText style={styles.errorText} normal>
                        {errorMessages[selectedParticipant.participantMemberId]?.participantPostalCode}
                      </HiraginoKakuText>
                    )}
                  </View>
                  <View style={styles.addressContainer}>
                    <View style={styles.rightLabelContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.subTitleText]}
                      >
                        住所
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.inputWithErrBoxes}>
                      <TextInput
                        style={styles.textInput}
                        value={selectedParticipant.participantAddress}
                        onChangeText={(text) => {
                          setSelectedParticipant({ ...selectedParticipant, participantAddress: text });
                        }}
                        onFocus={() => {
                          setBirthDateCalendarVisible(false);
                          const participantId = selectedParticipant?.participantMemberId;
                          if (participantId !== undefined) {
                            setErrorMessages(prevErrors => ({
                              ...prevErrors,
                              [participantId]: {
                                ...prevErrors[participantId],
                                participantAddress: ''
                              }
                            }));
                          }
                        }}
                        onBlur={() => {
                          handleInputBlur('participantAddress');
                          handleParticipantChange(
                            'participantAddress',
                            selectedParticipant.participantAddress
                          );
                        }}
                      />
                      {errorMessages[selectedParticipant?.participantMemberId]?.participantAddress?.length != 0 && (
                        <HiraginoKakuText style={styles.errorText} normal>
                          {errorMessages[selectedParticipant.participantMemberId]?.participantAddress}
                        </HiraginoKakuText>
                      )}
                    </View>
                  </View>
                  <View style={styles.relationshipContainer}>
                    <View style={styles.rightLabelContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.subTitleText]}
                      >
                        代表者との関係
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.inputWithErrBoxes}>
                      <TextInput
                        style={[styles.textInput, styles.relationInput,
                        selectedParticipant.participantMemberId === 0 && { backgroundColor: '#F5F5F5' }]}
                        value={selectedParticipant.participantRelationship}
                        editable={selectedParticipant.participantMemberId !== 0}
                        onChangeText={(text) => {
                          setSelectedParticipant({ ...selectedParticipant, participantRelationship: text });
                        }}
                        onFocus={() => {
                          setBirthDateCalendarVisible(false);
                          const participantId = selectedParticipant?.participantMemberId;
                          if (participantId !== undefined) {
                            setErrorMessages(prevErrors => ({
                              ...prevErrors,
                              [participantId]: {
                                ...prevErrors[participantId],
                                participantRelationship: ''
                              }
                            }));
                          }
                        }}
                        onBlur={() => {
                          handleInputBlur('participantRelationship');
                          handleParticipantChange(
                            'participantRelationship',
                            selectedParticipant.participantRelationship
                          );
                        }}
                      />
                      {errorMessages[selectedParticipant?.participantMemberId]?.participantRelationship?.length != 0 && (
                        <HiraginoKakuText style={styles.errorText} normal>
                          {errorMessages[selectedParticipant.participantMemberId]?.participantRelationship}
                        </HiraginoKakuText>
                      )}
                    </View>
                  </View>
                </View>
              )}
              <View style={styles.saveBtnContainer}>
                <TouchableOpacity
                  style={[styles.saveBtn]}
                  onPress={() => {
                    handleCreateButtonPress();
                  }}
                >
                  <HiraginoKakuText
                    style={[styles.saveBtnText, styles.LabelLargeBold]}
                  >
                    保存する
                  </HiraginoKakuText>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
        {isSaveModalVisible && (
          <ReceptionInfoSaveDialog
            onUnsaveButtonPress={handleNotSaveButton}
            onSaveButtonPress={handleSaveButton}
            onCancelButtonPress={handleCancelButton}
          />
        )}
        {isErrorModalVisible && (
          <ReceptionInfoErrorDialog
            onSwitch={handleSwitchButton}
            onCancel={handleCancelButton}
          />
        )}
        {isNoErrorModalVisible && (
          <ReceptionInfoNoErrorDialog
            onUnsaveButtonPress={handleNoErrNotSaveButton}
            onSaveButtonPress={handleNoErrSaveButton}
            onCancelButtonPress={handleCancelButton}
          />
        )}
        {showToast && (
          <View style={styles.modalBackground}>
            <View style={styles.toastMessage}>
              <HiraginoKakuText style={styles.toastText}>
                {toastMsg}
              </HiraginoKakuText>
            </View>
          </View>
        )}
      </SafeAreaView>
    </TouchableWithoutFeedback >
  );
};

const RadioButton = (props: any) => {
  const outerBorderColor = props.selected ? "#346DF4" : "#B8BCC7";

  return (
    <View
      style={[
        {
          height: 24,
          width: 24,
          borderRadius: 12,
          borderWidth: 2,
          borderColor: outerBorderColor,
          alignItems: "center",
          justifyContent: "center",
        },
        props.style,
      ]}
    >
      {props.selected ? (
        <View
          style={{
            height: 10,
            width: 10,
            borderRadius: 6,
            backgroundColor: "#346DF4",
          }}
        />
      ) : null}
    </View>
  );
};

interface RadioPanelProps {
  selected: boolean;
  onPress: () => void;
  radioBtnText: string;
}

const RadioPanel = ({ selected, onPress, radioBtnText }: RadioPanelProps) => {
  return (
    <Pressable onPress={onPress} style={styles.radioPressable}>
      <View style={styles.radioButtonIcon}>
        <RadioButton
          selected={selected}
          style={[styles.radioButton, selected && styles.selectedRadioButton]}
        />
      </View>
      <View style={styles.radioTextContainer}>
        <HiraginoKakuText style={styles.radioText} normal>
          {radioBtnText}
        </HiraginoKakuText>
      </View>
    </Pressable>
  );
};