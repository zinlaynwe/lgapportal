import { StyleSheet } from "react-native";
import { colors } from "../../styles/color";
import { BodyTextLarge } from "../../styles/typography";

export const styles = StyleSheet.create({
  bodyContainer: {
    backgroundColor: colors.secondary,
  },
  bodyText: {
    fontSize: BodyTextLarge.size,
    lineHeight: BodyTextLarge.lineHeight,
    color: colors.textColor,
  },
  bulletTextContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: 10,
    width: "100%",
  },
  bullet: {
    fontSize: 3,
    marginRight: 5,
  },
});

export default styles;
