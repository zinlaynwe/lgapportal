export class ReceptionInfoDeleteParams implements Params {
  private _deleteMode: string = "";
  private _receptionId: number = 0;
  private _memberId: number = 0;

  get deleteMode(): string {
    return this._deleteMode;
  }

  set deleteMode(value: string) {
    this._deleteMode = value;
  }

  get receptionId(): number {
    return this._receptionId;
  }

  set receptionId(value: number) {
    this._receptionId = value;
  }

  get memberId(): number {
    return this._memberId;
  }

  set memberId(value: number) {
    this._memberId = value;
  }

  getAllValuesAsString(): string {
    let paramsString = '[ClassName=ReceptionInfoDeleteParams';
    paramsString += ',_deleteMode=' + this._deleteMode;
    paramsString += ',_receptionId=' + this._receptionId;
    paramsString += ',_memberId=' + this._memberId;
    paramsString += ']';
    return paramsString
  }
}
