import React, { useEffect, useState } from "react";
import { View } from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import ModalComponent from "../../components/basics/ModalComponent";
import styles from "./ReceptionInfoDeleteDialogStyles";
import { FontAwesome } from "@expo/vector-icons";
import { User } from "../../model/User";
import { ActivityLogger } from "../../log/ActivityLogger";

type DeleteProps = {
  onCancelButtonPress?: () => void;
  onDeleteButtonPress?: () => void;
  registerType?: string;
  roleName?: string;
  personNames: string | string[];
};

export const ReceptionInfoDeleteDialog = (props: DeleteProps) => {
  const [isModalVisible, setModalVisible] = useState(true);
  const { registerType, roleName, personNames } = props;
  const [deletedNameArray, setDeletedNameArray] = useState<string[]>([]);

  useEffect(() => {
    ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoDeleteDialog', 'useEffect', 'screen open');
  }, []);

  useEffect(() => {
    if (typeof personNames === "string") {
      setDeletedNameArray([personNames]);
    } else {
      setDeletedNameArray(personNames);
    }
  }, [personNames]);

  let titleText = "";
  if (registerType === "individual") {
    titleText = `＜${deletedNameArray[0]}＞を削除しますか？`;
  } else if (roleName === "受付代表") {
    titleText = `＜${deletedNameArray[0]}＞を削除すると、同時に受付を行った人も削除されますがよろしいですか？`;
  } else {
    titleText = `＜${deletedNameArray[0]}＞を削除しますか？`;
  }

  return (
    <View>
      {isModalVisible && (
        <ModalComponent
          text={titleText}
          firstButtonText="キャンセル"
          secondButtonText="削除する"
          secondButtonType="ButtonMDanger"
          onFirstButtonPress={props.onCancelButtonPress}
          onSecondButtonPress={props.onDeleteButtonPress}
          toggleModal={props.onCancelButtonPress}
          secondButtonWidth={104}
          secondBtnTextWidth={64}
        >
          <View style={styles.bodyContainer}>
            {(registerType === "individual" ||
              (registerType === "group" && roleName === "一緒に受付")) && (
              <HiraginoKakuText style={styles.bodyText} normal>
                削除された人はもとに戻すことはできません。
              </HiraginoKakuText>
            )}
            {registerType === "group" && roleName === "受付代表" && (
              <HiraginoKakuText style={styles.bodyText} normal>
                「受付をした人」を削除すると、同時に受付を行った人も削除されます。削除された人はもとに戻すことはできません。
                {"\n"}
                {"\n"}
                以下の人が削除されます。{"\n"}
                {deletedNameArray.length > 1 &&
                  deletedNameArray.slice(1).map((name, index) => (
                    <View key={index} style={styles.bulletTextContainer}>
                      <FontAwesome name="circle" style={styles.bullet} />
                      <HiraginoKakuText style={styles.bodyText} normal>
                        ＜{name}＞
                      </HiraginoKakuText>
                    </View>
                  ))}
                {"\n"}
              </HiraginoKakuText>
            )}
          </View>
        </ModalComponent>
      )}
    </View>
  );
};
