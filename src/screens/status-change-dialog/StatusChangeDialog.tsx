import React, { useEffect, useState } from "react";
import { View } from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import ModalComponent from "../../components/basics/ModalComponent";
import styles from "./StatusChangeDialogStyles";
import { User } from "../../model/User";
import { ActivityLogger } from "../../log/ActivityLogger";

type StatusChangeProps = {
  statusName?: string;
  onCancelButtonPress?: () => void;
  onChangeButtonPress?: () => void;
};
export const StatusChangeDialog = (props: StatusChangeProps) => {
  const [isModalVisible, setModalVisible] = useState(true);

  useEffect(() => {
    ActivityLogger.insertInfoLogEntry(new User(), 'ReceptionInfoSaveDialog', 'useEffect', 'screen open');
  }, []);

  var status = props.statusName; //受付可能 or 受付終了 or アーカイブ

  var titleText = "";
  titleText =
    status == "準備中"
      ? "「準備中」に変更しますか？"
      : status == "受付可能"
      ? "「受付可能」に変更しますか？"
      : status == "受付終了"
      ? "「受付終了」に変更しますか？"
      : "「アーカイブ」に変更しますか？";
  return (
    <View>
      {isModalVisible && (
        <ModalComponent
          text={titleText}
          firstButtonText="キャンセル"
          secondButtonText="変更する"
          secondButtonType="ButtonMPrimary"
          onFirstButtonPress={props.onCancelButtonPress}
          onSecondButtonPress={props.onChangeButtonPress}
          toggleModal={props.onCancelButtonPress}
          secondButtonWidth={104}
          secondBtnTextWidth={64}
        >
          <View style={styles.bodyContainer}>
            {status == "準備中" && (
              <HiraginoKakuText style={styles.bodyText} normal>
                受付アプリに表示されなくなります。
                {"\n"}
                受付を開始する場合は「受付可能」に変更してください。
              </HiraginoKakuText>
            )}
            {status == "受付可能" && (
              <HiraginoKakuText style={styles.bodyText} normal>
                受付アプリに表示し、いつでも受付ができるようにします。
              </HiraginoKakuText>
            )}
            {status == "受付終了" && (
              <HiraginoKakuText style={styles.bodyText} normal>
                受付終了にすると、受付アプリでは非表示になるため、受付は行えません。
                {"\n"}
                再度、受付をしたい場合は「受付可能」に変更してください。
              </HiraginoKakuText>
            )}
            {status == "アーカイブ" && (
              <HiraginoKakuText style={styles.bodyText} normal>
                アーカイブにすると、一覧画面では非表示になります。{"\n"}
                一覧画面に表示したい場合は、検索部分の「受付状況」から「アーカイブ」を選択して検索してください。
              </HiraginoKakuText>
            )}
          </View>
        </ModalComponent>
      )}
    </View>
  );
};
