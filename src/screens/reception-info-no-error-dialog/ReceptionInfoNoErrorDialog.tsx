import React, { useState } from "react";
import { View } from "react-native";
import ModalComponent from "../../components/basics/ModalComponent";
import { HiraginoKakuText } from "../../components/StyledText";
type ReceptionSavelDialogProps = {
    onUnsaveButtonPress?: () => void;
    onSaveButtonPress?: () => void;
    onCancelButtonPress?: () => void;
};
export const ReceptionInfoNoErrorDialog = (props: ReceptionSavelDialogProps) => {
    const [isModalVisible, setModalVisible] = useState(true);

    return (
        <View>
            <ModalComponent
                text="変更内容を保存しますか？"
                firstButtonText="保存しない"
                secondButtonText="保存する"
                leftButtonText="キャンセル"
                leftButtonVisible={true}
                onFirstButtonPress={props.onUnsaveButtonPress}
                onSecondButtonPress={props.onSaveButtonPress}
                onLeftButtonPress={props.onCancelButtonPress}
                toggleModal={props.onCancelButtonPress}
                leftButtonType="ButtonMGray"
                firstButtonType="ButtonMSecondary"
                secondButtonWidth={104}
                secondBtnTextWidth={64}
            >
                <View>
                    <HiraginoKakuText normal style={{ fontSize: 16 }}>
                        保存せずに別の人に切り替えると、変更内容は破棄されます。
                    </HiraginoKakuText>
                </View>
            </ModalComponent>
        </View>
    );
};
