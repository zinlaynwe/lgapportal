import React, { useEffect, useState } from "react";
import { View } from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import ModalComponent from "../../components/basics/ModalComponent";
import styles from "./EventCreateCancelDialogStyles";
import { User } from "../../model/User";
import { ActivityLogger } from "../../log/ActivityLogger";

type EventCreatCancelDialogProps = {
  onCancelButtonPress?: () => void;
  onFinishButtonPress?: () => void;
};

export const EventCreateCancelDialog = (props: EventCreatCancelDialogProps) => {
  const [isModalVisible, setModalVisible] = useState(true);

  useEffect(() => {
    ActivityLogger.insertInfoLogEntry(new User(), 'EventCreateCancelDialog', 'useEffect', 'screen open');
  }, []);
  
  return (
    <View>
      {isModalVisible && (
        <ModalComponent
          text="イベント作成を終了しますか？"
          firstButtonVisible={false}
          leftButtonVisible={true}
          leftButtonText="キャンセル"
          secondButtonText="終了する"
          onSecondButtonPress={props.onFinishButtonPress}
          onLeftButtonPress={props.onCancelButtonPress}
          toggleModal={props.onCancelButtonPress}
          secondButtonWidth={104}
          secondBtnTextWidth={64}
        >
          <View style={styles.bodyContainer}>
            <HiraginoKakuText style={styles.bodyText} normal>
              イベントは作成されません。
            </HiraginoKakuText>
          </View>
        </ModalComponent>
      )}
    </View>
  );
};
