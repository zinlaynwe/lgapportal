import { Platform, StyleSheet } from "react-native";
import { colors } from "../../styles/color";
import {
  LabelLargeBold,
  LabelLargeRegular,
  LabelMediumRegular,
  LabelSmallBold,
} from "../../styles/typography";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { fonts } from "../../styles/font";

const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    height: "100%",
    backgroundColor: "#EEEFF1",
  },
  container: {
    ...Platform.select({
      web: {
        height: hp("90"),
      },
    }),
    paddingHorizontal: 120,
    paddingTop: hp("4.1"),
    paddingBottom: hp("12.25"),
    gap: hp("2.45"),
  },
  scrollContainer: {
    height: hp("90"),
  },
  scrollViewContent: {
    flex: 1,
  },
  bodyContainer: {
    backgroundColor: "#FFFFFF",
    borderRadius: 8,
    paddingHorizontal: 40,
    paddingTop: hp("3.25"),
    paddingBottom: hp("6.55"),
    gap: hp("3.25"),
  },
  eventTitleContainer: {
    gap: hp("0.8"),
  },
  eventHeadingLblContainer: {
    gap: hp("0.8"),
    flexDirection: "row",
  },
  statusLabelContainer: {
    gap: hp("0.4"),
    width: 32,
    height: 20,
  },
  eventTimeContainer: {
    gap: hp("1.65"),
    zIndex: 1,
  },
  eventTimeLblGpContainer: {
    gap: hp("0.8"),
  },
  eventVenueContainer: {
    gap: hp("0.8"),
  },
  eventReceptionContainer: {
    gap: hp("0.8"),
  },
  dateTimeSelectContainer: {
    width: "67.37%",
    flexDirection: "row",
    justifyContent: "center",
  },
  DateContainer: {
    gap: hp("0.8"),
    flex: 4,
  },
  endDateContainer: {
    gap: hp("0.8"),
    flex: 4,
    zIndex: -1,
  },
  calendarIconContainer: {
    right: 10,
    width: 24,
    height: 24,
    position: "absolute",
  },
  venueInputBoxesContainer: {
    gap: 16,
  },
  venueBtnContainer: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: "#D4D7DE",
    ...Platform.select({
      web: {
        paddingVertical: hp("0.4"),
        paddingHorizontal: hp("0.8"),
      },
    }),
    gap: hp("0.8"),
    flexDirection: "row",
    width: 128,
    height: 32,
    alignItems: "center",
    justifyContent: "center",
  },
  venueCrossIconContainer: {
    borderRadius: 4,
    gap: 8,
    padding: hp("0.8"),
  },
  eventReceptionBtnContainer: {
    gap: hp("0.8"),
    width: 200,
    flexDirection: "row",
  },
  createBtnContainer: {
    flex: 1,
    gap: hp("0.8"),
    height: 44,
  },
  statusBox: {
    width: 32,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: "#2E81FF",
    backgroundColor: "#FFFFFF",
    ...Platform.select({
      web: {
        width: 33,
        height: 20,
        marginTop: 3,
        paddingHorizontal: hp("0.1"),
      },
    }),
    gap: 4,
    alignItems: "center",
    justifyContent: "center",
  },
  eventTitleStatusBox: {
    borderColor: "#E90C0C",
  },
  eventInputBox: {
    height: hp("4.5"),
    borderWidth: 1,
    borderRadius: 6,
    borderColor: "#B8BCC7",
    paddingHorizontal: 9,
    gap: hp("0.8"),
    color: "#1E2129",
    fontSize: LabelLargeRegular.size,
    fontWeight: "300",
    textAlignVertical: "center",
  },
  datePickerContainer: {
    justifyContent: "center",
  },
  dateInput: {
    width: "100%",
    height: hp("4.5"),
    borderWidth: 1,
    borderRadius: 6,
    gap: 8,
    borderColor: colors.borderColor,
    backgroundColor: colors.secondary,
    paddingVertical: 7,
    paddingHorizontal: 8,
    fontSize: LabelLargeRegular.size,
    fontFamily: fonts.FontRegular.fontFamily,
    fontWeight: "300",
  },
  venueInputBox: {
    flexDirection: "row",
  },
  venueInput: {
    flex: 1,
  },
  bodyText: {
    color: colors.textColor,
  },
  LabelLargeBold: {
    fontSize: LabelLargeBold.size,
    fontWeight: "600",
  },
  eventLabel: {
    color: "#515867",
    fontWeight: "300",
    fontSize: LabelMediumRegular.size,
    lineHeight: LabelLargeRegular.lineHeight,
  },
  statusLabelText: {
    height: 20,
    ...Platform.select({
      web: {
        height: 18,
      },
    }),
    color: "#2E81FF",
    fontSize: LabelSmallBold.size,
    fontWeight: "600",
    lineHeight: LabelSmallBold.lineHeight,
  },
  eventRedStatusLblText: {
    color: "#E90C0C",
  },
  venueBtnText: {
    ...Platform.select({
      web: {
        width: 80,
        height: 24,
      },
    }),
  },
  createBtnText: {
    width: 64,
    color: "#FFFFFF",
  },
  horizontalLine: {
    borderBottomWidth: 1,
    borderColor: "#D4D7DE",
  },
  waveDash: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: 24,
  },
  calendarIcon: {
    width: 20,
    height: 22,
    position: "absolute",
    top: 0,
    right: 0,
  },
  receptionBtn: {
    height: 44,
    width: 88,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: "#D4D7DE",
    paddingVertical: hp("0.4"),
    paddingHorizontal: hp("1.24"),
    gap: hp("0.8"),
    alignItems: "center",
    justifyContent: "center",
  },
  receptionText: {
    width: 48,
    color: colors.textColor,
  },
  buttonText: {
    width: 64,
    color: colors.textColor,
  },
  acceptBtn: {
    width: 104,
  },
  createBtn: {
    width: 104,
    height: 44,
    backgroundColor: "#346DF4",
    position: "absolute",
    top: 0,
    right: 0,
    borderRadius: hp("0.35"),
    alignItems: "center",
    justifyContent: "center",
  },
  btnSelected: {
    borderColor: "#346DF4",
    borderWidth: 2,
    backgroundColor: colors.neuralLightBlueColor,
  },
  btnCloseContainer: {
    width: 96,
    height: 32,
    borderRadius: 4,
    paddingVertical: 4,
    paddingHorizontal: 8,
    gap: hp("0.8"),
  },
  errText: {
    fontWeight: "300",
    fontSize: LabelMediumRegular.size,
    color: colors.danger,
    zIndex: -1,
  },
});

export default styles;
