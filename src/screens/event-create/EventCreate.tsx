// EventCreate.tsx
import React, { useState, useRef, useEffect } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
  TextInput,
  ScrollView,
  Pressable,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";
import styles from "./EventCreateStyles";
import { MaterialIcons, Entypo } from "@expo/vector-icons";
import { HiraginoKakuText } from "../../components/StyledText";
import { Header } from "../../components/basics/header";
import { colors } from "../../styles/color";
import { CustomButton } from "../../components/basics/Button";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { EventCreateCancelDialog } from "../event-create-cancel-dialog/EventCreateCancelDialog";
import { CustomCalendar } from "../../components/basics/Calendar";
import { format, parse } from "date-fns";
import {
  fetchEventIdByTitle,
  fetchEventStatusData,
  insertEventData,
  insertVenueData,
} from "./EventCreateService";
import { EventDetailParams } from "../event-detail/EventDetailParams";
import { EventListParams } from "../event-list/EventListParams";
import { EventCreateParams } from "./EventCreateParams";
import { ActivityLogger } from "../../log/ActivityLogger";

type Props = {
  navigation: NavigationProp<any, any>;
};
type Params = {
  eventListParams: EventListParams;
  eventCreateParams: EventCreateParams;
};
export const EventCreate = ({ navigation }: Props) => {
  const route = useRoute();
  const { eventListParams, eventCreateParams } = route.params as Params;

  const [inputBoxes, setInputBoxes] = useState([""]);
  const [selectedButton, setSelectedButton] = useState<string>("1");
  const [isCreateCancelModalVisible, setIsCreateCancelModalVisible] =
    useState(false);
  const [isStartDateCalendarVisible, setStartDateCalendarVisible] =
    useState(false);
  const [isEndDateCalendarVisible, setEndDateCalendarVisible] = useState(false);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const startDateInputRef = useRef(null);
  const endDateInputRef = useRef(null);
  const startDateRef = useRef(null);
  const endDateRef = useRef(null);

  const [title, setTitle] = useState<string>("");
  const [titleErrMsg, setTitleErrMsg] = useState("");
  const [startDateErrMsg, setstartDateErrMsg] = useState("");
  const [endDateErrMsg, setendDateErrMsg] = useState("");

  useEffect(() => {
    ActivityLogger.insertInfoLogEntry(eventCreateParams.user, 'EventCreate', 'useEffect', 'screen open');
    handleEventStatusRead();
  }, []);

  const handleTextInputChange = (text: string, fieldName: string) => {
    if (fieldName == "title") {
      setTitle(text);
      setTitleErrMsg("");
    }
    if (fieldName == "startDate") {
      setStartDate(text.trim());
      setstartDateErrMsg("");
    }
    if (fieldName == "endDate") {
      setEndDate(text.trim());
      setendDateErrMsg("");
    }
  };

  const handleVenueInputChange = (text: string, index: number) => {
    const updatedInputBoxes = [...inputBoxes];
    updatedInputBoxes[index] = text;
    setInputBoxes(updatedInputBoxes);
  };
  const addInputBox = (event: any) => {
    setInputBoxes([...inputBoxes, ""]);
    closeCalendar(event);
  };

  const removeInputBox = (removedIndex: number) => {
    setInputBoxes(inputBoxes.filter((_, index) => index !== removedIndex));
    setStartDateCalendarVisible(false);
    setEndDateCalendarVisible(false);
  };

  const handleTitleBlur = () => {
    setTitle(title.trim());
    if (title === "") {
      setTitleErrMsg("イベント名を入力してください");
    } else {
      setTitleErrMsg("");
    }
  };
  const handleStartDateBlur = () => {
    if (!startDate && endDate) {
      setstartDateErrMsg("開始日を選択してください");
      setendDateErrMsg("");
    } else if (startDate && !endDate) {
      setendDateErrMsg("終了日を選択してください");
      setstartDateErrMsg("");
    } else if (!endDate && !startDate) {
      setstartDateErrMsg("開始日を選択してください");
    } else if (endDate && startDate) {
      if (startDate > endDate) {
        setendDateErrMsg("開始日以降の日付を入力してください");
      } else {
        setendDateErrMsg("");
      }
    } else {
      setstartDateErrMsg("");
      setendDateErrMsg("");
    }
  };
  const handleEndDateBlur = () => {
    if (startDate && !endDate) {
      setendDateErrMsg("終了日を選択してください");
      setstartDateErrMsg("");
    } else if (!startDate && endDate) {
      setstartDateErrMsg("開始日を選択してください");
      setendDateErrMsg("");
    } else if (!endDate && !startDate) {
      setendDateErrMsg("終了日を選択してください");
    } else if (endDate && startDate) {
      if (startDate > endDate) {
        setendDateErrMsg("開始日以降の日付を入力してください");
      } else {
        setendDateErrMsg("");
      }
    } else {
      setstartDateErrMsg("");
      setendDateErrMsg("");
    }
  };
  const handleVenueBlur = (text: string, index: number) => {
    const updatedInputBoxes = [...inputBoxes];
    updatedInputBoxes[index] = text.trim();
    setInputBoxes(updatedInputBoxes);
  };

  const handleButtonPressMain = () => {
    if (title && !startDate && !endDate) {
      handleEventInsert();
      setstartDateErrMsg("");
      setendDateErrMsg("");
    } else if (title && startDate && endDate) {
      if (startDate <= endDate) {
        handleEventInsert();
        setstartDateErrMsg("");
        setendDateErrMsg("");
      }
    } else {
      if (!title) {
        handleTitleBlur();
      }
      if (!startDate && title) {
        handleStartDateBlur();
      } else if (!startDate && endDate) {
        handleStartDateBlur();
      } else {
        if (!title && !startDate && endDate) {
          setstartDateErrMsg("");
        } else if (title && !startDate && !endDate) {
          setstartDateErrMsg("開始日を選択してください");
        }
      }
      if (!endDate && title) {
        handleEndDateBlur();
      } else if (!endDate && startDate) {
        handleEndDateBlur();
      } else {
        if (!title && startDate && !endDate) {
          setendDateErrMsg("");
        } else if (title && !startDate && !endDate) {
          setendDateErrMsg("終了日を選択してください");
        }
      }
    }

    setStartDateCalendarVisible(false);
    setEndDateCalendarVisible(false);
  };

  const handleButtonPress = (selectedButton: string) => {
    if (selectedButton === "create") {
      setSelectedButton("");
    } else {
      setSelectedButton(selectedButton);
    }
    setStartDateCalendarVisible(false);
    setEndDateCalendarVisible(false);
  };

  const handleClose = () => {
    setStartDateCalendarVisible(false);
    setEndDateCalendarVisible(false);
    setIsCreateCancelModalVisible(true);
    navigation.navigate("EventCreate", {
      eventCreateParams: eventCreateParams,
    });
    ActivityLogger.insertInfoLogEntry(eventCreateParams.user, 'EventCreate', 'handleClose', 'transition', 'EventCreate', eventCreateParams);
  };
  
  const handleCancelButton = () => {
    setIsCreateCancelModalVisible(false);
    navigation.navigate("EventCreate", {
      eventCreateParams: eventCreateParams,
    });
    ActivityLogger.insertInfoLogEntry(eventCreateParams.user, 'EventCreate', 'handleCancelButton', 'transition', 'EventCreate', eventCreateParams);
  };
  const handleFinishButton = () => {
    setIsCreateCancelModalVisible(false);

    const eventListParams = new EventListParams();
    eventListParams.user = eventCreateParams.user;

    navigation.navigate("EventList", { eventListParams });
    ActivityLogger.insertInfoLogEntry(eventCreateParams.user, 'EventCreate', 'handleFinishButton', 'transition', 'EventList', eventListParams);
  };

  const handleStartDateCalendarPress = (event: any) => {
    (startDateInputRef.current as any).focus();
    setStartDateCalendarVisible(!isStartDateCalendarVisible);
    setEndDateCalendarVisible(false);
    setstartDateErrMsg("");
  };

  const handleEndDateCalendarPress = (event: any) => {
    (endDateInputRef.current as any).focus();
    setEndDateCalendarVisible(!isEndDateCalendarVisible);
    setStartDateCalendarVisible(false);
    setendDateErrMsg("");
  };

  const handleStartDateSelect = (date: any) => {
    startDate == date ? setStartDate("") : setStartDate(date);
    setStartDateCalendarVisible(false);
    if (endDate) {
      if (date > endDate) {
        setendDateErrMsg("開始日以降の日付を入力してください");
        setstartDateErrMsg("");
      } else {
        setendDateErrMsg("");
        setstartDateErrMsg("");
      }
    } else {
      if (date) {
        setstartDateErrMsg("");
        handleEndDateBlur();
      }
    }
  };

  const handleEndDateSelect = (date: any) => {
    endDate == date ? setEndDate("") : setEndDate(date);
    setEndDateCalendarVisible(false);
    if (date < startDate) {
      setendDateErrMsg("開始日以降の日付を入力してください");
    } else {
      setendDateErrMsg("");
      if (!startDate) {
        handleStartDateBlur();
      }
    }
  };

  const closeCalendar = (event: any) => {
    if (
      event.nativeEvent.target != startDateInputRef.current &&
      event.nativeEvent.target != startDateRef
    ) {
      if (isStartDateCalendarVisible) {
        setStartDateCalendarVisible(false);
      }
    }
    if (
      event.nativeEvent.target != endDateInputRef.current &&
      event.nativeEvent.target != endDate
    ) {
      if (isEndDateCalendarVisible) {
        setEndDateCalendarVisible(false);
      }
    }
  };
  const handleEventTitleFocus = (event: any) => {
    closeCalendar(event);
  };

  // AWS
  const [eventStatus, setEventStatus] = useState<any[]>([]);
  const [message, setMessage] = useState<string>();

  const handleEventStatusRead = async () => {
    try {
      const result = await fetchEventStatusData();
      const message = result.message;
      if (message === "success") {
        setMessage("Optained Successfully!!");
        setEventStatus(result.data);
      } else {
        setMessage(message);
      }
    } catch (error) {
      console.error("Error from EventCreateService:", error);
    }
  };

  //INSERTION EVENT
  const handleEventInsert = async () => {
    try {
      const result = await insertEventData(
        "242152",
        0,
        eventCreateParams.user.userId,
        title.trim(),
        startDate,
        endDate,
        selectedButton
      );

      const message = result.message;
      if (message === "success") {
        handleVenueInsert();
        setMessage("Inserted Successfully!!");
      } else {
        setMessage(message);
      }
    } catch (error) {
      console.error("Error from EventCreateService:", error);
    }
  };

  const handleVenueInsert = async () => {
    const resultId = await fetchEventIdByTitle(title.trim()); // get the event id from event table []
    var getEventId = resultId.data[0].event_id;
    values = inputBoxes[0].trim().toString();

    const message = resultId.message;
    if (message === "success") {
      try {
        if (inputBoxes.length <= 1) {
          if (inputBoxes[0].trim() == "") {
            values = "会場1";
          } else {
            values = inputBoxes[0].trim().toString();
          }

          const result = await insertVenueData(
            "242152",
            getEventId,
            0,
            eventCreateParams.user.userId,
            values
          );
          const message = result.message;
          if (message === "success") {
            setTitle("");
            setStartDate("");
            setEndDate("");
            setInputBoxes([""]);
            setSelectedButton("1");
            setMessage("Inserted Successfully!!");

            const eventDetailParams = new EventDetailParams();
            eventDetailParams.user = eventCreateParams.user;
            eventDetailParams.eventId = getEventId;
            eventDetailParams.toastMsg = "イベントを作成しました";
            if (selectedButton == "1") {
              navigation.navigate("EventDetail", {
                eventDetailParams,
              });
              ActivityLogger.insertInfoLogEntry(eventCreateParams.user, 'EventCreate', 'handleVenueInsert', 'transition', 'EventDetail', eventDetailParams);
            } else if (selectedButton == "2") {
              navigation.navigate("EventDetail", {
                eventDetailParams,
              });
              ActivityLogger.insertInfoLogEntry(eventCreateParams.user, 'EventCreate', 'handleVenueInsert', 'transition', 'EventDetail', eventDetailParams);
            }
          } else {
            setMessage(message);
          }
        } else if (inputBoxes.length >= 2) {
          let allEmpty = true;
          for (let i = 0; i < inputBoxes.length; i++) {
            if (inputBoxes[i].trim() !== "") {
              allEmpty = false;
              break;
            }
          }
          if (allEmpty) {
            values = "会場1";
            const result = await insertVenueData(
              "242152",
              getEventId,
              0,
              eventCreateParams.user.userId,
              values
            );
            const message = result.message;
            if (message === "success") {
              setTitle("");
              setStartDate("");
              setEndDate("");
              setInputBoxes([""]);
              setSelectedButton("1");
              setMessage("Inserted Successfully!!");
              const eventDetailParams = new EventDetailParams();
              eventDetailParams.user = eventCreateParams.user;
              eventDetailParams.eventId = getEventId;
              eventDetailParams.toastMsg = "イベントを作成しました";
              if (selectedButton == "1") {
                navigation.navigate("EventDetail", {
                  eventDetailParams,
                });
                ActivityLogger.insertInfoLogEntry(eventCreateParams.user, 'EventCreate', 'handleVenueInsert', 'transition', 'EventDetail', eventDetailParams);
              } else if (selectedButton == "2") {
                navigation.navigate("EventDetail", {
                  eventDetailParams,
                });
                ActivityLogger.insertInfoLogEntry(eventCreateParams.user, 'EventCreate', 'handleVenueInsert', 'transition', 'EventDetail', eventDetailParams);
              }
            } else {
              setMessage(message);
            }
          } else {
            for (let i = 0; i < inputBoxes.length; i++) {
              var values = inputBoxes[i].trim().toString();

              if (values != "") {
                const result = await insertVenueData(
                  "242152",
                  getEventId,
                  0,
                  eventCreateParams.user.userId,
                  values
                );
                const message = result.message;
                if (message === "success") {
                  setTitle("");
                  setStartDate("");
                  setEndDate("");
                  setInputBoxes([""]);
                  setSelectedButton("1");
                  setMessage("Inserted Successfully!!");
                  const eventDetailParams = new EventDetailParams();
                  eventDetailParams.user = eventCreateParams.user;
                  eventDetailParams.eventId = getEventId;
                  eventDetailParams.toastMsg = "イベントを作成しました";

                  navigation.navigate("EventDetail", {
                    eventDetailParams,
                  });
                  ActivityLogger.insertInfoLogEntry(eventCreateParams.user, 'EventCreate', 'handleVenueInsert', 'transition', 'EventDetail', eventDetailParams);
                } else {
                  setMessage(message);
                }
              }
            }
          }
        }
      } catch (error) {
        console.error("Error from EventCreateService:", error);
      }
      setMessage("Inserted Successfully!!");
    } else {
      setMessage(message);
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <Header middleTitleName="イベント作成" buttonName="">
          <CustomButton
            style={styles.btnCloseContainer}
            text="閉じる"
            type="ButtonSDefault"
            icon={<Entypo name="cross" size={24} color={colors.primary} />}
            iconPosition="front"
            onPress={handleClose}
          />
        </Header>
        <ScrollView style={styles.scrollViewContent}>
          <TouchableWithoutFeedback onPress={closeCalendar}>
            <View style={styles.container}>
              <View style={styles.bodyContainer}>
                <View style={styles.eventTitleContainer}>
                  <View style={styles.eventHeadingLblContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      イベント名
                    </HiraginoKakuText>

                    <View style={styles.statusLabelContainer}>
                      <View
                        style={[styles.statusBox, styles.eventTitleStatusBox]}
                      >
                        <HiraginoKakuText
                          style={[
                            styles.statusLabelText,
                            styles.eventRedStatusLblText,
                          ]}
                        >
                          必須
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <View>
                    <TextInput
                      placeholder="イベントタイトル"
                      placeholderTextColor={colors.placeholderTextColor}
                      style={styles.eventInputBox}
                      onFocus={closeCalendar}
                      value={title}
                      onChangeText={(text) =>
                        handleTextInputChange(text, "title")
                      }
                      onBlur={handleTitleBlur}
                    />
                  </View>
                  {typeof titleErrMsg === "string" &&
                    titleErrMsg.length > 0 && (
                      <HiraginoKakuText style={styles.errText} normal>
                        {titleErrMsg}
                      </HiraginoKakuText>
                    )}
                </View>
                <View style={styles.eventTimeContainer}>
                  <View style={styles.eventTimeLblGpContainer}>
                    <View style={styles.eventHeadingLblContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        イベント期間
                      </HiraginoKakuText>
                      <View style={styles.statusLabelContainer}>
                        <View style={styles.statusBox}>
                          <HiraginoKakuText style={styles.statusLabelText}>
                            任意
                          </HiraginoKakuText>
                        </View>
                      </View>
                    </View>
                    <HiraginoKakuText normal style={styles.eventLabel}>
                      開始日と終了日を入力してください
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.dateTimeSelectContainer}>
                    <View style={styles.DateContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        開始日
                      </HiraginoKakuText>
                      <View style={styles.datePickerContainer}>
                        <TextInput
                          ref={startDateInputRef}
                          style={styles.dateInput}
                          placeholder="日付を選択"
                          placeholderTextColor={colors.placeholderTextColor}
                          value={
                            startDate != ""
                              ? format(new Date(startDate), "yyyy/MM/dd")
                              : startDate
                          }
                          onPressIn={handleStartDateCalendarPress}
                          onPointerDown={handleStartDateCalendarPress}
                          showSoftInputOnFocus={false}
                          onTouchStart={() => Keyboard.dismiss()}
                          editable={false}
                          onChangeText={(text) =>
                            handleTextInputChange(text, "startDate")
                          }
                          onBlur={handleStartDateBlur}
                        />
                        <Pressable
                          ref={startDateRef}
                          style={styles.calendarIconContainer}
                          onPress={handleStartDateCalendarPress}
                        >
                          <MaterialIcons
                            name="calendar-today"
                            size={22}
                            color={colors.activeCarouselColor}
                            style={styles.calendarIcon}
                          />
                        </Pressable>
                        {isStartDateCalendarVisible && (
                          <CustomCalendar
                            selectedDate={startDate}
                            onDateSelect={handleStartDateSelect}
                          />
                        )}
                      </View>

                      {typeof startDateErrMsg === "string" &&
                        startDateErrMsg.length > 0 && (
                          <HiraginoKakuText style={styles.errText} normal>
                            {startDateErrMsg}
                          </HiraginoKakuText>
                        )}
                    </View>

                    <View style={styles.waveDash}>
                      <Text style={styles.LabelLargeBold}> ~ </Text>
                    </View>

                    <View style={styles.endDateContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        終了日
                      </HiraginoKakuText>
                      <View style={styles.datePickerContainer}>
                        <TextInput
                          ref={endDateInputRef}
                          style={styles.dateInput}
                          placeholder="日付を選択"
                          placeholderTextColor={colors.placeholderTextColor}
                          value={
                            endDate != ""
                              ? format(new Date(endDate), "yyyy/MM/dd")
                              : endDate
                          }
                          onPressIn={handleEndDateCalendarPress}
                          onPointerDown={handleEndDateCalendarPress}
                          showSoftInputOnFocus={false}
                          onTouchStart={() => Keyboard.dismiss()}
                          editable={false}
                          onChangeText={(text) =>
                            handleTextInputChange(text, "endDate")
                          }
                          onBlur={handleEndDateBlur}
                        />
                        <Pressable
                          ref={endDateRef}
                          onPress={handleEndDateCalendarPress}
                          style={styles.calendarIconContainer}
                        >
                          <MaterialIcons
                            name="calendar-today"
                            size={22}
                            color={colors.activeCarouselColor}
                            style={styles.calendarIcon}
                          />
                        </Pressable>
                        {isEndDateCalendarVisible && (
                          <CustomCalendar
                            selectedDate={endDate}
                            onDateSelect={handleEndDateSelect}
                          />
                        )}
                      </View>
                      {typeof endDateErrMsg === "string" &&
                        endDateErrMsg.length > 0 && (
                          <HiraginoKakuText style={styles.errText} normal>
                            {endDateErrMsg}
                          </HiraginoKakuText>
                        )}
                    </View>
                  </View>
                </View>
                <View style={styles.eventVenueContainer}>
                  <View style={styles.eventHeadingLblContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      会場
                    </HiraginoKakuText>
                    <View style={styles.statusLabelContainer}>
                      <View style={styles.statusBox}>
                        <HiraginoKakuText style={styles.statusLabelText}>
                          任意
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <View style={styles.venueInputBoxesContainer}>
                    {inputBoxes.map((venue, index) => (
                      <View key={index} style={styles.venueInputBox}>
                        <TextInput
                          placeholder="会場名"
                          placeholderTextColor={colors.placeholderTextColor}
                          style={[styles.eventInputBox, styles.venueInput]}
                          onFocus={closeCalendar}
                          value={venue}
                          onChangeText={(text) =>
                            handleVenueInputChange(text, index)
                          }
                          onBlur={() => handleVenueBlur(venue, index)}
                        />
                        {inputBoxes.length > 1 && (
                          <TouchableOpacity
                            onPress={() => removeInputBox(index)}
                            style={styles.venueCrossIconContainer}
                          >
                            <MaterialIcons
                              name="close"
                              size={22}
                              color="#515867"
                            />
                          </TouchableOpacity>
                        )}
                      </View>
                    ))}
                    <TouchableOpacity
                      style={styles.venueBtnContainer}
                      onPress={addInputBox}
                    >
                      <MaterialIcons name="add" size={22} color="#515867" />
                      <HiraginoKakuText
                        style={[
                          styles.bodyText,
                          styles.LabelLargeBold,
                          styles.venueBtnText,
                        ]}
                      >
                        会場を追加
                      </HiraginoKakuText>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.horizontalLine} />
                <View style={styles.eventReceptionContainer}>
                  <View style={styles.eventHeadingLblContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      イベントの受付状況
                    </HiraginoKakuText>
                    <View style={styles.statusLabelContainer}>
                      <View
                        style={[styles.statusBox, styles.eventTitleStatusBox]}
                      >
                        <HiraginoKakuText
                          style={[
                            styles.statusLabelText,
                            styles.eventRedStatusLblText,
                          ]}
                        >
                          必須
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <HiraginoKakuText normal style={styles.eventLabel}>
                    受付可能にすると受付アプリに表示されます
                  </HiraginoKakuText>
                  <View style={styles.eventReceptionBtnContainer}>
                    {eventStatus.map((eventStatus) => (
                      <TouchableOpacity
                        key={eventStatus.event_status_code}
                        style={[
                          styles.receptionBtn,
                          selectedButton === eventStatus.event_status_code &&
                            styles.btnSelected,
                          eventStatus.name === "受付可能" && styles.acceptBtn,
                        ]}
                        onPress={() =>
                          handleButtonPress(eventStatus.event_status_code)
                        }
                      >
                        <Text
                          style={[
                            eventStatus.name === "受付可能"
                              ? styles.buttonText
                              : styles.receptionText,
                            styles.LabelLargeBold,
                          ]}
                        >
                          {eventStatus.name}
                        </Text>
                      </TouchableOpacity>
                    ))}
                  </View>
                </View>
              </View>
              <View style={styles.createBtnContainer}>
                <TouchableOpacity
                  style={[
                    styles.createBtn,
                    selectedButton === "create" && styles.btnSelected,
                  ]}
                  onPress={handleButtonPressMain}
                >
                  <HiraginoKakuText
                    style={[styles.createBtnText, styles.LabelLargeBold]}
                  >
                    作成する
                  </HiraginoKakuText>
                </TouchableOpacity>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>

        {isCreateCancelModalVisible && (
          <EventCreateCancelDialog
            onCancelButtonPress={handleCancelButton}
            onFinishButtonPress={handleFinishButton}
          ></EventCreateCancelDialog>
        )}
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};
