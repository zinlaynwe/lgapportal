import { User } from "../../model/User";

export class EventCreateParams implements Params {
  private _user: User = new User();
  private _pgType: string = "";

  //getter
  get user(): User {
    return this._user;
  }
  get pgType(): string {
    return this._pgType;
  }

  //setter
  set user(value: User) {
    this._user = value;
  }
  set pgType(value: string) {
    this._pgType = value;
  }
  
  getAllValuesAsString(): string {
    let paramsString = '[ClassName=EventCreateParams';
    paramsString += ',_user=' + this._user.getAllValuesAsString();
    paramsString += ',_pgType=' + this._pgType;
    paramsString += ']';
    return paramsString
  }
}
