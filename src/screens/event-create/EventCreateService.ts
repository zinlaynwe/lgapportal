import { executeQuery } from "../../aws/db/dbOperation";
import { getCurrentJapanTime } from "../../environments/TimeUtils";
import { ActivityLogger } from "../../log/ActivityLogger";
import { User } from "../../model/User";

// get Data from Event_Status
export const fetchEventStatusData = async () => {
  const method = "POST";
  const queryString =
    "SELECT event_status_code,name FROM event_status WHERE is_show_on_new_entry ORDER BY event_status_code";
  ActivityLogger.insertInfoLogEntry(new User(), 'EventCreate', 'fetchEventStatusData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

//insert event
export const insertEventData = async (
  cityCode: string,
  historyNumber: number,
  modifierId: string,
  name: string,
  startDate: string,
  endDate: string,
  eventStatusCode?: string
) => {
  const method = "POST";

  function formatDate(dateString: any) {
    if (!dateString) return null;
    const date = new Date(dateString);
    if (isNaN(date.getTime())) return null;
    const formattedDate = date.toISOString().split("T")[0];
    return formattedDate;
  }

  const formattedStartDate = formatDate(startDate);
  const formattedEndDate = formatDate(endDate);
  const currentJapanTime = getCurrentJapanTime();

  const queryString = `INSERT INTO event (
    city_code, 
    history_number, 
    modifier_id, 
    modification_timestamp, 
    is_deleted, 
    name, 
    creation_timestamp, 
    start_date, 
    end_date, 
    event_status_code) 
    VALUES (
      '${cityCode}', 
      ${historyNumber}, 
      '${modifierId}', 
      '${currentJapanTime}', 
      false, 
      '${name}', 
      '${currentJapanTime}', 
      ${formattedStartDate ? `'${formattedStartDate}'` : "NULL"}, 
      ${formattedEndDate ? `'${formattedEndDate}'` : "NULL"}, 
      '${eventStatusCode}');`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventCreate', 'insertEventData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

//get event data for insertion of venue
export const fetchEventIdByTitle = async (eventName: string) => {
  const method = "POST";
  const queryString =
    "SELECT event_id FROM event WHERE name ='" +
    eventName +
    "'ORDER BY event_id DESC LIMIT 1;";
  ActivityLogger.insertInfoLogEntry(new User(), 'EventCreate', 'fetchEventIdByTitle', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

//insert venue
export const insertVenueData = async (
  cityCode: string,
  eventId: number,
  historyNumber: number,
  modifierId: string,
  name: string
) => {
  const method = "POST";
  const currentJapanTime = getCurrentJapanTime();

  const queryString = `INSERT INTO venue (
    city_code, 
    event_id, 
    history_number, 
    modifier_id, 
    modification_timestamp, 
    is_deleted, 
    name) 
    VALUES (
      '${cityCode}',
      ${eventId}, 
      ${historyNumber}, 
      '${modifierId}',
      '${currentJapanTime}',
      false, 
      '${name}');`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventCreate', 'insertVenueData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};
