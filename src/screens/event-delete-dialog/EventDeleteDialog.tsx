import React, { useEffect, useState } from "react";
import { View } from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import ModalComponent from "../../components/basics/ModalComponent";
import styles from "./EventDeleteDialogStyles";
import { User } from "../../model/User";
import { ActivityLogger } from "../../log/ActivityLogger";

type Props = {
  onCancelButtonPress?: () => void;
  onDeleteButtonPress?: () => void;
};
export const EventDeleteDialog = (props: Props) => {
  const [isModalVisible, setModalVisible] = useState(true);

  useEffect(() => {
    ActivityLogger.insertInfoLogEntry(new User(), 'EventDeleteDialog', 'useEffect', 'screen open');
  }, []);

  return (
    <View>
      {isModalVisible && (
        <ModalComponent
          text="＜イベントタイトル＞を削除しますか？"
          firstButtonText="キャンセル"
          secondButtonText="削除する"
          secondButtonType="ButtonMDanger"
          onFirstButtonPress={props.onCancelButtonPress}
          onSecondButtonPress={props.onDeleteButtonPress}
          toggleModal={props.onCancelButtonPress}
          secondButtonWidth={104}
          secondBtnTextWidth={64}
        >
          <View style={styles.bodyContainer}>
            <HiraginoKakuText style={styles.bodyText} normal>
              イベントを削除すると、このイベントで受け付けた人の情報も削除されます。
              {"\n"}
              削除されたイベントはもとに戻すことはできません。
            </HiraginoKakuText>
          </View>
        </ModalComponent>
      )}
    </View>
  );
};
