import React, { useEffect, useState } from "react";
import {
  View,
  TextInput,
  Pressable,
  StatusBar,
  SafeAreaView,
} from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import { Ionicons } from "@expo/vector-icons";
import styles from "./LoginStyles";
import { colors } from "../../styles/color";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { CustomButton } from "../../components/basics/Button";
import { Header } from "../../components/basics/header";
import { realtimeDB } from "../../config/firebaseConfig";
import { get, ref } from "firebase/database";
import { NavigationProp } from "@react-navigation/native";
import * as crypto from "crypto-js";
import { EventListParams } from "../event-list/EventListParams";
import { User } from "../../model/User";
import { ActivityLogger } from "../../log/ActivityLogger";

type Props = {
  navigation: NavigationProp<any, any>;
};

export const Login = ({ navigation }: Props) => {
  const [userid, setUserId] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [inputNotEmpty, setInputNotEmpty] = useState(false);

  useEffect(() => {
    ActivityLogger.insertInfoLogEntry(new User(), 'Login', 'useEffect', 'screen open');
  }, []);

  const checkLogin = () => {
    ActivityLogger.insertInfoLogEntry(new User(), 'Login', 'checkLogin', 'login', '', null, '', 'userid=' + userid + ',password=' + password);

    const userRef = ref(realtimeDB, "baseMember");
    // 'baseMember'ノードからデータをフェッチする
    get(userRef)
      .then((snapshot) => {
        if (snapshot.exists()) {
          const userData = snapshot.val();

          // ログインをチェック
          let loggedIn = false;
          for (const userId in userData) {
            if (userData.hasOwnProperty(userId)) {
              const user = userData[userId];
              let hashpassword = crypto
                .SHA256(password)
                .toString(crypto.enc.Hex);
              if (user.id === userid && user.password === hashpassword) {
                loggedIn = true;
                setErrorMessage("");
                const user = new User();
                user.userId = userid;
                const eventListParams = new EventListParams();
                eventListParams.user = user;
                navigation.navigate("EventList", { eventListParams });
                ActivityLogger.insertInfoLogEntry(user, 'Login', 'checkLogin', 'transition', 'EventList', eventListParams);
                console.log("ログイン成功");
                break;
              }
            }
          }
          if (!loggedIn) {
            setErrorMessage("IDまたはパスワードが正しくありません");
            console.log("IDまたはパスワードが正しくありません");
          }
        } else {
          console.log("ユーザーデータが見つかりません");
        }
      })
      .catch((error) => {
        console.error("ユーザーデータの取得に失敗しました", error);
      });
  };

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword);
  };

  const handleInputChange = (text: string, isPassword: boolean = false) => {
    if (isPassword) {
      setPassword(text);
      setInputNotEmpty(userid.trim().length > 0 && text.trim().length > 0);
    } else {
      setUserId(text);
      setInputNotEmpty(text.trim().length > 0 && password.trim().length > 0);
    }
  };

  useEffect(() => { });

  const renderEyeIcon = () => {
    return (
      <Pressable style={styles.eyeIconContainer} hitSlop={18}>
        <Ionicons
          name={!showPassword ? "eye-off" : "eye"}
          size={24}
          color={colors.secondary}
          style={styles.eyeIcon}
          onPress={togglePasswordVisibility}
        />
      </Pressable>
    );
  };

  return (
    <KeyboardAwareScrollView
      style={{ flex: 1, width: "100%" }}
      resetScrollToCoords={{ x: 0, y: 0 }}
      contentContainerStyle={styles.mainContainer}
      scrollEnabled={false}
    >
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <Header
          middleTitleName="LGaP_RECEPCORE"
          buttonName=""
          hasButton={false}
        />

        <View style={styles.bodyContainer}>
          <HiraginoKakuText style={styles.loginText}>ログイン</HiraginoKakuText>

          <View style={styles.infoBox}>
            <View style={styles.inputContainer}>
              <View style={styles.labelInputSetBox}>
                <HiraginoKakuText style={styles.label}>ID</HiraginoKakuText>
                <View style={[styles.passwordBox]}>
                  <TextInput
                    placeholder="ID"
                    placeholderTextColor={colors.placeholderTextColor}
                    onChangeText={(text) => handleInputChange(text, false)}
                    value={userid}
                    style={[styles.input]}
                  />
                </View>
              </View>

              <View style={styles.labelInputSetBox}>
                <HiraginoKakuText style={styles.label}>
                  パスワード
                </HiraginoKakuText>

                <View style={[styles.passwordBox]}>
                  <TextInput
                    style={[styles.input]}
                    secureTextEntry={!showPassword}
                    placeholder="パスワード"
                    placeholderTextColor={colors.placeholderTextColor}
                    value={password}
                    onChangeText={(text) => handleInputChange(text, true)}
                    passwordRules=""
                  />

                  {renderEyeIcon()}
                </View>
                {errorMessage !== "" && (
                  <View style={styles.messageContainer}>
                    <HiraginoKakuText style={styles.errorMessage} normal>
                      {errorMessage}
                    </HiraginoKakuText>
                  </View>
                )}
              </View>
            </View>
            <CustomButton
              text="ログイン"
              onPress={() => checkLogin()}
              style={styles.buttonLogin}
              type={inputNotEmpty ? "ButtonMPrimary" : "ButtonMDisable"}
            />
          </View>
        </View>
      </SafeAreaView>
    </KeyboardAwareScrollView>
  );
};
