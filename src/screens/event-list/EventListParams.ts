import { User } from "../../model/User";

export class EventListParams implements Params {
  private _user: User = new User();
  private _toastMsg: string = "";
  //getter
  get user(): User {
    return this._user.clone();
  }
  get toastMsg(): string {
    return this._toastMsg;
  }
  //setter
  set user(value: User) {
    this._user = value;
  }
  set toastMsg(value: string) {
    this._toastMsg = value;
  }

  getAllValuesAsString(): string {
    let paramsString = '[ClassName=EventListParams';
    paramsString += ',_user=' + this._user.getAllValuesAsString();
    paramsString += ',_toastMsg=' + this._toastMsg;
    paramsString += ']';
    return paramsString
  }
}
