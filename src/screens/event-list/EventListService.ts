import { executeQuery } from "../../aws/db/dbOperation";
import { getCurrentJapanTime } from "../../environments/TimeUtils";
import { ActivityLogger } from "../../log/ActivityLogger";
import { User } from "../../model/User";

//get event status

export const fetchStatusData = async () => {
  const method = "POST";
  const queryString = `SELECT
      event_status_code AS id,
      name AS label,
      is_search_default
    FROM
      event_status
    ORDER BY
      event_status_code;`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventList', 'fetchStatusData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

// get event Initial Data
export const fetchInitialEventData = async (statusCode: string[]) => {
  const method = "POST";

  let statusCodeCondition = "";
  if (statusCode) {
    let statusCodeString = "'" + statusCode.join("','") + "'";
    statusCodeCondition = `AND event_status_code IN (${statusCodeString})`;
  }
  const queryString = `SELECT
    a.event_id,
    a.modification_timestamp,
    a.event_status_code,
    c.name AS event_status_name,
    a.name AS event_name,
    a.start_date,
    a.end_date,
    b.name AS venue_names,
    COALESCE(d.count, 0) AS count
FROM
    (
    SELECT
        event_id,
        modification_timestamp,
        name,
        start_date,
        end_date,
        event_status_code
    FROM
        event
    WHERE
        city_code = '242152'
        AND NOT is_deleted
        ${statusCodeCondition} 
    ) AS a
    INNER JOIN (
      SELECT
        event_id,
        string_agg(
        name,
         '、'
      ORDER BY
            venue_id
      ) AS name --会場名を1行にする
      FROM
        venue
      WHERE
        city_code = '242152'
        AND NOT is_deleted
      GROUP BY
        event_id
    ) AS b ON a.event_id = b.event_id
    LEFT JOIN event_status AS c ON a.event_status_code = c.event_status_code
    LEFT JOIN (
    SELECT
        event_id,
        COUNT(*) AS COUNT
    FROM
        reception
    WHERE
        city_code = '242152'
        AND NOT is_deleted
    GROUP BY
        event_id
    ) AS d ON a.event_id = d.event_id
ORDER BY
  a.modification_timestamp desc,
  b.event_id`;

  ActivityLogger.insertInfoLogEntry(new User(), 'EventList', 'fetchInitialEventData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

// search Event
export const fetchSearchEventData = async (
  eventName: string,
  venue: string,
  fromStartDate: string,
  toStartDate: string,
  fromEndDate: string,
  toEndDate: string,
  selectedOption: string,
  statusCode: string[]
) => {
  const method = "POST";
  let eventNameCondition = "";
  let venueCondition = "";
  let startCondition = "";
  let endCondition = "";
  let statusCodeCondition = "";
  let sortOrderCondition = "";

  // 1. Event name conditions
  if (eventName) {
    eventNameCondition = `AND name LIKE CONCAT('%', '${eventName}', '%')`;
  }

  // 2. Venue conditions
  if (venue) {
    venueCondition = `AND name LIKE CONCAT('%', '${venue}', '%')`;
  }
  if (statusCode) {
    let statusCodeString = "'" + statusCode.join("','") + "'";
    statusCodeCondition = `AND event_status_code IN (${statusCodeString})`;
  }

  // 3. Start date conditions
  if (fromStartDate && toStartDate) {
    startCondition = `AND (start_date IS NULL OR ('%${fromStartDate}%' <= start_date AND start_date <= '%${toStartDate}%'))`;
  } else if (fromStartDate && !toStartDate) {
    startCondition = `AND (start_date IS NULL OR ('%${fromStartDate}%' <= start_date))`;
  } else if (!fromStartDate && toStartDate) {
    startCondition = `AND (start_date IS NULL OR (start_date <= '%${toStartDate}%'))`;
  }

  // 4. End date conditions
  if (fromEndDate && toEndDate) {
    endCondition = `AND (end_date IS NULL OR ('%${fromEndDate}%' <= end_date AND end_date <= '%${toEndDate}%'))`;
  } else if (fromEndDate && !toEndDate) {
    endCondition = `AND (end_date IS NULL OR ('%${fromEndDate}%' <= end_date))`;
  } else if (!fromEndDate && toEndDate) {
    endCondition = `AND (end_date IS NULL OR (end_date <= '%${toEndDate}%'))`;
  }

  // 5. Sort order conditions
  if (selectedOption === "最終更新日が新しい") {
    sortOrderCondition = `a.modification_timestamp DESC,`;
  } else if (selectedOption === "最終更新日が古い") {
    sortOrderCondition = `a.modification_timestamp ASC,`;
  }
  const qString = `
  `;

  const queryString = `SELECT
  a.event_id,
  a.modification_timestamp,
  a.event_status_code,
  c.name AS event_status_name,
  a.name AS event_name,
  a.start_date,
  a.end_date,
  b.name AS venue_names,
  COALESCE(d.count, 0) AS count
FROM
  (
  SELECT
      event_id,
      modification_timestamp,
      name,
      start_date,
      end_date,
      event_status_code
  FROM
      event
  WHERE
      city_code = '242152'
      AND NOT is_deleted
      ${eventNameCondition}
      ${startCondition}
      ${endCondition}
      ${statusCodeCondition} 
  ) AS a
  INNER JOIN (
  SELECT
      e.event_id,
      e.name
  FROM
  (
    SELECT
        event_id
    FROM
        venue
    WHERE
        city_code = '242152'
        AND NOT is_deleted
        ${venueCondition}
    GROUP BY
        event_id
    ) AS c
    INNER JOIN (
        SELECT
            event_id,
            string_agg(name, '、') AS name --会場名を1行にする
        FROM
        (
            SELECT
                event_id,
                name
            FROM
                venue
            WHERE
                city_code = '242152'
                AND NOT is_deleted
            ORDER BY
                event_id,
                venue_id
            ) AS d
            GROUP BY
        event_id
      ) e ON c.event_id = e.event_id
  ) AS b ON a.event_id = b.event_id
  LEFT JOIN event_status AS c ON a.event_status_code = c.event_status_code
  LEFT JOIN (
  SELECT
      event_id,
      COUNT(*) AS COUNT
  FROM
      reception
  WHERE
      city_code = '242152'
      AND NOT is_deleted
  GROUP BY
      event_id
  ) AS d ON a.event_id = d.event_id
ORDER BY
${sortOrderCondition}
b.event_id`;

  ActivityLogger.insertInfoLogEntry(new User(), 'EventList', 'fetchSearchEventData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const insertEventHistory = async (eventId: number) => {
  const method = "POST";

  const queryString = `INSERT INTO 
  event_history 
SELECT 
  * 
FROM 
  event 
WHERE 
  city_code = '242152'
  AND event_id = ${eventId}`;

  ActivityLogger.insertInfoLogEntry(new User(), 'EventList', 'insertEventHistory', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const updateEvent = async (
  eventId: number,
  selectedStatusName: string,
  modifierId: string
) => {
  const method = "POST";

  const currentJapanTime = getCurrentJapanTime();

  const queryString = `UPDATE event 
SET 
  history_number = history_number + 1,
  modifier_id= '${modifierId}',
  modification_timestamp = '${currentJapanTime}',
  event_status_code = CASE 
                      WHEN '${selectedStatusName}' = '準備中' THEN '1'
                      WHEN '${selectedStatusName}' = '受付可能' THEN '2'
                      WHEN '${selectedStatusName}' = '受付終了' THEN '3'
                      ELSE '4'
                    END
WHERE
  city_code = '242152'
  AND event_id=${eventId}`;

  ActivityLogger.insertInfoLogEntry(new User(), 'EventList', 'updateEvent', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};
export const logicalDeleteEvent = async (
  eventId: number,
  modifierId: string
) => {
  const method = "POST";

  const currentJapanTime = getCurrentJapanTime();

  const queryString = `UPDATE event 
SET 
  history_number = history_number + 1,
  modifier_id= '${modifierId}',
  modification_timestamp = '${currentJapanTime}',
  is_deleted= true
WHERE
  city_code = '242152'
  AND event_id=${eventId}`;

  ActivityLogger.insertInfoLogEntry(new User(), 'EventList', 'logicalDeleteEvent', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};
export const fetchEventName = async (eventId: number) => {
  const method = "POST";
  const queryString = `SELECT
  name
FROM
  event
WHERE
  city_code = '242152'
  AND event_id = ${eventId}`;

  ActivityLogger.insertInfoLogEntry(new User(), 'EventList', 'fetchEventName', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

