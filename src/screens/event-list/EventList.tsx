import React, { useEffect, useRef, useState } from "react";
import {
  StatusBar,
  SafeAreaView,
  TextInput,
  Pressable,
  View,
  TouchableWithoutFeedback,
  ScrollView,
  Keyboard,
  Dimensions,
} from "react-native";
import styles from "./EventListStyle";
import { Header } from "../../components/basics/header";
import { CustomButton } from "../../components/basics/Button";
import { HiraginoKakuText } from "../../components/StyledText";
import { colors } from "../../styles/color";
import {
  Entypo,
  MaterialIcons,
  MaterialCommunityIcons,
  Feather,
} from "@expo/vector-icons";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { StatusChangeDialog } from "../status-change-dialog/StatusChangeDialog";
import { EventDeleteDialog } from "../event-delete-dialog/EventDeleteDialog";
import { Logout } from "../logout/Logout";
import { MultiDropBox } from "../../components/basics/MultiDropBox";
import { format, formatDate } from "date-fns";
import { CustomCalendar } from "../../components/basics/Calendar";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { EventListParams } from "./EventListParams";
import { EventDetailParams } from "../event-detail/EventDetailParams";
import { EventCreateParams } from "../event-create/EventCreateParams";
import {
  fetchEventName,
  fetchInitialEventData,
  fetchSearchEventData,
  fetchStatusData,
  insertEventHistory,
  logicalDeleteEvent,
  updateEvent,
} from "./EventListService";

import { EventEditParams } from "../event-edit/EventEditParams";
import { ActivityLogger } from "../../log/ActivityLogger";

type Props = {
  navigation: NavigationProp<any, any>;
};
type Params = {
  eventListParams: EventListParams;
};

export const EventList = ({ navigation }: Props) => {
  const route = useRoute();
  const { eventListParams } = route.params as Params;
  const [showInputs, setShowInputs] = useState(false);
  const [isDropdownVisible, setIsDropdownVisible] = useState(false);
  const [isStatusChangeModalVisible, setIsStatusChangeModalVisible] =
    useState(false);
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
  const [isLogOutModalVisible, setIsLogOutModalVisible] = useState(false);
  const [selectedOption, setSelectedOption] = useState("最終更新日が新しい");
  const [isFromStartDateCalendarVisible, setFromStartDateCalendarVisible] =
    useState(false);
  const [isToStartDateCalendarVisible, setToStartDateCalendarVisible] =
    useState(false);
  const [isFromEndDateCalendarVisible, setFromEndDateCalendarVisible] =
    useState(false);
  const [isToEndDateCalendarVisible, setToEndDateCalendarVisible] =
    useState(false);
  const [fromStartDate, setFromStartDate] = useState("");
  const [toStartDate, setToStartDate] = useState("");
  const [fromEndDate, setFromEndDate] = useState("");
  const [toEndDate, setToEndDate] = useState("");
  const fromStartDateInputRef = useRef(null);
  const toStartDateInputRef = useRef(null);
  const fromEndDateInputRef = useRef(null);
  const toEndDateInputRef = useRef(null);
  const fromStartDateRef = useRef(null);
  const toStartDateRef = useRef(null);
  const fromEndDateRef = useRef(null);
  const toEndDateRef = useRef(null);
  const { width } = Dimensions.get("window");
  const [x, setX] = useState(0);
  const [focusedInput, setFocusedInput] = useState("");
  const [eventName, setEventName] = useState("");
  const [venue, setVenue] = useState("");
  const [event, setEvent] = useState("");

  const [toastMessage, setToastMessage] = useState("");
  const [showToast, setShowToast] = useState(false);
  const [receptionStatusAscending, setReceptionStatusAscending] = useState<
    boolean | undefined
  >(undefined);
  const [numberOfPeopleAscending, setNumberOfPeopleAscending] = useState<
    boolean | undefined
  >(undefined);
  const [timeAscending, setTimeAscending] = useState<boolean | undefined>(
    undefined
  );
  const [isClear, setIsClear] = useState(false);
  const [timeField, setTimeField] = useState("");
  const [initial, setInitial] = useState("");
  const [isPressableVisible, setIsPressableVisible] = useState(false);

  // AWS
  const [events, setEvents] = useState<any[]>([]);
  const [message, setMessage] = useState<string>();
  const [errMsg, setErrMsg] = useState("");
  const [searchErrMsg, setSearchErrMsg] = useState("");
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(1);
  const [searchValueCount, setValue] = useState(0);
  const [eventStatus, setEventStatus] = useState<any[]>([]);
  const [selectedIds, setSelectedIds] = useState<string[]>([]);
  const [initialSelectedId, setInitialSelectedId] = useState<string[]>([]);
  const [firstSort, setFirstSort] = useState("");
  useEffect(() => {
    if (!loading) {
      setPage(0);
    }
    handleEventStatusCode();
    sortData();
    handleDropdownSelect(selectedOption);
  }, [loading, eventListParams, isClear]);
  useEffect(() => {
    ActivityLogger.insertInfoLogEntry(eventListParams.user, 'EventList', 'useEffect', 'screen open');
  }, []);

  useEffect(() => {
    setToastMessage(eventListParams.toastMsg);
    if (eventListParams.toastMsg !== "") {
      showToastMessage(eventListParams.toastMsg);
    }
  }, [eventListParams, event]);

  const showToastMessage = (message: string) => {
    setToastMessage(message);
    setShowToast(true);
    const timer = setTimeout(() => {
      setToastMessage("");
      setShowToast(false);
    }, 3000);
    return () => clearTimeout(timer);
  };

  useEffect(() => { }, [receptionStatusAscending, numberOfPeopleAscending]);

  //delete dialog
  const openEventDeleteModal = () => {
    setIsDeleteModalVisible(true);
  };

  const handleDeleteCancelButton = () => {
    setIsDeleteModalVisible(false);
    navigation.navigate("EventList", { eventListParams });
    ActivityLogger.insertInfoLogEntry(eventListParams.user, 'EventList', 'handleDeleteCancelButton', 'transition', 'EventList', eventListParams);
  };

  //event delete
  const handleDeleteButton = () => {
    deleteEvent(event);
    setIsDeleteModalVisible(false);
  };

  const handleEventDetail = (eventId: number) => {
    if (showDropdown || isDropdownVisible) {
      setShowDropdown(false);
      setShowSecondDropdown(false);
      setIsDropdownVisible(false);
    } else {
      setIsDropdownVisible(false);
      setShowDropdown(false);
      setShowSecondDropdown(false);
      const eventDetailParams = new EventDetailParams();
      eventDetailParams.user = eventListParams.user;
      eventDetailParams.eventId = eventId;
      navigation.navigate("EventDetail", {
        eventDetailParams,
      });
      ActivityLogger.insertInfoLogEntry(eventListParams.user, 'EventList','handleEventDetail', 'transition', 'EventDetail', eventDetailParams);
    }
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
  };

  const handleTextInputPress = () => {
    if (showDropdown || isDropdownVisible) {
      setShowDropdown(false);
      setShowSecondDropdown(false);
      setIsDropdownVisible(false);
    }
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
  };

  //logout Dialog
  const handleLogOut = () => {
    setIsDropdownVisible(false);
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
    setIsLogOutModalVisible(true);
  };
  const handleLogoutCancelButton = () => {
    setIsLogOutModalVisible(false);
    navigation.navigate("EventList", { eventListParams });
    ActivityLogger.insertInfoLogEntry(eventListParams.user, 'EventList', 'handleLogoutCancelButton', 'transition', 'EventList', eventListParams);
  };
  const handleLogoutButton = () => {
    setIsLogOutModalVisible(false);
    navigation.navigate("Login");
    ActivityLogger.insertInfoLogEntry(eventListParams.user, 'EventList', 'handleLogoutButton', 'transition', 'Login');
  };
  const handleCreateEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);

    const eventCreateParams = new EventCreateParams();
    eventCreateParams.user = eventListParams.user;

    navigation.navigate("EventCreate", {
      eventCreateParams,
    });
    ActivityLogger.insertInfoLogEntry(eventListParams.user, 'EventList', 'handleCreateEvent', 'transition', 'EventCreate', eventCreateParams);
  };
  const handleEditEvent = (itemId: number) => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    const eventEditParams = new EventEditParams();
    eventEditParams.user = eventListParams.user;
    eventEditParams.eventId = itemId;

    navigation.navigate("EventEdit", {
      eventEditParams,
    });
    ActivityLogger.insertInfoLogEntry(eventListParams.user, 'EventList', 'handleEditEvent', 'transition', 'EventEdit', eventEditParams);
  };
  const handleReproductionEvent = (itemId: number) => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    const eventCreateParams = new EventCreateParams();
    eventCreateParams.user = eventListParams.user;
    navigation.navigate("EventCreate", {
      eventCreateParams,
    });
    ActivityLogger.insertInfoLogEntry(eventListParams.user, 'EventList', 'handleReproductionEvent', 'transition', 'EventCreate', eventCreateParams);
  };
  const handleDeleteEvent = (itemId: number) => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    openEventDeleteModal();
  };

  //status change dialog
  const [selectedStatusName, setSelectedStatusName] = useState("");
  const handleStatusChangeEvent = (status: string, eventId: number) => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setSelectedStatusName(status);
    setIsStatusChangeModalVisible(true);
    setIsDropdownVisible(false);
  };
  const handleCancelButton = () => {
    setIsStatusChangeModalVisible(false);
    navigation.navigate("EventList", { eventListParams });
    ActivityLogger.insertInfoLogEntry(eventListParams.user, 'EventList', 'handleCancelButton', 'transition', 'EventList', eventListParams);
  };
  const handleChangeButton = () => {
    handleUpdateStatusCode();
    setIsStatusChangeModalVisible(false);
  };

  const handleStatusChangePreparationEvent = (
    status: string,
    eventId: number
  ) => {
    const eventDetailParams = new EventDetailParams();
    eventDetailParams.eventId = eventId;
    eventDetailParams.user = eventListParams.user;
    navigation.navigate("EventDetail", {
      eventDetailParams,
    });
    ActivityLogger.insertInfoLogEntry(eventListParams.user, 'EventList', 'handleStatusChangePreparationEvent', 'transition', 'EventDetail', eventDetailParams);
  };

  // Dropdown
  const dropdownData = [
    { label: "最終更新日が新しい", value: "newest" },
    { label: "最終更新日が古い", value: "oldest" },
  ];
  const handleSortingDropdownPress = () => {
    setIsDropdownVisible(!isDropdownVisible);
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setDropBoxVisible(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
    setTimeField("time");
  };

  const handleDropdownSelect = (value: any) => {
    setIsDropdownVisible(false);
    if (timeField !== "time") {
      if (timeAscending === undefined) {
        setTimeAscending(undefined);
        setSelectedOption("最終更新日が新しい");
      }
    } else {
      if (timeAscending === undefined) {
        setTimeAscending(true);
      } else {
        setTimeAscending(!timeAscending);
      }
      setFirstSort("time");
    }
    if (selectedOption != value) {
      setSelectedOption(value);
      setEvents(
        events.sort((a, b) => {
          if (selectedOption == "最終更新日が新しい") {
            if (a.modification_timestamp > b.modification_timestamp) {
              return 1;
            } else {
              return -1;
            }
          } else {
            if (a.modification_timestamp < b.modification_timestamp) {
              return 1;
            } else {
              return -1;
            }
          }
        })
      );
    }
  };

  // Edit Dropdown
  const [showDropdown, setShowDropdown] = useState(false);
  const [showSecondDropdown, setShowSecondDropdown] = useState(false);
  const [isDropBoxVisible, setDropBoxVisible] = useState(false);
  const [openDropdownId, setOpenDropdownId] = useState<number | null>(null);
  const [selectedItemId, setSelectedItemId] = useState(0);

  const handleDropDown = (itemId: number) => {
    setSelectedItemId(itemId);
    setShowDropdown((prev) => {
      if (prev && openDropdownId === itemId) {
        setOpenDropdownId(null);
        return false;
      } else {
        setOpenDropdownId(itemId);
        return true;
      }
    });
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    setDropBoxVisible(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
  };

  const dropDownRef = useRef(null);
  // Close dropdown when click outside
  const handleClosePopup = (event: any) => {
    if ((showDropdown && showSecondDropdown) || showDropdown) {
      setShowDropdown(false);
      setShowSecondDropdown(false);
    }
    // Close sorting dropdown when click outside
    if (isDropdownVisible) {
      setIsDropdownVisible(false);
    }
    if (event.nativeEvent.target != dropDownRef) {
      if (isDropBoxVisible) {
        setDropBoxVisible(false);
      }
    }
    closeCalendar(event);
  };

  const handleFromStartDateCalendarPress = (event: any) => {
    (fromStartDateInputRef.current as any).focus();
    setFromStartDateCalendarVisible(!isFromStartDateCalendarVisible);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    setFocusedInput("fromStartDateInput");
  };

  const handleToStartDateCalendarPress = (event: any) => {
    (toStartDateInputRef.current as any).focus();
    setToStartDateCalendarVisible(!isToStartDateCalendarVisible);
    setFromStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    (toStartDateInputRef.current as any).measure(
      (
        x: number,
        y: number,
        width: number,
        height: number,
        pageX: number,
        pageY: number
      ) => {
        setX(pageX);
      }
    );
    setFocusedInput("toStartDateInput");
  };

  const handleFromEndDateCalendarPress = (event: any) => {
    (fromEndDateInputRef.current as any).focus();
    setFromEndDateCalendarVisible(!isFromEndDateCalendarVisible);
    setToEndDateCalendarVisible(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
  };

  const handleToEndDateCalendarPress = (event: any) => {
    (toEndDateInputRef.current as any).focus();
    setToEndDateCalendarVisible(!isToEndDateCalendarVisible);
    setFromEndDateCalendarVisible(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
  };

  const handleFromStartDateSelect = (date: any) => {
    fromStartDate == date ? setFromStartDate("") : setFromStartDate(date);
    setFromStartDateCalendarVisible(false);
  };

  const handleToStartDateSelect = (date: any) => {
    toStartDate == date ? setToStartDate("") : setToStartDate(date);
    setToStartDateCalendarVisible(false);
  };

  const handleFromEndDateSelect = (date: any) => {
    fromEndDate == date ? setFromEndDate("") : setFromEndDate(date);
    setFromEndDateCalendarVisible(false);
  };

  const handleToEndDateSelect = (date: any) => {
    toEndDate == date ? setToEndDate("") : setToEndDate(date);
    setToEndDateCalendarVisible(false);
  };

  const closeCalendar = (event: any) => {
    if (
      event.nativeEvent.target != fromStartDateInputRef.current &&
      event.nativeEvent.target != fromStartDateRef
    ) {
      if (isFromStartDateCalendarVisible) {
        setFromStartDateCalendarVisible(false);
      }
    }
    if (
      event.nativeEvent.target != toStartDateInputRef.current &&
      event.nativeEvent.target != toStartDateRef
    ) {
      if (isToStartDateCalendarVisible) {
        setToStartDateCalendarVisible(false);
      }
    }
    if (
      event.nativeEvent.target != fromEndDateInputRef.current &&
      event.nativeEvent.target != fromEndDateRef
    ) {
      if (isFromEndDateCalendarVisible) {
        setFromEndDateCalendarVisible(false);
      }
    }
    if (
      event.nativeEvent.target != toEndDateInputRef.current &&
      event.nativeEvent.target != toEndDateRef
    ) {
      if (isToEndDateCalendarVisible) {
        setToEndDateCalendarVisible(false);
      }
    }
  };

  const toggleInputs = (event: any) => {
    setShowInputs(!showInputs);

    let count = 0;
    if (venue != "") {
      count = count + 1;
    }
    if (
      (fromStartDate !== "" && toStartDate === "") ||
      (fromStartDate === "" && toStartDate !== "") ||
      (fromStartDate !== "" && toStartDate !== "")
    ) {
      count = count + 1;
    }
    if (
      (fromEndDate !== "" && toEndDate === "") ||
      (fromEndDate === "" && toEndDate !== "") ||
      (fromEndDate !== "" && toEndDate !== "")
    ) {
      count = count + 1;
    }
    setValue(count);
    handleClosePopup(event);
  };

  const handleEventTitleChange = (text: string) => {
    setEventName(text);
  };

  const handleVenueChange = (text: string) => {
    setVenue(text);
  };

  const handleEventStatusCode = async () => {
    try {
      const result = await fetchStatusData();
      const message = result.message;
      if (message === "success") {
        setMessage("Optained Successfully!!");
        setEventStatus(result.data);

        if (!Array.isArray(result.data)) {
          throw new Error("Data is not an array");
        }

        const transformedData = result.data
          .filter((option) => option.is_search_default)
          .map(({ id, label, is_search_default }) => ({
            id,
            label,
            is_search_default,
          }));

        const idsFromData = transformedData.map(
          (option: { id: string }) => option.id
        );

        if (selectedIds.length == 0) {
          setInitialSelectedId(idsFromData);
          setSelectedIds(idsFromData);
          handleRead(idsFromData);
        } else {
          if (initial == "initial") {
            setInitialSelectedId(idsFromData);
            setSelectedIds(idsFromData);
            handleRead(idsFromData);
            setInitial("");
          } else {
            setInitialSelectedId(selectedIds);
            setSelectedIds(selectedIds);
            handleRead(selectedIds);
          }
        }
      } else {
        setMessage(message);
      }
    } catch (error) {
      console.error("Error from EventListService:", error);
    }
  };

  // Initial State
  const handleRead = async (idsFromData: string[]) => {
    try {
      const result = await fetchInitialEventData(idsFromData);
      if (result.data) {
        setLoading(true);
      } else {
        setLoading(false);
        setSearchErrMsg("");
        setErrMsg(
          "イベントがありません。\n イベントを作成するとここに表示されます。"
        );
      }
      const message = result.message;
      if (message === "success") {
        setMessage("Obtained Successfully!!");
        if (
          eventName !== "" ||
          venue !== "" ||
          fromStartDate !== "" ||
          toStartDate !== "" ||
          fromEndDate !== "" ||
          toEndDate !== ""
        ) {
          handleSearchPressTarget(
            eventName,
            venue,
            fromStartDate,
            toStartDate,
            fromEndDate,
            toEndDate,
            selectedOption,
            selectedIds
          );
        } else {
          setEvents(result.data);
          setIsClear(false);
        }
      } else {
        setMessage(message);
      }
    } catch (error) {
      console.error("Error from EventListService:", error);
      setMessage("An error occurred");
    }
  };
  //Search
  const handleSearchPress = async (event: any) => {
    eventListParams.toastMsg = "";
    handleClosePopup(event);
    setEvent(event);
    const searchedResult = await fetchSearchEventData(
      eventName,
      venue,
      fromStartDate,
      toStartDate,
      fromEndDate,
      toEndDate,
      selectedOption,
      selectedIds
    );
    setEvents(searchedResult.data);

    if (searchedResult.data) {
      setPage(1);
    } else {
      setPage(0);
      setErrMsg("");
      setSearchErrMsg(
        "指定された条件に当てはまるイベントはありません。\n 別の条件を指定して再度検索してください。"
      );
    }
  };

  //SearchTarget
  const handleSearchPressTarget = async (
    eventName: string,
    venue: string,
    fromStartDate: string,
    toStartDate: string,
    fromEndDate: string,
    toEndDate: string,
    selectedOption: string,
    selectedIds: string[]
  ) => {
    setIsClear(false);
    const searchedResult = await fetchSearchEventData(
      eventName,
      venue,
      fromStartDate,
      toStartDate,
      fromEndDate,
      toEndDate,
      selectedOption,
      selectedIds
    );
    setEvents(searchedResult.data);
    if (searchedResult.data) {
      setPage(1);
    } else {
      setPage(0);
      setErrMsg("");
      setSearchErrMsg(
        "指定された条件に当てはまるイベントはありません。\n 別の条件を指定して再度検索してください。"
      );
    }
  };
  // Clear data from search
  const handleClearPress = (event: any) => {
    eventListParams.toastMsg = "";
    setEvent(event);
    handleClosePopup(event);
    setEventName("");
    setVenue("");
    setFromStartDate("");
    setToStartDate("");
    setFromEndDate("");
    setToEndDate("");
    setSelectedOption("最終更新日が新しい");
    setIsClear(true);
    setReceptionStatusAscending(undefined);
    setNumberOfPeopleAscending(undefined);
    setTimeAscending(undefined);
    setValue(0);
    setFirstSort("");
    sortIconChange("");
    setTimeField("");
    setInitial("initial");
    setPage(1);
  };

  //update event status code
  const handleUpdateStatusCode = async () => {
    try {
      const result = await insertEventHistory(selectedItemId);

      const message = result.message;

      if (message === "success") {
        const updEventResult = await updateEvent(
          selectedItemId,
          selectedStatusName,
          eventListParams.user.userId
        );
        const updmessage = await updEventResult.message;
        if (updmessage === "success") {
          setMessage("Inserted Successfully!!");
          handleEventStatusCode();
          sortData();

          const eventNameResult = await fetchEventName(selectedItemId);

          var getEventName = eventNameResult.data[0].name;
          const eventListParams = new EventListParams();
          eventListParams.user = eventListParams.user;
          eventListParams.toastMsg = `＜${getEventName}＞を${selectedStatusName}に変更しました`;

          navigation.navigate("EventList", { eventListParams });
          ActivityLogger.insertInfoLogEntry(eventListParams.user, 'EventList', 'handleUpdateStatusCode', 'transition', 'EventList', eventListParams);
        } else {
          setMessage(updmessage);
        }
      } else {
        setMessage(message);
      }
    } catch (error) {
      console.error("Error from EventListService:", error);
    }
  };

  //delete event
  const deleteEvent = async (event: any) => {
    setIsClear(false);
    try {
      const deleteResult = await insertEventHistory(selectedItemId);

      const message = deleteResult.message;

      if (message === "success") {
        const updEventResult = await logicalDeleteEvent(
          selectedItemId,
          eventListParams.user.userId
        );
        const updmessage = await updEventResult.message;
        if (updmessage === "success") {
          setMessage("Inserted Successfully!!");
          const eventListParams = new EventListParams();
          eventListParams.user = eventListParams.user;
          eventListParams.toastMsg = `イベントを削除しました`;

          navigation.navigate("EventList", { eventListParams });
          ActivityLogger.insertInfoLogEntry(eventListParams.user, 'EventList', 'deleteEvent', 'transition', 'EventList', eventListParams);
        } else {
          setMessage(updmessage);
        }
      } else {
        setMessage(message);
      }
    } catch (error) {
      console.error("Error from EventListService:", error);
    }
  };

  //Hover
  const [isHovered, setIsHovered] = useState(false);
  const [hoveredEventId, setHoveredEventId] = useState<number | null>(null);

  const handleMouseEnter = (eventId: number) => {
    setHoveredEventId(eventId);
    setIsHovered(true);
  };
  const handleMouseEnterRow = (eventId: number) => {
    setHoveredEventId(eventId);
    setIsHovered(false);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
    setHoveredEventId(null);
  };
  const handleMouseLeaveRow = () => {
    setHoveredEventId(null);
  };

  const formatDate = (dateString: string | undefined) => {
    if (!dateString) return "";
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const day = String(date.getDate()).padStart(2, "0");
    return `${year}/${month}/${day}`;
  };

  const sortIconChange = (field: string) => {
    if (field === "receptionStatus") {
      if (receptionStatusAscending === undefined) {
        setReceptionStatusAscending(true);
        setNumberOfPeopleAscending(undefined);
      } else {
        setReceptionStatusAscending(!receptionStatusAscending);
        setNumberOfPeopleAscending(undefined);
      }
      setFirstSort("receptionStatus");
    } else {
      if (receptionStatusAscending === undefined) {
        setReceptionStatusAscending(undefined);
      }
    }
    if (field === "people") {
      if (numberOfPeopleAscending === undefined) {
        setNumberOfPeopleAscending(true);
        setReceptionStatusAscending(undefined);
      } else {
        setNumberOfPeopleAscending(!numberOfPeopleAscending);
        setReceptionStatusAscending(undefined);
      }
      setFirstSort("people");
    } else {
      if (numberOfPeopleAscending === undefined) {
        setNumberOfPeopleAscending(undefined);
      }
    }
  };

  const sortData = () => {
    const sortedData = [...events].sort((a, b) => {
      if (firstSort == "") {
        if (selectedOption == "最終更新日が新しい") {
          let modificationComparison = b.modification_timestamp.localeCompare(
            a.modification_timestamp
          );
          if (selectedOption) {
            modificationComparison *= 1;
          }
          if (modificationComparison !== 0) return modificationComparison;
        }
        if (selectedOption == "最終更新日が古い") {
          let modificationComparison = a.modification_timestamp.localeCompare(
            b.modification_timestamp
          );
          if (!selectedOption) {
            modificationComparison *= -1;
          }
          if (modificationComparison !== 0) return modificationComparison;
        }
      } else if (firstSort == "time") {
        if (receptionStatusAscending !== undefined) {
          let statusComparison = a.event_status_code.localeCompare(
            b.event_status_code
          );
          if (!receptionStatusAscending) {
            statusComparison *= -1;
          }
          if (statusComparison !== 0) return statusComparison;
        }
        if (numberOfPeopleAscending !== undefined) {
          let peopleComparison = a.count - b.count;

          if (!numberOfPeopleAscending) {
            peopleComparison *= -1;
          }
          if (peopleComparison !== 0) return peopleComparison;
        }

        if (selectedOption == "最終更新日が新しい") {
          let modificationComparison = b.modification_timestamp.localeCompare(
            a.modification_timestamp
          );
          if (selectedOption) {
            modificationComparison *= 1;
          }
          if (modificationComparison !== 0) return modificationComparison;
        }
        if (selectedOption == "最終更新日が古い") {
          let modificationComparison = a.modification_timestamp.localeCompare(
            b.modification_timestamp
          );
          if (!selectedOption) {
            modificationComparison *= -1;
          }
          if (modificationComparison !== 0) return modificationComparison;
        }
      } else if (firstSort == "receptionStatus") {
        let statusComparison = a.event_status_code.localeCompare(
          b.event_status_code
        );
        if (!receptionStatusAscending) {
          statusComparison *= -1;
        }
        if (statusComparison !== 0) return statusComparison;
        if (selectedOption == "最終更新日が新しい") {
          let modificationComparison = b.modification_timestamp.localeCompare(
            a.modification_timestamp
          );
          if (selectedOption) {
            modificationComparison *= 1;
          }
          if (modificationComparison !== 0) return modificationComparison;
        }
        if (selectedOption == "最終更新日が古い") {
          let modificationComparison = a.modification_timestamp.localeCompare(
            b.modification_timestamp
          );
          if (!selectedOption) {
            modificationComparison *= -1;
          }
          if (modificationComparison !== 0) return modificationComparison;
        }
      } else if (firstSort == "people") {
        let peopleComparison = a.count - b.count;

        if (!numberOfPeopleAscending) {
          peopleComparison *= -1;
        }
        if (peopleComparison !== 0) return peopleComparison;
        if (selectedOption == "最終更新日が新しい") {
          let modificationComparison = b.modification_timestamp.localeCompare(
            a.modification_timestamp
          );
          if (selectedOption) {
            modificationComparison *= 1;
          }
          if (modificationComparison !== 0) return modificationComparison;
        }
        if (selectedOption == "最終更新日が古い") {
          let modificationComparison = a.modification_timestamp.localeCompare(
            b.modification_timestamp
          );
          if (!selectedOption) {
            modificationComparison *= -1;
          }
          if (modificationComparison !== 0) return modificationComparison;
        }
      }
    });

    return sortedData;
  };

  // Pagination
  const ITEMS_PER_PAGE = 50;

  let totalCount: string = "";
  let newTotalCount: number = 0;
  let totalPages = 0;

  if (events == undefined) {
    totalCount = "0";
    newTotalCount = 0;
  } else {
    totalCount = events.length.toLocaleString();
    newTotalCount = events.length;
    totalPages = Math.ceil(newTotalCount / ITEMS_PER_PAGE);
  }

  const getPageData = () => {
    let sortedData = sortData();
    const startIndex = (page - 1) * ITEMS_PER_PAGE;
    const endIndex = startIndex + ITEMS_PER_PAGE;
    return sortedData.slice(startIndex, endIndex);
  };

  const renderHeader = () => (
    <View style={styles.header}>
      <Pressable
        style={styles.receptionStatusHeader}
        onPress={() => sortIconChange("receptionStatus")}
      >
        <HiraginoKakuText style={styles.receptionStatusText}>
          受付状況
        </HiraginoKakuText>

        {receptionStatusAscending === undefined ? (
          <MaterialCommunityIcons
            name="swap-vertical"
            size={20}
            color={colors.greyTextColor}
          />
        ) : receptionStatusAscending ? (
          <MaterialIcons
            name="arrow-upward"
            size={24}
            color={colors.greyTextColor}
          />
        ) : (
          <MaterialIcons
            name="arrow-downward"
            size={24}
            color={colors.greyTextColor}
          />
        )}
      </Pressable>
      <View style={[styles.eachHeadingContainer, styles.eventHeaderText]}>
        <HiraginoKakuText style={[styles.headerText]}>
          イベント名
        </HiraginoKakuText>
      </View>
      <View style={[styles.eachHeadingContainer, styles.venueHeaderText]}>
        <HiraginoKakuText style={[styles.headerText]}>会場</HiraginoKakuText>
      </View>
      <View style={[styles.eachHeadingContainer, styles.eventDateText]}>
        <HiraginoKakuText style={[styles.headerText]}>
          イベント期間
        </HiraginoKakuText>
      </View>
      <Pressable
        style={styles.acceptedPersonHeader}
        onPress={() => sortIconChange("people")}
      >
        <HiraginoKakuText style={[styles.acceptedPersonHeaderText]}>
          受付した人
        </HiraginoKakuText>

        {numberOfPeopleAscending === undefined ? (
          <MaterialCommunityIcons
            name="swap-vertical"
            size={20}
            color={colors.greyTextColor}
          />
        ) : numberOfPeopleAscending ? (
          <MaterialIcons
            name="arrow-upward"
            size={24}
            color={colors.greyTextColor}
          />
        ) : (
          <MaterialIcons
            name="arrow-downward"
            size={24}
            color={colors.greyTextColor}
          />
        )}
      </Pressable>
      <View></View>
    </View>
  );

  //pagination action
  const handlePageChange = (pageNum: number) => {
    setPage(pageNum);
  };

  const renderPaginationButtons = () => {
    const maxVisibleButtons = 5;

    const currentPage = page;
    let startPage = 1;
    let endPage = Math.min(totalPages, maxVisibleButtons);

    if (totalPages > maxVisibleButtons) {
      if (currentPage <= Math.floor(maxVisibleButtons / 2) + 1) {
        endPage = maxVisibleButtons;
      } else if (
        currentPage >=
        totalPages - Math.floor(maxVisibleButtons / 2)
      ) {
        startPage = totalPages - maxVisibleButtons + 1;
        endPage = totalPages;
      } else {
        startPage = currentPage - Math.floor(maxVisibleButtons / 2);
        endPage = currentPage + Math.floor(maxVisibleButtons / 2);
      }
    }

    const buttons = [];
    for (let i = startPage; i <= endPage; i++) {
      buttons.push(
        <Pressable
          key={`button-${i}`}
          onPress={() => handlePageChange(i)}
          style={
            i === currentPage ? styles.activeButton : styles.inactiveButton
          }
        >
          <HiraginoKakuText
            style={i === currentPage ? styles.activeText : styles.inactiveText}
          >
            {i}
          </HiraginoKakuText>
        </Pressable>
      );
    }

    if (totalPages > maxVisibleButtons && endPage < totalPages) {
      buttons.push(
        <Pressable
          key={`ellipsis-button`}
          onPress={() => handlePageChange(endPage + 1)}
        >
          <HiraginoKakuText style={styles.inactiveText}>...</HiraginoKakuText>
        </Pressable>
      );
      buttons.push(
        <Pressable
          key={`last-page-button`}
          onPress={() => handlePageChange(totalPages)}
          style={styles.inactiveButton}
        >
          <HiraginoKakuText style={styles.inactiveText}>
            {totalPages}
          </HiraginoKakuText>
        </Pressable>
      );
    }

    return buttons;
  };

  const handleFirstPage = () => {
    setPage(1);
  };

  const handlePrevPage = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };

  const handleNextPage = () => {
    if (page < totalPages) {
      setPage(page + 1);
    }
  };

  const handleLastPage = () => {
    setPage(totalPages);
  };

  const lastPage = Math.ceil(newTotalCount / ITEMS_PER_PAGE);

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        middleTitleName="LGaP_RECEPCORE"
        buttonName="ログアウト"
        onPress={handleLogOut}
      ></Header>
      <ScrollView>
        <TouchableWithoutFeedback onPress={handleClosePopup}>
          <View style={styles.bodyContainer}>
            <View style={styles.bodyTitleContainer}>
              <HiraginoKakuText style={styles.bodyText}>
                イベント
              </HiraginoKakuText>
              <CustomButton
                text="イベント作成"
                onPress={handleCreateEvent}
                style={styles.bodyTitleButton}
                type="ButtonMPrimary"
              />
            </View>
            <View style={styles.mainInfoContainer}>
              <View style={[styles.firstChildContainer]}>
                <View style={styles.infoContainer}>
                  <View style={styles.parentInputContainer}>
                    <View style={styles.childInputContainer}>
                      <View style={styles.labelContainer}>
                        <HiraginoKakuText style={styles.labelText}>
                          イベント名
                        </HiraginoKakuText>
                      </View>
                      <View style={styles.inputContainer}>
                        <TextInput
                          style={styles.input}
                          placeholder="イベントタイトル"
                          placeholderTextColor={colors.placeholderTextColor}
                          onFocus={handleTextInputPress}
                          onChangeText={handleEventTitleChange}
                          value={eventName}
                        />
                      </View>
                    </View>
                    <View style={styles.childInputContainer}>
                      <View style={styles.labelContainer}>
                        <HiraginoKakuText style={styles.labelText}>
                          受付状況
                        </HiraginoKakuText>
                      </View>
                      <Pressable ref={dropDownRef}>
                        <MultiDropBox
                          initialOptions={eventStatus}
                          initialSelectedIds={initialSelectedId}
                          onCloseSortingDropdown={handleTextInputPress}
                          onSelectedIdsChange={(selectedIds) =>
                            setSelectedIds(selectedIds)
                          }
                        />
                      </Pressable>
                    </View>
                  </View>
                  <>
                    <Pressable
                      style={[
                        styles.dropdownContainer,
                        { width: searchValueCount > 0 ? 156 : 112 },
                      ]}
                      onPress={toggleInputs}
                    >
                      <HiraginoKakuText style={styles.dropdownText}>
                        詳細検索
                        {searchValueCount > 0 && ` (${searchValueCount})`}
                      </HiraginoKakuText>
                      <Entypo
                        name={showInputs ? "chevron-up" : "chevron-down"}
                        size={24}
                        color={colors.primary}
                        style={styles.iconStyle}
                      />
                    </Pressable>
                    {showInputs && (
                      <View style={styles.hiddenContainer}>
                        <View style={styles.parentInputContainer}>
                          <View style={styles.childInputContainer}>
                            <View style={styles.labelContainer}>
                              <HiraginoKakuText style={styles.labelText}>
                                会場
                              </HiraginoKakuText>
                            </View>
                            <View style={styles.inputContainer}>
                              <TextInput
                                style={styles.input}
                                placeholder="会場名"
                                placeholderTextColor={
                                  colors.placeholderTextColor
                                }
                                onFocus={handleTextInputPress}
                                onChangeText={handleVenueChange}
                                value={venue}
                              />
                            </View>
                          </View>
                        </View>
                        <View style={[styles.mainDateContainer]}>
                          <View style={styles.parentDateContainer}>
                            <View style={styles.labelContainer}>
                              <HiraginoKakuText style={styles.labelText}>
                                終了日
                              </HiraginoKakuText>
                            </View>
                            <View style={styles.childDateContainer}>
                              <View style={styles.dateInputContainer}>
                                <TextInput
                                  ref={fromEndDateInputRef}
                                  style={styles.dateInput}
                                  placeholder="日付を選択"
                                  placeholderTextColor={
                                    colors.placeholderTextColor
                                  }
                                  value={
                                    fromEndDate != ""
                                      ? format(
                                        new Date(fromEndDate),
                                        "yyyy/MM/dd"
                                      )
                                      : fromEndDate
                                  }
                                  onPressIn={handleFromEndDateCalendarPress}
                                  onPointerDown={handleFromEndDateCalendarPress}
                                  showSoftInputOnFocus={false}
                                  onTouchStart={() => Keyboard.dismiss()}
                                  editable={false}
                                />
                                <Pressable
                                  ref={fromEndDateRef}
                                  style={styles.calendarIconContainer}
                                  onPress={handleFromEndDateCalendarPress}
                                >
                                  <MaterialIcons
                                    name="calendar-today"
                                    size={24}
                                    color={colors.activeCarouselColor}
                                  />
                                </Pressable>
                                {isFromEndDateCalendarVisible && (
                                  <CustomCalendar
                                    selectedDate={fromEndDate}
                                    onDateSelect={handleFromEndDateSelect}
                                  />
                                )}
                              </View>
                              <HiraginoKakuText style={styles.tildeText}>
                                〜
                              </HiraginoKakuText>
                              <View style={styles.secondDateInputContainer}>
                                <TextInput
                                  ref={toEndDateInputRef}
                                  style={styles.dateInput}
                                  placeholder="日付を選択"
                                  placeholderTextColor={
                                    colors.placeholderTextColor
                                  }
                                  value={
                                    toEndDate != ""
                                      ? format(
                                        new Date(toEndDate),
                                        "yyyy/MM/dd"
                                      )
                                      : toEndDate
                                  }
                                  onPressIn={handleToEndDateCalendarPress}
                                  onPointerDown={handleToEndDateCalendarPress}
                                  showSoftInputOnFocus={false}
                                  onTouchStart={() => Keyboard.dismiss()}
                                  editable={false}
                                />
                                <Pressable
                                  ref={toEndDateRef}
                                  style={styles.calendarIconContainer}
                                  onPress={handleToEndDateCalendarPress}
                                >
                                  <MaterialIcons
                                    name="calendar-today"
                                    size={24}
                                    color={colors.activeCarouselColor}
                                  />
                                </Pressable>
                                {isToEndDateCalendarVisible && (
                                  <CustomCalendar
                                    selectedDate={toEndDate}
                                    onDateSelect={handleToEndDateSelect}
                                  />
                                )}
                              </View>
                            </View>
                          </View>
                          <View style={styles.secondParentDateContainer}>
                            <View style={styles.labelContainer}>
                              <HiraginoKakuText style={styles.labelText}>
                                開始日
                              </HiraginoKakuText>
                            </View>
                            <View style={styles.childDateContainer}>
                              <View
                                style={[
                                  styles.dateInputContainer,
                                  x + wp("30.25%") > width &&
                                  focusedInput === "toStartDateInput" && {
                                    zIndex: -1,
                                  },
                                ]}
                              >
                                <TextInput
                                  ref={fromStartDateInputRef}
                                  style={styles.dateInput}
                                  placeholder="日付を選択"
                                  placeholderTextColor={
                                    colors.placeholderTextColor
                                  }
                                  value={
                                    fromStartDate != ""
                                      ? format(
                                        new Date(fromStartDate),
                                        "yyyy/MM/dd"
                                      )
                                      : fromStartDate
                                  }
                                  onPressIn={handleFromStartDateCalendarPress}
                                  onPointerDown={
                                    handleFromStartDateCalendarPress
                                  }
                                  showSoftInputOnFocus={false}
                                  onTouchStart={() => Keyboard.dismiss()}
                                  editable={false}
                                />
                                <Pressable
                                  ref={fromStartDateRef}
                                  style={styles.calendarIconContainer}
                                  onPress={handleFromStartDateCalendarPress}
                                >
                                  <MaterialIcons
                                    name="calendar-today"
                                    size={24}
                                    color={colors.activeCarouselColor}
                                  />
                                </Pressable>
                                {isFromStartDateCalendarVisible && (
                                  <CustomCalendar
                                    selectedDate={fromStartDate}
                                    onDateSelect={handleFromStartDateSelect}
                                  />
                                )}
                              </View>
                              <HiraginoKakuText style={styles.tildeText}>
                                〜
                              </HiraginoKakuText>
                              <View
                                style={[
                                  styles.secondDateInputContainer,
                                  x + wp("30.25%") > width && {
                                    justifyContent: "flex-end",
                                  },
                                  x + wp("30.25%") > width &&
                                  focusedInput === "fromStartDateInput" && {
                                    zIndex: -1,
                                  },
                                ]}
                              >
                                <TextInput
                                  ref={toStartDateInputRef}
                                  style={styles.dateInput}
                                  placeholder="日付を選択"
                                  placeholderTextColor={
                                    colors.placeholderTextColor
                                  }
                                  value={
                                    toStartDate != ""
                                      ? format(
                                        new Date(toStartDate),
                                        "yyyy/MM/dd"
                                      )
                                      : toStartDate
                                  }
                                  onPressIn={handleToStartDateCalendarPress}
                                  onPointerDown={handleToStartDateCalendarPress}
                                  showSoftInputOnFocus={false}
                                  onTouchStart={() => Keyboard.dismiss()}
                                  editable={false}
                                />
                                <Pressable
                                  ref={toStartDateRef}
                                  onPress={handleToStartDateCalendarPress}
                                  style={styles.calendarIconContainer}
                                >
                                  <MaterialIcons
                                    name="calendar-today"
                                    size={24}
                                    color={colors.activeCarouselColor}
                                  />
                                </Pressable>
                                {isToStartDateCalendarVisible && (
                                  <CustomCalendar
                                    selectedDate={toStartDate}
                                    onDateSelect={handleToStartDateSelect}
                                  />
                                )}
                              </View>
                            </View>
                          </View>
                        </View>
                      </View>
                    )}
                  </>
                </View>
                <View style={styles.buttonContainer}>
                  <CustomButton
                    text="クリア"
                    onPress={handleClearPress}
                    style={styles.grayMButton}
                    type="ButtonMediumGray"
                    textWidth={48}
                  />
                  <CustomButton
                    text="検索"
                    onPress={handleSearchPress}
                    style={styles.PrimaryMButton}
                    type="ButtonMPrimary"
                  />
                </View>
              </View>

              <View style={styles.parentPaginationContainer}>
                <View style={styles.topPaginationContainer}>
                  <View style={styles.countContainer}>
                    <HiraginoKakuText style={styles.paginationCount} normal>
                      {page}-{lastPage} / {totalCount} 件中
                    </HiraginoKakuText>
                  </View>
                  <Pressable
                    style={styles.sortingContainer}
                    onPress={handleSortingDropdownPress}
                  >
                    {timeAscending === undefined ? (
                      <MaterialCommunityIcons
                        name="swap-vertical"
                        size={20}
                        color={colors.activeCarouselColor}
                      />
                    ) : selectedOption == "最終更新日が新しい" ? (
                      <MaterialIcons
                        name="arrow-upward"
                        size={24}
                        color={colors.greyTextColor}
                      />
                    ) : (
                      <MaterialIcons
                        name="arrow-downward"
                        size={24}
                        color={colors.greyTextColor}
                      />
                    )}
                    <HiraginoKakuText style={styles.paginationCount} normal>
                      {selectedOption}
                    </HiraginoKakuText>
                    <Entypo
                      name="chevron-down"
                      size={20}
                      color={colors.greyTextColor}
                      style={styles.dropdownIconStyle}
                    />
                  </Pressable>

                  {/* Sorting Dropdown */}
                  {isDropdownVisible && (
                    <View style={styles.sortingDropdown}>
                      {dropdownData.map((item) => (
                        <Pressable
                          key={item.value}
                          style={styles.dropdownItem}
                          onPress={() => handleDropdownSelect(item.label)}
                        >
                          <HiraginoKakuText
                            style={styles.paginationCount}
                            normal
                          >
                            {item.label}
                          </HiraginoKakuText>
                        </Pressable>
                      ))}
                    </View>
                  )}
                </View>
                <View style={[styles.tableContainer]}>
                  {/* Render the header */}
                  {renderHeader()}
                  {/* Render the table items */}
                  {events == undefined || !loading ? (
                    <View style={styles.noDataContainer}>
                      <View style={styles.noDataChildContainer}>
                        <HiraginoKakuText style={styles.noDataText} normal>
                          {typeof errMsg === "string" && errMsg.length > 0}
                          {errMsg}
                          {typeof searchErrMsg === "string" &&
                            searchErrMsg.length > 0}

                          {searchErrMsg}
                        </HiraginoKakuText>
                      </View>
                    </View>
                  ) : (
                    <ScrollView
                      style={styles.tableItemContainer}
                    >
                      <View>
                        {getPageData().map((item, index) => (
                          <View key={index} style={{ zIndex: -index }}>
                            <View
                              onPointerLeave={handleMouseLeave}
                              style={[{ position: "relative" }]}
                            >
                              <Pressable
                                style={[
                                  styles.flexRow,
                                  hoveredEventId === item.event_id &&
                                  styles.dataContainer,
                                  hoveredEventId === item.event_id &&
                                  styles.rowhover,
                                ]}
                                onPress={() => handleEventDetail(item.event_id)}
                              >
                                <Pressable
                                  style={[
                                    styles.row,
                                    styles.rowpressed,
                                    hoveredEventId === item.event_id &&
                                    styles.rowhover,
                                  ]}
                                  onHoverIn={() =>
                                    handleMouseEnterRow(item.event_id)
                                  }
                                  onHoverOut={() => handleMouseLeaveRow}
                                  onPressIn={() =>
                                    handleMouseEnterRow(item.event_id)
                                  }
                                  onPressOut={() => handleMouseLeaveRow}
                                  onPress={() =>
                                    handleEventDetail(item.event_id)
                                  }
                                >
                                  <View
                                    style={[
                                      styles.cell,
                                      styles.receptionStatusBodyText,
                                    ]}
                                  >
                                    {item.event_status_name === "受付可能" ? (
                                      <View
                                        style={
                                          styles.receptionStatusPrimaryLabel
                                        }
                                      >
                                        <HiraginoKakuText
                                          style={
                                            styles.receptionStatusPrimaryLabelText
                                          }
                                        >
                                          受付可能
                                        </HiraginoKakuText>
                                      </View>
                                    ) : item.event_status_name ===
                                      "受付終了" ? (
                                      <View
                                        style={
                                          styles.receptionStatusDefaultLabel
                                        }
                                      >
                                        <HiraginoKakuText
                                          style={
                                            styles.receptionStatusDefaultLabelText
                                          }
                                        >
                                          受付終了
                                        </HiraginoKakuText>
                                      </View>
                                    ) : item.event_status_name === "準備中" ? (
                                      <View style={styles.receptionStatusLabel}>
                                        <HiraginoKakuText
                                          style={
                                            styles.receptionStatusLabelText
                                          }
                                        >
                                          準備中
                                        </HiraginoKakuText>
                                      </View>
                                    ) : (
                                      <View
                                        style={
                                          styles.receptionStatusArchiveLabel
                                        }
                                      >
                                        <HiraginoKakuText
                                          style={
                                            styles.receptionStatusArchiveLabelText
                                          }
                                        >
                                          アーカイブ
                                        </HiraginoKakuText>
                                      </View>
                                    )}
                                  </View>
                                  <View
                                    style={[
                                      styles.cell,
                                      styles.eventTableBodyContainer,
                                    ]}
                                  >
                                    <HiraginoKakuText
                                      style={styles.eventTableBodyText}
                                      numberOfLines={1}
                                    >
                                      {item.event_name}
                                    </HiraginoKakuText>
                                  </View>
                                  <View
                                    style={[
                                      styles.cell,
                                      styles.venueBodyContainer,
                                    ]}
                                  >
                                    <HiraginoKakuText
                                      style={styles.venueBodyText}
                                      numberOfLines={1}
                                      normal
                                    >
                                      {item.venue_names}
                                    </HiraginoKakuText>
                                  </View>
                                  <View style={styles.eventDateBodyContainer}>
                                    <HiraginoKakuText
                                      style={styles.eventDateBodyText}
                                      normal
                                    >
                                      {item.start_date &&
                                        item.end_date &&
                                        `${formatDate(
                                          item.start_date
                                        )} ~${formatDate(item.end_date)}`}
                                      {item.start_date &&
                                        !item.end_date &&
                                        `${formatDate(item.start_date)} ~`}
                                      {item.end_date &&
                                        !item.start_date &&
                                        `~${formatDate(item.end_date)}`}
                                    </HiraginoKakuText>
                                  </View>
                                  <View
                                    style={styles.acceptedPersonBodyContainer}
                                  >
                                    <HiraginoKakuText
                                      style={styles.acceptedPersonBodyText}
                                      numberOfLines={1}
                                    >
                                      {item.event_id === 1
                                        ? "121232321212"
                                        : item.count}
                                    </HiraginoKakuText>
                                  </View>
                                </Pressable>
                                <View style={styles.cellContainer}>
                                  <Pressable
                                    onHoverIn={() =>
                                      handleMouseEnter(item.event_id)
                                    }
                                    onHoverOut={() => handleMouseLeave}
                                    onPressIn={() =>
                                      handleMouseEnter(item.event_id)
                                    }
                                    onPressOut={() => handleMouseLeave}
                                    style={[
                                      styles.threeDotsContainer,
                                      styles.pressed,
                                      isHovered &&
                                      hoveredEventId === item.event_id &&
                                      styles.hover,
                                      openDropdownId === item.event_id && {
                                        zIndex: 10,
                                      },
                                    ]}
                                    onPress={() =>
                                      handleDropDown(item.event_id)
                                    }
                                  >
                                    <View
                                      style={[styles.threeDotsChildContainer]}
                                    >
                                      <MaterialIcons
                                        name="more-horiz"
                                        size={24}
                                        color={colors.greyTextColor}
                                      />
                                    </View>

                                    {openDropdownId === item.event_id &&
                                      showDropdown && (
                                        <View style={styles.dropdown}>
                                          <Pressable
                                            onPress={() =>
                                              setShowSecondDropdown(
                                                !showSecondDropdown
                                              )
                                            }
                                          >
                                            <HiraginoKakuText
                                              normal
                                              style={[
                                                styles.optionText,
                                                styles.bodyText,
                                              ]}
                                            >
                                              受付状況を変更
                                            </HiraginoKakuText>
                                          </Pressable>
                                          <Pressable
                                            onPress={() =>
                                              handleEditEvent(item.event_id)
                                            }
                                          >
                                            <HiraginoKakuText
                                              normal
                                              style={[
                                                styles.optionText,
                                                styles.bodyText,
                                              ]}
                                            >
                                              編集
                                            </HiraginoKakuText>
                                          </Pressable>
                                          {isPressableVisible && (
                                            <Pressable
                                              onPress={() =>
                                                handleReproductionEvent(
                                                  item.event_id
                                                )
                                              }
                                            >
                                              <HiraginoKakuText
                                                normal
                                                style={[
                                                  styles.optionText,
                                                  styles.bodyText,
                                                ]}
                                              >
                                                複製
                                              </HiraginoKakuText>
                                            </Pressable>
                                          )}

                                          <Pressable
                                            onPress={() =>
                                              handleDeleteEvent(item.event_id)
                                            }
                                          >
                                            <HiraginoKakuText
                                              normal
                                              style={[
                                                styles.optionText,
                                                styles.deleteBodyText,
                                              ]}
                                            >
                                              削除
                                            </HiraginoKakuText>
                                          </Pressable>
                                        </View>
                                      )}
                                    {openDropdownId === item.event_id &&
                                      showSecondDropdown && (
                                        <View style={styles.secondDropdown}>
                                          {item.event_status_name !==
                                            "準備中" && (
                                              <Pressable
                                                onPress={() =>
                                                  handleStatusChangeEvent(
                                                    "準備中",
                                                    item.event_id
                                                  )
                                                }
                                              >
                                                <HiraginoKakuText
                                                  normal
                                                  style={[
                                                    styles.optionText,
                                                    styles.bodyText,
                                                  ]}
                                                >
                                                  準備中
                                                </HiraginoKakuText>
                                              </Pressable>
                                            )}
                                          {item.event_status_name !==
                                            "受付可能" && (
                                              <Pressable
                                                onPress={() =>
                                                  handleStatusChangeEvent(
                                                    "受付可能",
                                                    item.event_id
                                                  )
                                                }
                                              >
                                                <HiraginoKakuText
                                                  normal
                                                  style={[
                                                    styles.optionText,
                                                    styles.bodyText,
                                                  ]}
                                                >
                                                  受付可能
                                                </HiraginoKakuText>
                                              </Pressable>
                                            )}
                                          {item.event_status_name !==
                                            "受付終了" && (
                                              <Pressable
                                                onPress={() =>
                                                  handleStatusChangeEvent(
                                                    "受付終了",
                                                    item.event_id
                                                  )
                                                }
                                              >
                                                <HiraginoKakuText
                                                  normal
                                                  style={[
                                                    styles.optionText,
                                                    styles.bodyText,
                                                  ]}
                                                >
                                                  受付終了
                                                </HiraginoKakuText>
                                              </Pressable>
                                            )}
                                          {item.event_status_name !==
                                            "アーカイブ" && (
                                              <Pressable
                                                onPress={() =>
                                                  handleStatusChangeEvent(
                                                    "アーカイブ",
                                                    item.event_id
                                                  )
                                                }
                                              >
                                                <HiraginoKakuText
                                                  normal
                                                  style={[
                                                    styles.optionText,
                                                    styles.bodyText,
                                                  ]}
                                                >
                                                  アーカイブ
                                                </HiraginoKakuText>
                                              </Pressable>
                                            )}
                                        </View>
                                      )}
                                  </Pressable>
                                </View>
                              </Pressable>
                            </View>
                          </View>
                        ))}
                      </View>
                    </ScrollView>
                  )}
                </View>

                {events != undefined && loading && totalPages > 1 && (
                  <View style={styles.onChangePageContainer}>
                    <View style={styles.previousButtonsContainer}>
                      <CustomButton
                        text=""
                        onPress={handleFirstPage}
                        style={
                          page === 1
                            ? styles.skipBackDisable
                            : styles.skipBackEnable
                        }
                        type={page === 1 ? "ButtonSDisable" : "ButtonSGray"}
                        icon={
                          <Feather
                            name="skip-back"
                            size={20}
                            color={colors.greyTextColor}
                          />
                        }
                        iconPosition="center"
                      />
                      <CustomButton
                        text=""
                        onPress={handlePrevPage}
                        style={
                          page === 1
                            ? styles.chevronDisable
                            : styles.chevronEnable
                        }
                        type={page === 1 ? "ButtonSDisable" : "ButtonSGray"}
                        icon={
                          <Feather
                            name="chevron-left"
                            size={20}
                            color={colors.greyTextColor}
                          />
                        }
                        iconPosition="center"
                      />
                    </View>
                    {totalPages > 1 && (
                      <View style={styles.pageNumberContainer}>
                        {renderPaginationButtons()}
                      </View>
                    )}

                    {totalPages > 1 && (
                      <View style={styles.nextButtonsContainer}>
                        <CustomButton
                          text=""
                          onPress={handleNextPage}
                          style={
                            page === lastPage
                              ? styles.chevronDisable
                              : styles.chevronEnable
                          }
                          type={
                            page === lastPage ? "ButtonSDisable" : "ButtonSGray"
                          }
                          icon={
                            <Feather
                              name="chevron-right"
                              size={20}
                              color={colors.textColor}
                            />
                          }
                          iconPosition="center"
                        />
                        <CustomButton
                          text=""
                          onPress={handleLastPage}
                          style={
                            page === lastPage
                              ? styles.skipForwardDisable
                              : styles.skipForwardEnable
                          }
                          type={
                            page === lastPage ? "ButtonSDisable" : "ButtonSGray"
                          }
                          icon={
                            <Feather
                              name="skip-forward"
                              size={20}
                              color={colors.textColor}
                            />
                          }
                          iconPosition="center"
                        />
                      </View>
                    )}
                  </View>
                )}
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>

      {showToast && (
        <View style={styles.modalBackground}>
          <View style={styles.toastMessage}>
            <HiraginoKakuText style={styles.toastText}>
              {toastMessage}
            </HiraginoKakuText>
          </View>
        </View>
      )}

      {isStatusChangeModalVisible && (
        <StatusChangeDialog
          statusName={selectedStatusName}
          onCancelButtonPress={handleCancelButton}
          onChangeButtonPress={handleChangeButton}
        />
      )}
      {isDeleteModalVisible && (
        <EventDeleteDialog
          onCancelButtonPress={handleDeleteCancelButton}
          onDeleteButtonPress={handleDeleteButton}
        />
      )}
      {isLogOutModalVisible && (
        <Logout
          onCancelButtonPress={handleLogoutCancelButton}
          onLogoutButtonPress={handleLogoutButton}
        />
      )}
    </SafeAreaView>
  );
};
