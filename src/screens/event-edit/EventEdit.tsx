import React, { useState, useRef, useEffect } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
  TextInput,
  ScrollView,
  Pressable,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";
import styles from "./EventEditStyles";
import { MaterialIcons, Entypo } from "@expo/vector-icons";
import { HiraginoKakuText } from "../../components/StyledText";
import { Header } from "../../components/basics/header";
import { colors } from "../../styles/color";
import { CustomButton } from "../../components/basics/Button";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { EventSaveDialog } from "../event-save-dialog/EventSaveDialog";
import { CustomCalendar } from "../../components/basics/Calendar";
import { format } from "date-fns";
import {
  fetchEventData,
  fetchEventIdByTitle,
  fetchEventStatusData,
  fetchEventVenueData,
  insertEventData,
  insertEventHistory,
  insertVenueData,
  insertVenueHistory,
  updateEventData,
  updateEventVenueData,
} from "./EventEditService";
import { EventDetailParams } from "../event-detail/EventDetailParams";
import { EventListParams } from "../event-list/EventListParams";
import { EventEditParams } from "./EventEditParams";
import { ActivityLogger } from "../../log/ActivityLogger";

type Props = {
  navigation: NavigationProp<any, any>;
};
type Params = {
  eventListParams: EventListParams;
  eventEditParams: EventEditParams;
};

interface Event {
  eventTitle: string;
  startDate: string;
  endDate: string;
  eventStatusCode: string;
}

export const EventEdit = ({ navigation }: Props) => {
  const route = useRoute();
  const { eventEditParams } = route.params as Params;

  const [inputBoxes, setInputBoxes] = useState<any[]>([]);
  const [updateBoxes, setUpdateBoxes] = useState<any[]>([]);
  const [deletedBoxes, setDeletedBoxes] = useState<any[]>([]);
  const [selectedButton, setSelectedButton] = useState<string>("");
  const [isStartDateCalendarVisible, setStartDateCalendarVisible] =
    useState(false);
  const [isEndDateCalendarVisible, setEndDateCalendarVisible] = useState(false);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const startDateInputRef = useRef(null);
  const endDateInputRef = useRef(null);
  const startDateRef = useRef(null);
  const endDateRef = useRef(null);

  const [title, setTitle] = useState<string>("");
  const [titleErrMsg, setTitleErrMsg] = useState("");
  const [startDateErrMsg, setstartDateErrMsg] = useState("");
  const [endDateErrMsg, setendDateErrMsg] = useState("");

  // EVENT Data
  const [originalEvent, setOriginalEvent] = useState<Event | null>(null);
  const [updatedEvent, setUpdatedEvent] = useState<Event | null>(null);

  useEffect(() => {
    ActivityLogger.insertInfoLogEntry(
      eventEditParams.user,
      "EventEdit",
      "useEffect",
      "screen open"
    );
    handleEventStatusRead();
    handleEventRead();
    handleVenueRead();
  }, []);

  const handleTextInputChange = (text: string, fieldName: string) => {
    if (fieldName == "title") {
      setTitle(text);
      setTitleErrMsg("");
    }
    if (fieldName == "startDate") {
      setStartDate(text.trim());
      setstartDateErrMsg("");
    }
    if (fieldName == "endDate") {
      setEndDate(text.trim());
      setendDateErrMsg("");
    }

    // HANDLE Update
    if (!updatedEvent) return;
    setUpdatedEvent({
      ...updatedEvent,
      eventTitle: fieldName === "title" ? text.trim() : updatedEvent.eventTitle,
      startDate:
        fieldName === "startDate" ? text.trim() : updatedEvent.startDate,
      endDate: fieldName === "endDate" ? text.trim() : updatedEvent.endDate,
      eventStatusCode: updatedEvent.eventStatusCode, // this line is unnecessary but clarifies all properties
    });
  };

  const handleVenueInputChange = (name: string, index: number) => {
    const venueId = inputBoxes[index].venue_id;
    const updatedInputBoxes = [...inputBoxes];
    const updatedUpdateBoxes = [...updateBoxes];
    updatedInputBoxes[index] = {
      venue_id: venueId,
      name: name,
      is_deleted: false,
    };
    updatedUpdateBoxes[index] = {
      venue_id: venueId,
      name: name,
      is_deleted: false,
    };
    setInputBoxes(updatedInputBoxes);
    setUpdateBoxes(updatedUpdateBoxes);
  };

  const addInputBox = (event: any) => {
    length = inputBoxes.length;
    setInputBoxes([
      ...inputBoxes,
      { venue_id: "", name: "", is_deleted: false },
    ]);
    setUpdateBoxes([
      ...updateBoxes,
      { venue_id: "", name: "", is_deleted: false },
    ]);
    closeCalendar(event);
  };

  const removeInputBox = (removedIndex: number) => {
    const venueId = inputBoxes[removedIndex].venue_id;
    setDeletedBoxes([...deletedBoxes, venueId]);
    setInputBoxes(inputBoxes.filter((_, index) => index !== removedIndex));
    setStartDateCalendarVisible(false);
    setEndDateCalendarVisible(false);
  };

  const handleTitleBlur = () => {
    if (title === "") {
      setTitleErrMsg("イベント名を入力してください");
    } else {
      setTitleErrMsg("");
    }
  };
  const handleStartDateBlur = () => {
    if (!startDate && endDate) {
      setstartDateErrMsg("開始日を選択してください");
      setendDateErrMsg("");
    } else if (startDate && !endDate) {
      setendDateErrMsg("終了日を選択してください");
      setstartDateErrMsg("");
    } else if (!endDate && !startDate) {
      setstartDateErrMsg("開始日を選択してください");
    } else if (endDate && startDate) {
      if (startDate > endDate) {
        setendDateErrMsg("開始日以降の日付を入力してください");
      } else {
        setendDateErrMsg("");
      }
    } else {
      setstartDateErrMsg("");
      setendDateErrMsg("");
    }
  };
  const handleEndDateBlur = () => {
    if (startDate && !endDate) {
      setendDateErrMsg("終了日を選択してください");
      setstartDateErrMsg("");
    } else if (!startDate && endDate) {
      setstartDateErrMsg("開始日を選択してください");
      setendDateErrMsg("");
    } else if (!endDate && !startDate) {
      setendDateErrMsg("終了日を選択してください");
    } else if (endDate && startDate) {
      if (startDate > endDate) {
        setendDateErrMsg("開始日以降の日付を入力してください");
      } else {
        setendDateErrMsg("");
      }
      setIsSaveModalVisible(false);
    } else {
      setstartDateErrMsg("");
      setendDateErrMsg("");
    }
  };

  const handleButtonPressMain = () => {
    if (title && !startDate && !endDate) {
      setstartDateErrMsg("");
      setendDateErrMsg("");
    } else if (title && startDate && endDate) {
      if (startDate <= endDate) {
        setstartDateErrMsg("");
        setendDateErrMsg("");
      }
    } else {
      if (!title) {
        handleTitleBlur();
      }
      if (!startDate && title) {
        handleStartDateBlur();
      } else if (!startDate && endDate) {
        handleStartDateBlur();
      } else {
        if (!title && !startDate && endDate) {
          setstartDateErrMsg("");
        }
      }
      if (!endDate && title) {
        handleEndDateBlur();
      } else if (!endDate && startDate) {
        handleEndDateBlur();
      } else {
        if (!title && startDate && !endDate) {
          setendDateErrMsg("");
        }
      }
    }
    setStartDateCalendarVisible(false);
    setEndDateCalendarVisible(false);
  };

  const handleButtonPress = (selectedButton: string) => {
    setSelectedButton(selectedButton);
    setStartDateCalendarVisible(false);
    setEndDateCalendarVisible(false);

    // HANDLE Update
    if (!updatedEvent) return;
    setUpdatedEvent({
      ...updatedEvent,
      eventStatusCode: selectedButton,
    });
  };

  // Event Save Dialog
  const [isSaveModalVisible, setIsSaveModalVisible] = useState(false);
  const handleCloseEventEdit = () => {
    setStartDateCalendarVisible(false);
    setEndDateCalendarVisible(false);
    setIsSaveModalVisible(true);
  };
  const handleCancelButton = () => {
    setIsSaveModalVisible(false);
    navigation.navigate("EventEdit", {
      eventEditParams,
    });
    ActivityLogger.insertInfoLogEntry(
      eventEditParams.user,
      "EventEdit",
      "handleCancelButton",
      "transition",
      "EventEdit",
      eventEditParams
    );
  };
  const handleNotSaveButton = () => {
    setIsSaveModalVisible(false);
    if (eventEditParams.editPgType === "EventDetail") {
      const eventDetailParams = new EventDetailParams();
      eventDetailParams.user = eventEditParams.user;
      eventDetailParams.eventId = eventEditParams.eventId;
      navigation.navigate("EventDetail", {
        eventDetailParams,
      });
      ActivityLogger.insertInfoLogEntry(
        eventEditParams.user,
        "EventEdit",
        "handleNotSaveButton",
        "transition",
        "EventDetail",
        eventDetailParams
      );
    } else {
      const eventListParams = new EventListParams();
      eventListParams.user = eventEditParams.user;
      navigation.navigate("EventList", {
        eventListParams,
      });
      ActivityLogger.insertInfoLogEntry(
        eventEditParams.user,
        "EventEdit",
        "handleNotSaveButton",
        "transition",
        "EventList",
        eventListParams
      );
    }
  };

  const handleSaveButton = async () => {
    handleButtonPressMain();
    handleTitleBlur();
    handleStartDateBlur();
    handleEndDateBlur();

    if (titleErrMsg == "" && startDateErrMsg == "" && endDateErrMsg == "") {
      const result = await handleDataBase();
      if (result) {
        if (eventEditParams.editPgType === "EventDetail") {
          const eventDetailParams = new EventDetailParams();
          eventDetailParams.user = eventEditParams.user;
          eventDetailParams.eventId = eventEditParams.eventId;
          eventDetailParams.toastMsg = "イベントを更新しました";
          navigation.navigate("EventDetail", {
            eventDetailParams,
          });
          ActivityLogger.insertInfoLogEntry(
            eventEditParams.user,
            "EventEdit",
            "handleSaveButton",
            "transition",
            "EventDetail",
            eventDetailParams
          );
        } else {
          const eventListParams = new EventListParams();
          eventListParams.user = eventEditParams.user;
          eventListParams.toastMsg = "イベントを更新しました";
          navigation.navigate("EventList", {
            eventListParams,
          });
          ActivityLogger.insertInfoLogEntry(
            eventEditParams.user,
            "EventEdit",
            "handleSaveButton",
            "transition",
            "EventList",
            eventListParams
          );
        }
      } else {
        if (eventEditParams.editPgType === "EventDetail") {
          const eventDetailParams = new EventDetailParams();
          eventDetailParams.user = eventEditParams.user;
          eventDetailParams.eventId = eventEditParams.eventId;
          navigation.navigate("EventDetail", {
            eventDetailParams,
          });
          ActivityLogger.insertInfoLogEntry(
            eventEditParams.user,
            "EventEdit",
            "handleNotSaveButton",
            "transition",
            "EventDetail",
            eventDetailParams
          );
        } else {
          const eventListParams = new EventListParams();
          eventListParams.user = eventEditParams.user;
          navigation.navigate("EventList", {
            eventListParams,
          });
          ActivityLogger.insertInfoLogEntry(
            eventEditParams.user,
            "EventEdit",
            "handleNotSaveButton",
            "transition",
            "EventList",
            eventListParams
          );
        }
      }
    }
    setIsSaveModalVisible(false);
  };

  const handleDataBase = async () => {
    // HANDLE Update
    const updatedData = getUpdatedData(originalEvent, updatedEvent);
    if (updatedData.length > 0) {
      handleHistoryInsert();
      const resultEventUpdate = await handleEventUpdate();
      if (resultEventUpdate !== "success") {
        return false;
      }

      const resultEventVenueUpdate = await handleEventVenueUpdate();
      if (resultEventVenueUpdate !== "success") {
        return false;
      }

      if (
        resultEventUpdate === "success" ||
        resultEventVenueUpdate === "success"
      ) {
        return true;
      }
    }
    return false;
  };

  const handleStartDateCalendarPress = (event: any) => {
    (startDateInputRef.current as any).focus();
    setStartDateCalendarVisible(!isStartDateCalendarVisible);
    setEndDateCalendarVisible(false);
    setstartDateErrMsg("");
  };

  const handleEndDateCalendarPress = (event: any) => {
    (endDateInputRef.current as any).focus();
    setEndDateCalendarVisible(!isEndDateCalendarVisible);
    setStartDateCalendarVisible(false);
    setendDateErrMsg("");
  };

  const handleStartDateSelect = (date: any) => {
    startDate == date ? setStartDate("") : setStartDate(date);
    setStartDateCalendarVisible(false);
    if (endDate) {
      if (date > endDate) {
        setendDateErrMsg("開始日以降の日付を入力してください");
        setstartDateErrMsg("");
      } else {
        setendDateErrMsg("");
        setstartDateErrMsg("");
      }
    } else {
      if (date) {
        setstartDateErrMsg("");
        handleEndDateBlur();
      }
    }
  };

  const handleEndDateSelect = (date: any) => {
    endDate == date ? setEndDate("") : setEndDate(date);
    setEndDateCalendarVisible(false);
    if (date < startDate) {
      setendDateErrMsg("開始日以降の日付を入力してください");
    } else {
      setendDateErrMsg("");
      if (!startDate) {
        handleStartDateBlur();
      }
    }
  };

  const closeCalendar = (event: any) => {
    if (
      event.nativeEvent.target != startDateInputRef.current &&
      event.nativeEvent.target != startDateRef
    ) {
      if (isStartDateCalendarVisible) {
        setStartDateCalendarVisible(false);
      }
    }
    if (
      event.nativeEvent.target != endDateInputRef.current &&
      event.nativeEvent.target != endDate
    ) {
      if (isEndDateCalendarVisible) {
        setEndDateCalendarVisible(false);
      }
    }
  };

  // AWS
  const [eventStatus, setEventStatus] = useState<any[]>([]);
  const [message, setMessage] = useState<string>("");

  const handleEventStatusRead = async () => {
    try {
      const result = await fetchEventStatusData();
      if (result.message === "success") {
        setMessage("Read Successfully!!");
        setEventStatus(result.data);
      }
    } catch (error) {
      console.error("Error from EventEditService:", error);
    }
  };

  const handleEventRead = async () => {
    try {
      const result = await fetchEventData(eventEditParams.eventId);
      if (result.message === "success") {
        setMessage("Read Successfully!!");
        setTitle(result.data[0].name);
        setStartDate(
          result.data[0].start_date
            ? format(new Date(result.data[0].start_date), "yyyy-MM-dd")
            : ""
        );
        setEndDate(
          result.data[0].end_date
            ? format(new Date(result.data[0].end_date), "yyyy-MM-dd")
            : ""
        );
        setSelectedButton(result.data[0].event_status_code);

        const eventData = {
          eventTitle: result.data[0].name,
          startDate: result.data[0].start_date,
          endDate: result.data[0].end_date,
          eventStatusCode: result.data[0].event_status_code,
        } as Event;

        setOriginalEvent(eventData);
        setUpdatedEvent(eventData);
      }
    } catch (error) {
      console.error("Error from EventEditService:", error);
    }
  };

  const handleVenueRead = async () => {
    try {
      const result = await fetchEventVenueData(eventEditParams.eventId);
      if (result.message === "success") {
        setMessage("Read Successfully!!");
        setInputBoxes(result.data);
        setUpdateBoxes(result.data);
      }
    } catch (error) {
      console.error("Error from EventEditService:", error);
    }
  };

  // INSERTION HISTORY
  const handleHistoryInsert = async () => {
    try {
      const resultEvent = await insertEventHistory(eventEditParams.eventId);
      if (resultEvent.message !== "success") {
        setMessage(resultEvent.message);
        return;
      }
      const resultVenue = await insertVenueHistory(eventEditParams.eventId);
      if (resultVenue.message !== "success") {
        setMessage(resultVenue.message);
        return;
      }
      setMessage("Inserted Successfully!!");
    } catch (error) {
      console.error("Error from EventEditService:", error);
    }
    return;
  };

  //INSERTION EVENT
  const handleEventInsert = async () => {
    try {
      const result = await insertEventData(
        "242152",
        0,
        eventEditParams.user.userId,
        title.trim(),
        startDate,
        endDate,
        selectedButton
      );
      if (result.message === "success") {
        setMessage("Inserted Successfully!!");
      }
    } catch (error) {
      console.error("Error from EventEditService:", error);
    }
  };

  const handleVenueInsert = async () => {
    const resultId = await fetchEventIdByTitle(title.trim());
    var getEventId = eventEditParams.eventId;
    const message = resultId.message;
    if (message !== "success") {
      setMessage(message);
      return;
    }
    try {
      for (let i = 0; i < updateBoxes.length; i++) {
        if (updateBoxes[i].name != "" && !updateBoxes[i].is_deleted) {
          const result = await insertVenueData(
            "242152",
            getEventId,
            0,
            eventEditParams.user.userId,
            updateBoxes[i].name
          );
          if (result.message !== "success") {
            return;
          }
        }
      }
      setMessage("Inserted Successfully!!");
    } catch (error) {
      console.error("Error from EventEditService:", error);
    }
  };

  //UPDATE EVENT
  const handleEventUpdate = async () => {
    const resultId = await fetchEventData(eventEditParams.eventId); // get the event id from event table []
    const message = resultId.message;

    if (message == "success") {
      try {
        const result = await updateEventData(
          eventEditParams.eventId,
          eventEditParams.user.userId,
          title.trim(),
          startDate,
          endDate,
          selectedButton
        );
        if (result.message !== "success") {
          return message;
        }
        setMessage("Updated Successfully!!");
      } catch (error) {
        console.error("Error from EventEditService:", error);
      }
    } else {
      handleEventInsert();
      setMessage(message);
    }
    return message;
  };

  //UPDATE EVENT_VENUE
  const handleEventVenueUpdate = async () => {
    var message = "";
    try {
      if (!eventEditParams.eventId) {
        handleVenueInsert();
      } else {
        for (let i = 0; i < updateBoxes.length; i++) {
          if (updateBoxes[i].venue_id != "") {
            deletedBoxes.includes(updateBoxes[i].venue_id)
              ? (updateBoxes[i].is_deleted = true)
              : (updateBoxes[i].is_deleted = false);
            const result = await updateEventVenueData(
              eventEditParams.eventId,
              eventEditParams.user.userId,
              updateBoxes[i].name,
              updateBoxes[i].venue_id,
              updateBoxes[i].is_deleted
            );
            message = result.message;
          }
          if (message !== "success") {
            if (updateBoxes[i].name != "" && !updateBoxes[i].is_deleted) {
              const result = await insertVenueData(
                "242152",
                eventEditParams.eventId,
                0,
                eventEditParams.user.userId,
                updateBoxes[i].name
              );
              if (result.message !== "success") {
                return message;
              }
            }
          }
        }
      }
      setMessage("Inserted Successfully!!");
      setTitle("");
      setStartDate("");
      setEndDate("");
      setSelectedButton("");
      return message;
    } catch (error) {
      console.error("Error from EventEditService:", error);
    }
  };

  //HANDLE Update
  const getUpdatedData = (
    originalEvent: Event | null,
    updatedEvent: Event | null
  ) => {
    if (!originalEvent || !updatedEvent) return [];

    const updatedParticipants: Event[] = [];

    if (JSON.stringify(originalEvent) !== JSON.stringify(updatedEvent)) {
      updatedParticipants.push(updatedEvent);
    }

    return updatedParticipants;
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <Header middleTitleName="イベント編集" buttonName="">
          <CustomButton
            text="閉じる"
            type="ButtonSDefault"
            icon={<Entypo name="cross" size={24} color={colors.primary} />}
            iconPosition="front"
            onPress={handleCloseEventEdit}
          />
        </Header>
        <ScrollView style={styles.scrollViewContent}>
          <TouchableWithoutFeedback onPress={closeCalendar}>
            <View style={styles.container}>
              <View style={styles.bodyContainer}>
                <View style={styles.eventTitleContainer}>
                  <View style={styles.eventHeadingLblContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      イベント名
                    </HiraginoKakuText>

                    <View style={styles.statusLabelContainer}>
                      <View
                        style={[styles.statusBox, styles.eventTitleStatusBox]}
                      >
                        <HiraginoKakuText
                          style={[
                            styles.statusLabelText,
                            styles.eventRedStatusLblText,
                          ]}
                        >
                          必須
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <View>
                    <TextInput
                      placeholder="イベントタイトル"
                      placeholderTextColor={colors.placeholderTextColor}
                      style={styles.eventInputBox}
                      onFocus={closeCalendar}
                      value={title}
                      onChangeText={(text) =>
                        handleTextInputChange(text, "title")
                      }
                      onBlur={handleTitleBlur}
                    />
                  </View>
                  {typeof titleErrMsg === "string" &&
                    titleErrMsg.length > 0 && (
                      <HiraginoKakuText style={styles.errText} normal>
                        {titleErrMsg}
                      </HiraginoKakuText>
                    )}
                </View>
                <View style={styles.eventTimeContainer}>
                  <View style={styles.eventTimeLblGpContainer}>
                    <View style={styles.eventHeadingLblContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        イベント期間
                      </HiraginoKakuText>
                      <View style={styles.statusLabelContainer}>
                        <View style={styles.statusBox}>
                          <HiraginoKakuText style={styles.statusLabelText}>
                            任意
                          </HiraginoKakuText>
                        </View>
                      </View>
                    </View>
                    <HiraginoKakuText normal style={styles.eventLabel}>
                      開始日と終了日を入力してください
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.dateTimeSelectContainer}>
                    <View style={styles.DateContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        開始日
                      </HiraginoKakuText>
                      <View style={styles.datePickerBox}>
                        <TextInput
                          ref={startDateInputRef}
                          style={[styles.bodyText, styles.dateInput]}
                          placeholder="日付を選択"
                          placeholderTextColor={colors.placeholderTextColor}
                          value={
                            startDate != ""
                              ? format(new Date(startDate), "yyyy/MM/dd")
                              : ""
                          }
                          onPressIn={handleStartDateCalendarPress}
                          onPointerDown={handleStartDateCalendarPress}
                          showSoftInputOnFocus={false}
                          onTouchStart={() => Keyboard.dismiss()}
                          editable={false}
                          onChangeText={(text) =>
                            handleTextInputChange(text, "startDate")
                          }
                          onBlur={handleStartDateBlur}
                        />
                        <Pressable
                          style={styles.calendarIconContainer}
                          ref={startDateRef}
                          onPress={handleStartDateCalendarPress}
                        >
                          <MaterialIcons
                            name="calendar-today"
                            size={22}
                            color={colors.activeCarouselColor}
                            style={styles.calendarIcon}
                          />
                        </Pressable>
                        {isStartDateCalendarVisible && (
                          <CustomCalendar
                            selectedDate={startDate}
                            onDateSelect={handleStartDateSelect}
                          />
                        )}
                      </View>

                      {typeof startDateErrMsg === "string" &&
                        startDateErrMsg.length > 0 && (
                          <HiraginoKakuText style={styles.errText} normal>
                            {startDateErrMsg}
                          </HiraginoKakuText>
                        )}
                    </View>

                    <View style={styles.waveDash}>
                      <Text style={styles.LabelLargeBold}> ~ </Text>
                    </View>

                    <View style={styles.DateContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        終了日
                      </HiraginoKakuText>
                      <View style={styles.datePickerBox}>
                        <TextInput
                          ref={endDateInputRef}
                          style={[styles.bodyText, styles.dateInput]}
                          placeholder="日付を選択"
                          placeholderTextColor={colors.placeholderTextColor}
                          value={
                            endDate != ""
                              ? format(new Date(endDate), "yyyy/MM/dd")
                              : ""
                          }
                          onPressIn={handleEndDateCalendarPress}
                          onPointerDown={handleEndDateCalendarPress}
                          showSoftInputOnFocus={false}
                          onTouchStart={() => Keyboard.dismiss()}
                          editable={false}
                          onChangeText={(text) =>
                            handleTextInputChange(text, "endDate")
                          }
                          onBlur={handleEndDateBlur}
                        />
                        <Pressable
                          style={styles.calendarIconContainer}
                          ref={endDateRef}
                          onPress={handleEndDateCalendarPress}
                        >
                          <MaterialIcons
                            name="calendar-today"
                            size={22}
                            color={colors.activeCarouselColor}
                            style={styles.calendarIcon}
                          />
                        </Pressable>
                        {isEndDateCalendarVisible && (
                          <CustomCalendar
                            selectedDate={endDate}
                            onDateSelect={handleEndDateSelect}
                          />
                        )}
                      </View>
                      {typeof endDateErrMsg === "string" &&
                        endDateErrMsg.length > 0 && (
                          <HiraginoKakuText style={styles.errText} normal>
                            {endDateErrMsg}
                          </HiraginoKakuText>
                        )}
                    </View>
                  </View>
                </View>
                <View style={styles.eventVenueContainer}>
                  <View style={styles.eventHeadingLblContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      会場
                    </HiraginoKakuText>
                    <View style={styles.statusLabelContainer}>
                      <View style={styles.statusBox}>
                        <HiraginoKakuText style={styles.statusLabelText}>
                          任意
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <View style={styles.venueInputBoxesContainer}>
                    {inputBoxes.map((venue, index) => (
                      <View key={index} style={styles.venueInputBox}>
                        <TextInput
                          placeholder="会場名"
                          placeholderTextColor={colors.placeholderTextColor}
                          style={[styles.eventInputBox, styles.venueInput]}
                          onFocus={closeCalendar}
                          value={venue.name}
                          onChangeText={(text) =>
                            handleVenueInputChange(text, index)
                          }
                        />
                        {inputBoxes.length > 1 && (
                          <TouchableOpacity
                            onPress={() => removeInputBox(index)}
                            style={styles.venueCrossIconContainer}
                          >
                            <MaterialIcons
                              name="close"
                              size={22}
                              color="#515867"
                            />
                          </TouchableOpacity>
                        )}
                      </View>
                    ))}
                    <TouchableOpacity
                      style={styles.venueBtnContainer}
                      onPress={addInputBox}
                    >
                      <MaterialIcons name="add" size={22} color="#515867" />
                      <HiraginoKakuText
                        style={[
                          styles.bodyText,
                          styles.LabelLargeBold,
                          styles.venueBtnText,
                        ]}
                      >
                        会場を追加
                      </HiraginoKakuText>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.horizontalLine} />
                <View style={styles.eventReceptionContainer}>
                  <View style={styles.eventHeadingLblContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      イベントの受付状況
                    </HiraginoKakuText>
                    <View style={styles.statusLabelContainer}>
                      <View
                        style={[styles.statusBox, styles.eventTitleStatusBox]}
                      >
                        <HiraginoKakuText
                          style={[
                            styles.statusLabelText,
                            styles.eventRedStatusLblText,
                          ]}
                        >
                          必須
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <HiraginoKakuText normal style={styles.eventLabel}>
                    受付可能にすると受付アプリに表示されます
                  </HiraginoKakuText>
                  <View style={styles.eventReceptionBtnContainer}>
                    {eventStatus.map((eventStatus) => (
                      <TouchableOpacity
                        key={eventStatus.event_status_code}
                        style={[
                          styles.receptionBtn,
                          selectedButton === eventStatus.event_status_code &&
                            styles.btnSelected,
                          eventStatus.name === "受付可能" &&
                            styles.eventMedianBtn,
                          eventStatus.name === "受付終了" &&
                            styles.eventMedianBtn,
                          eventStatus.name === "アーカイブ" &&
                            styles.eventLargeBtn,
                        ]}
                        onPress={() =>
                          handleButtonPress(eventStatus.event_status_code)
                        }
                      >
                        <Text
                          style={[
                            eventStatus.name === "準備中" &&
                              styles.receptionText,
                            eventStatus.name === "受付可能" &&
                              styles.buttonText,
                            eventStatus.name === "受付終了" &&
                              styles.buttonText,
                            eventStatus.name === "アーカイブ" &&
                              styles.archiveButtonText,
                            styles.LabelLargeBold,
                          ]}
                        >
                          {eventStatus.name}
                        </Text>
                      </TouchableOpacity>
                    ))}
                  </View>
                </View>
              </View>
              <View style={styles.createBtnContainer}>
                <TouchableOpacity
                  style={[
                    styles.createBtn,
                    selectedButton === "create" && styles.btnSelected,
                  ]}
                  onPress={handleSaveButton}
                >
                  <HiraginoKakuText
                    style={[styles.createBtnText, styles.LabelLargeBold]}
                  >
                    保存する
                  </HiraginoKakuText>
                </TouchableOpacity>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
        {isSaveModalVisible && (
          <EventSaveDialog
            onUnsaveButtonPress={handleNotSaveButton}
            onSaveButtonPress={handleSaveButton}
            onCancelButtonPress={handleCancelButton}
          />
        )}
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};
