import { User } from "../../model/User";

export class EventEditParams implements Params {
  private _user: User = new User();
  private _eventId: number = 0;
  private _pgType: string = "";
  private _editPgType: string = "";

  //getter
  get user(): User {
    return this._user.clone();
  }

  get eventId(): number {
    return this._eventId;
  }
  get pgType(): string {
    return this._pgType;
  }

  get editPgType(): string {
    return this._editPgType;
  }

  //setter
  set user(value: User) {
    this._user = value;
  }

  set eventId(value: number) {
    this._eventId = value;
  }

  set pgType(value: string) {
    this._pgType = value;
  }
  set editPgType(value: string) {
    this._editPgType = value;
  }

  getAllValuesAsString(): string {
    let paramsString = '[ClassName=EventEditParams';
    paramsString += ',_user=' + this._user.getAllValuesAsString();
    paramsString += ',_eventId=' + this._eventId;
    paramsString += ',_pgType=' + this._pgType;
    paramsString += ',_editPgType=' + this._editPgType;
    paramsString += ']';
    return paramsString
  }
}
