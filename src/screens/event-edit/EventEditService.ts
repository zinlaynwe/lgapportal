import { executeQuery } from "../../aws/db/dbOperation";
import { getCurrentJapanTime } from "../../environments/TimeUtils";
import { ActivityLogger } from "../../log/ActivityLogger";
import { User } from "../../model/User";

// get Data from Event_Status
export const fetchEventStatusData = async () => {
  const method = "POST";
  const queryString =
    "SELECT event_status_code, name FROM event_status WHERE is_show_on_update ORDER BY event_status_code;";
  ActivityLogger.insertInfoLogEntry(new User(), 'EventEdit', 'fetchEventStatusData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

// get event data
export const fetchEventData = async (eventId: number) => {
  const method = "POST";
  const queryString =
  `SELECT name, start_date, end_date, event_status_code FROM event WHERE city_code = '242152' AND event_id = ${eventId};`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventEdit', 'fetchEventData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
}

// get venue data
export const fetchEventVenueData = async (eventId: number) => {
  const method = "POST";
  const queryString =
  `SELECT venue_id, name, is_deleted FROM venue WHERE city_code = '242152' AND event_id = ${eventId} AND NOT is_deleted ORDER BY venue_id;`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventEdit', 'fetchEventVenueData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
}

//insert event_history
export const insertEventHistory = async (
  eventId: number
) => {
  const method = "POST";

  const queryString = `INSERT INTO
  event_history
  SELECT
   * 
  FROM
   event 
  WHERE 
   city_code = '242152'
   AND event_id = ${eventId};`;
   ActivityLogger.insertInfoLogEntry(new User(), 'EventEdit', 'insertEventHistory', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

//insert venue_history
export const insertVenueHistory = async (
  eventId: number
) => {
  const method = "POST";

  const queryString = `INSERT INTO
  venue_history
  SELECT
   * 
  FROM
   venue 
  WHERE 
   city_code = '242152'
   AND event_id = ${eventId}
   AND NOT is_deleted;`;
   ActivityLogger.insertInfoLogEntry(new User(), 'EventEdit', 'insertVenueHistory', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

//insert event
export const insertEventData = async (
  cityCode: string,
  historyNumber: number,
  modifierId: string,
  name: string,
  startDate: string,
  endDate: string,
  eventStatusCode?: string
) => {
  const method = "POST";

  function formatDate(dateString: any) {
    if (!dateString) return null;
    const date = new Date(dateString);
    if (isNaN(date.getTime())) return null;
    const formattedDate = date.toISOString().split("T")[0];
    return formattedDate;
  }

  const formattedStartDate = formatDate(startDate);
  const formattedEndDate = formatDate(endDate);
  const currentJapanTime = getCurrentJapanTime();

  const queryString = `INSERT INTO event (
    city_code, 
    history_number, 
    modifier_id, 
    modification_timestamp, 
    is_deleted, 
    name, 
    creation_timestamp, 
    start_date, 
    end_date, 
    event_status_code) 
    VALUES (
      '${cityCode}', 
      ${historyNumber}, 
      '${modifierId}',  
      '${currentJapanTime}', 
      false, 
      '${name}', 
      '${currentJapanTime}', 
      ${formattedStartDate ? `'${formattedStartDate}'` : "NULL"}, 
      ${formattedEndDate ? `'${formattedEndDate}'` : "NULL"}, 
      '${eventStatusCode}');`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventEdit', 'insertEventData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

//get event data for insertion of venue
export const fetchEventIdByTitle = async (eventName: string) => {
  const method = "POST";
  const queryString =
    "SELECT event_id FROM event WHERE name ='" +
    eventName +
    "'ORDER BY event_id DESC LIMIT 1;";
  ActivityLogger.insertInfoLogEntry(new User(), 'EventEdit', 'fetchEventIdByTitle', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

//insert venue
export const insertVenueData = async (
  cityCode: string,
  eventId: number,
  historyNumber: number,
  modifierId: string,
  name: string
) => {  
  const currentJapanTime = getCurrentJapanTime();
  const method = "POST";
  const queryString = `INSERT INTO venue (
    city_code, 
    event_id,
    history_number, 
    modifier_id, 
    modification_timestamp, 
    is_deleted, 
    name) 
    VALUES (
      '${cityCode}',
      ${eventId},
      ${historyNumber}, 
      '${modifierId}',
      '${currentJapanTime}', 
      false, 
      '${name}');`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventEdit', 'insertVenueData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const updateEventData = async (
  eventId: number,
  modifierId: string,
  name: string,
  startDate: string, 
  endDate: string,
  eventStatusCode: string
) => {
  const method = "POST";
  const code = 0;

  function formatDate(dateString: any) {
    if (!dateString) return null;
    const date = new Date(dateString);
    if (isNaN(date.getTime())) return null;
    const formattedDate = date.toISOString().split("T")[0];
    return formattedDate;
  }

  const formattedStartDate = formatDate(startDate);
  const formattedEndDate = formatDate(endDate);
  const currentJapanTime = getCurrentJapanTime();
  
  const queryString = `UPDATE event
  SET
  history_number = history_number + 1,
  modifier_id = '${modifierId}',
  modification_timestamp = '${currentJapanTime}',
  name = '${name}',
  start_date = ${formattedStartDate ? `'${formattedStartDate}'` : "NULL"},
  end_date = ${formattedEndDate ? `'${formattedEndDate}'` : "NULL"},
  event_status_code = '${eventStatusCode}'
  WHERE city_code = '242152' AND event_id = ${eventId};`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventEdit', 'updateEventData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const updateEventVenueData = async (
  eventId: number,
  modifierId: string,
  name: string,
  venueId: number,
  isDeleted: boolean
) => {
  const method = "POST";
  const currentJapanTime = getCurrentJapanTime();

  const queryString = `UPDATE venue
  SET
  history_number = history_number + 1,
  modifier_id = '${modifierId}',
  modification_timestamp = '${currentJapanTime}',
  name = '${name}',
  is_deleted = ${isDeleted}
  WHERE city_code = '242152' AND event_id = ${eventId} AND venue_id = ${venueId};`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventEdit', 'updateEventVenueData', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};