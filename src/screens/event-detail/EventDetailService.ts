import { executeQuery } from "../../aws/db/dbOperation";
import { getCurrentJapanTime } from "../../environments/TimeUtils";
import { ActivityLogger } from "../../log/ActivityLogger";
import { User } from "../../model/User";

// get Data from Event_Status
export const fetchEvent = async (eventId: number) => {
  const method = "POST";
  const queryString =
    `SELECT name,start_date,end_date,event_status_code FROM event WHERE city_code='242152' 
    AND event_id='${eventId}'`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventDetail', 'fetchEvent', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const fetchVenue = async (eventId: number) => {
  const method = "POST";
  const queryString = `SELECT name,venue_id FROM venue WHERE city_code='242152' AND event_id='${eventId}'
  AND NOT is_deleted ORDER BY venue_id`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventDetail', 'fetchVenue', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const fetchReception = async (eventId: number) => {
  const method = "POST";
  const queryString = `SELECT
  venue_id,
  accepted_timestamp,
  lgap_id,
  user_rank,
  lastname,
  firstname,
  date_of_birth,
  a.gender_code,
  d.name AS gender_name,
  address,
  a.reception_type_code,
  b.short_name,
  family_order_number,
  member_size_per_group,
  a.reception_id,
  member_id,
  accepted_terminal_id,
  modifier_id,
  modification_timestamp,
  lastname_kana,
  firstname_kana,
  postal_code,
  relationship
FROM
  reception a
  LEFT JOIN reception_type b ON a.reception_type_code = b.reception_type_code
  LEFT JOIN (
    SELECT
      reception_id,
      COUNT(*) AS member_size_per_group
    FROM
      reception
    WHERE
      city_code = '242152'
      AND event_id = '${eventId}'
      AND NOT is_deleted
    GROUP BY
      reception_id
  ) c ON a.reception_id = c.reception_id
  LEFT JOIN gender d ON a.gender_code = d.gender_code
WHERE
  city_code = '242152'
  AND event_id = '${eventId}'
  AND NOT is_deleted
ORDER BY
  accepted_timestamp DESC,
  a.reception_id DESC,
  family_order_number ASC`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventDetail', 'fetchReception', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const insertEventHistory = async (eventId: number) => {
  const method = "POST";

  const queryString = `INSERT INTO 
  event_history 
SELECT 
  * 
FROM 
  event 
WHERE 
  city_code = '242152'
  AND event_id = ${eventId}`;

  ActivityLogger.insertInfoLogEntry(new User(), 'EventDetail', 'insertEventHistory', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const updateEvent = async (
  eventId: number,
  selectedStatusName: string,
  modifierId: string
) => {
  const method = "POST";

  const currentJapanTime = getCurrentJapanTime();

  const queryString = `UPDATE event 
SET 
  history_number = history_number + 1,
  modifier_id= '${modifierId}',
  modification_timestamp = '${currentJapanTime}',
  event_status_code = CASE 
                      WHEN '${selectedStatusName}' = '準備中' THEN '1'
                      WHEN '${selectedStatusName}' = '受付可能' THEN '2'
                      WHEN '${selectedStatusName}' = '受付終了' THEN '3'
                      ELSE '4'
                    END
WHERE
  city_code = '242152'
  AND event_id=${eventId}`;

  ActivityLogger.insertInfoLogEntry(new User(), 'EventDetail', 'updateEvent', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};

export const logicalDeleteEvent = async (
  eventId: number,
  modifierId: string
) => {
  const method = "POST";

  const currentJapanTime = getCurrentJapanTime();

  const queryString = `UPDATE event 
SET 
  history_number = history_number + 1,
  modifier_id= '${modifierId}',
  modification_timestamp = '${currentJapanTime}',
  is_deleted= true
WHERE
  city_code = '242152'
  AND event_id=${eventId}`;
  ActivityLogger.insertInfoLogEntry(new User(), 'EventDetail', 'logicalDeleteEvent', 'execute query', '', null, queryString);
  return executeQuery(method, queryString);
};