import { StyleSheet, Platform, Dimensions } from "react-native";
import { colors } from "../../styles/color";
import {
  LabelMediumBold,
  HeadingXSmallBold,
  LabelLargeRegular,
  HeadingXxxSmallBold,
  HeadingXxSmallBold,
  HeadingMediumBold,
  LabelLargeBold,
  LabelMediumRegular,
  LabelSmallBold,
  ButtonSmallBold,
  BodyTextLarge,
  HeadingXxxxSmallBold,
} from "../../styles/typography";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const screenWidth = Dimensions.get("window").width;

export const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    height: "100%",
  },
  warningContainer: {
    padding: 12,
    gap: wp("0.65%"),
    backgroundColor: colors.warningBackgroundColor,
  },
  warningItemContainer: {
    gap: wp("0.65%"),
    backgroundColor: colors.warningBackgroundColor,
    flexDirection: "row",
  },
  warningIcon: {
    height: 20,
  },
  warningTextContainer: {
    width: "100%",
    flexDirection: "column",
    gap: hp("0.65%"),
  },
  warningTitle: {
    fontSize: HeadingXxxxSmallBold.size,
    lineHeight: HeadingXxxxSmallBold.lineHeight,
  },
  warningText: {
    fontSize: BodyTextLarge.size,
    lineHeight: BodyTextLarge.lineHeight,
  },
  btnMakeAccept: {
    width: wp("10.7%"),
    height: 32,
    borderRadius: 4,
    borderWidth: 1,
    paddingVertical: hp("0.45%"),
    paddingHorizontal: wp("0.65%"),
    gap: 8,
    borderColor: colors.gray,
  },
  firstParentContainer: {
    paddingTop: hp("1.55%"),
    paddingHorizontal: wp("2%"),
    paddingBottom: 0,
    gap: hp("2.05%"),
    backgroundColor: colors.secondary,
  },
  contentsContainer: {
    gap: hp("1.05%"),
  },
  btnsContainer: {
    gap: wp("1.35%"),
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  eventNameContainer: {
    gap: hp("1.05%"),
    width: wp("85%"),
  },
  eventLabel: {
    width: 88,
    height: 25,
    borderRadius: 1,
    borderWidth: 1,
    paddingVertical: 2,
    paddingHorizontal: 8,
    gap: 8,
    borderColor: colors.primary,
    justifyContent: "center",
  },
  stillPrepareStyle: {
    borderColor: colors.warningColor,
  },
  availableStyle: {
    backgroundColor: colors.primary,
  },
  closeStyle: {
    borderColor: colors.primary,
  },
  archiveStyle: {
    backgroundColor: colors.greyTextColor,
    borderColor: colors.greyTextColor,
  },
  receptionStatus: {
    color: colors.primary,
    textAlign: "center",
    fontSize: LabelMediumBold.size,
    lineHeight: LabelMediumBold.lineHeight,
  },
  eventTitleText: {
    fontSize: HeadingXSmallBold.size,
    lineHeight: HeadingXSmallBold.lineHeight,
  },
  editBtnContainer: {
    display: "flex",
    flexDirection: "row",
  },
  pencilIcon: {
    width: 44,
    height: 44,
    borderRadius: 4,
    borderWidth: 1,
    padding: 14,
    gap: 8,
    borderColor: colors.gray,
  },
  moreIcon: {
    width: 44,
    height: 44,
    borderRadius: 4,
    borderWidth: 1,
    padding: 14,
    gap: 8,
    borderColor: colors.gray,
    marginLeft: 8,
  },
  showDataContainer: {
    gap: hp("0.5%"),
    width: wp("33.6%"),
    zIndex: -1,
  },
  eventPeriodContainer: {
    flexDirection: "row",
  },
  eventPeriodLabel: {
    fontSize: LabelLargeRegular.size,
    lineHeight: LabelLargeRegular.lineHeight,
  },
  eventPeriodData: {
    fontSize: LabelLargeRegular.size,
    lineHeight: LabelLargeRegular.lineHeight,
  },
  venueContainer: {
    flexDirection: "row",
    gap: wp("0.7%"),
    width: wp("90%"),
  },
  venueLabel: {
    fontSize: LabelLargeRegular.size,
    lineHeight: LabelLargeRegular.lineHeight,
  },
  venueData: {
    fontSize: LabelLargeRegular.size,
    lineHeight: LabelLargeRegular.lineHeight,
  },
  tabsContainer: {
    gap: wp("1.35%"),
    flexDirection: "row",
    width: wp("33.6%"),
  },
  venueTab: {
    gap: hp("0.5%"),
    paddingVertical: hp("0.5%"),
    paddingHorizontal: wp("0.7%"),
  },
  currentTab: {
    borderBottomWidth: 2,
    borderColor: colors.primary,
  },
  venueText: {
    color: colors.greyTextColor,
    fontSize: HeadingXxxSmallBold.size,
    lineHeight: HeadingXxxSmallBold.lineHeight,
  },
  currentText: {
    color: colors.greyTextColor,
  },
  secondParentContainer: {
    paddingTop: 24,
    paddingHorizontal: wp("2%"),
    paddingBottom: 120,
    gap: 24,
    zIndex: -1,
  },
  statisticsContainer: {
    borderRadius: 8,
    paddingVertical: hp("1.52%"),
    paddingHorizontal: wp("1.99%"),
    gap: hp("1.55%"),
    backgroundColor: colors.secondary,
  },
  topContainer: {
    gap: wp("0.7%"),
    flexDirection: "row",
    alignItems: "center",
    display: "flex",
    justifyContent: "space-between",
  },
  statisticsText: {
    fontSize: HeadingXxSmallBold.size,
    lineHeight: HeadingXxSmallBold.lineHeight,
  },
  csvDownloadButton: {
    width: 173,
    height: 44,
    borderRadius: 4,
    borderWidth: 1,
    paddingVertical: 10,
    paddingHorizontal: 20,
    gap: 8,
    borderColor: colors.gray,
  },
  numberOfPeople: {
    gap: wp("1.31%"),
    flexDirection: "row",
    alignItems: "center",
  },
  numOfPeopleLabel: {
    fontSize: HeadingXxSmallBold.size,
    lineHeight: HeadingXxSmallBold.lineHeight,
  },
  lineSeparator: {
    borderWidth: 1,
    borderColor: colors.gray,
  },
  numOfPeopleContainer: {
    gap: wp("0.7%"),
    flexDirection: "row",
    alignItems: "center",
  },
  numOfPeopleValue: {
    fontSize: HeadingMediumBold.size,
    lineHeight: HeadingMediumBold.lineHeight,
    color: colors.primary,
  },
  hitoLabel: {
    fontSize: HeadingXSmallBold.size,
    lineHeight: HeadingXSmallBold.lineHeight,
  },
  ellipseContainer: {
    gap: wp("1.99%"),
    flexDirection: "row",
    alignItems: "center",
  },
  pieChartContainer: {
    gap: wp("1.31%"),
    width: wp("27.97%"),
    flexDirection: "row",
    alignItems: "center",
  },
  userCountsContainer: {
    gap: hp("0.5%"),
  },
  firstCountContainer: {
    gap: wp("0.7%"),
    flexDirection: "row",
    alignItems: "center",
  },
  firstCountRectangle: {
    width: 12,
    height: 12,
    borderRadius: 3,
    backgroundColor: colors.skyBlueColor,
  },
  firstCountText: {
    fontSize: LabelLargeBold.size,
    lineHeight: LabelLargeBold.lineHeight,
  },
  secondCountContainer: {
    gap: wp("0.7%"),
    flexDirection: "row",
    alignItems: "center",
  },
  secondCountRectangle: {
    width: 12,
    height: 12,
    borderRadius: 3,
    backgroundColor: colors.lightBlueColor,
  },
  thirdCountRectangle: {
    width: 12,
    height: 12,
    borderRadius: 3,
    backgroundColor: colors.paleBlueColor,
  },
  secondCountText: {
    fontSize: LabelLargeBold.size,
    lineHeight: LabelLargeBold.lineHeight,
  },
  verticalSeparator: {
    width: 1,
    height: 70,
    borderWidth: 1,
    borderColor: colors.greyTextColor,
  },
  paginationContainer: {
    borderRadius: 8,
    paddingTop: 24,
    paddingHorizontal: 24,
    paddingBottom: 40,
    gap: 24,
    backgroundColor: colors.secondary,
  },
  paginationHeaderContainer: {
    gap: 24,
  },
  paginationHeaderText: {
    fontSize: HeadingXxSmallBold.size,
    lineHeight: HeadingXxSmallBold.lineHeight,
  },
  dropDownBtnContainer: {
    gap: 8,
    flexDirection: "row",
  },
  dropdownButton: {
    minWidth: 132,
    height: 44,
    borderRadius: 4,
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
  },
  smallDropdownButton: {
    minWidth: 100,
    height: 44,
    borderRadius: 4,
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: "row",
  },
  topPaginationContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    zIndex: -1,
  },
  countContainer: {
    justifyContent: "center",
    backgroundColor: colors.secondary,
  },
  paginationCount: {
    fontSize: LabelLargeRegular.size,
    lineHeight: LabelLargeRegular.lineHeight,
  },
  noDataContainer: {
    height: 182,
    paddingVertical: 64,
    paddingHorizontal: 12,
    gap: 4,
    alignItems: "center",
    justifyContent: "center",
  },
  noDataChildContainer: {
    width: "auto",
    height: 54,
  },
  noDataText: {
    fontSize: BodyTextLarge.size,
    lineHeight: BodyTextLarge.lineHeight,
    textAlign: "center",
  },
  // Pagination
  header: {
    flexDirection: "row",
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: colors.gray,
  },
  receptionDateTimeHeader: {
    width: wp("13.06%"),
    flexDirection: "row",
    paddingVertical: hp("0.5%"),
    paddingHorizontal: wp("1%"),
    gap: wp("0.34%"),
  },
  headerText: {
    color: colors.greyTextColor,
    fontSize: LabelLargeBold.size,
    lineHeight: LabelLargeBold.lineHeight,
  },
  receptionMethodHeader: {
    width: wp("9.4%"),
    ...Platform.select({
      web: {
        width: wp("10.4%"),
      },
    }),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: hp("0.5%"),
    paddingHorizontal: wp("1%"),
    gap: wp("0.34%"),
  },
  identificationHeader: {
    width: wp("9.4%"),
    ...Platform.select({
      web: {
        width: wp("10.4%"),
      },
    }),
    flexDirection: "row",
    paddingVertical: hp("0.5%"),
    paddingHorizontal: wp("1%"),
    gap: wp("0.34%"),
  },
  receptionTargetHeader: {
    width: wp("8.4%"),
    flexDirection: "row",
    paddingVertical: hp("0.5%"),
    paddingHorizontal: wp("1%"),
    gap: wp("0.34%"),
  },
  acceptedPersonHeader: {
    width: wp("13.75%"),
    ...Platform.select({
      web: {
        width: wp("11.75%"),
      },
    }),
    flexDirection: "row",
    paddingVertical: hp("0.5%"),
    paddingHorizontal: wp("1%"),
    gap: wp("0.34%"),
  },
  genderHeader: {
    width: wp("5.5%"),
    flexDirection: "row",
    paddingVertical: hp("0.5%"),
    paddingHorizontal: wp("1%"),
    gap: wp("0.34%"),
  },
  dateOfBirthHeader: {
    width: wp("14.41%"),
    flexDirection: "row",
    paddingVertical: hp("0.5%"),
    paddingHorizontal: wp("1%"),
    gap: wp("0.34%"),
  },
  addressHeader: {
    width: wp("18.1%"),
    flexDirection: "row",
    paddingVertical: hp("0.5%"),
    paddingHorizontal: wp("1%"),
    gap: wp("0.34%"),
  },
  row: {
    height: 50,
    flexDirection: "row",
    backgroundColor: colors.secondary,
    borderBottomWidth: 1,
    borderColor: colors.gray,
    alignItems: "center",
  },
  rowHovered: {
    backgroundColor: colors.gray,
  },
  cell: {
    color: colors.textColor,
    paddingVertical: hp("0.75%"),
    paddingHorizontal: wp("1%"),
    gap: 8,
  },
  receptionDateTimeContainer: {
    width: wp("13.06%"),
  },
  receptionDateTimeText: {
    width: wp("11.17%"),
    color: colors.textColor,
    fontSize: screenWidth < 1194 ? 12 : LabelMediumRegular.size,
  },
  receptionMethodContainer: {
    width: wp("9.4%"),
    ...Platform.select({
      web: {
        width: wp("10.4%"),
      },
    }),
  },
  receptionMethodText: {
    textAlign: "center",
    color: colors.textColor,
    fontSize: screenWidth < 1194 ? 12 : LabelMediumRegular.size,
  },
  identificationContainer: {
    width: wp("9.4%"),
    ...Platform.select({
      web: {
        width: wp("10.4%"),
      },
    }),
  },
  identification: {
    width: 88,
    height: 24,
    borderRadius: 50,
    alignItems: "center",
  },
  checkcircleStyle: {
    height: 20,
  },
  noneText: {
    fontSize: LabelSmallBold.size,
    color: colors.textColor,
  },
  receptionTargetContainer: {
    width: wp("8.4%"),
  },
  representativeLabel: {
    width: 64,
    height: 26,
    borderRadius: 4,
    paddingHorizontal: 8,
    justifyContent: "center",
    gap: 8,
    backgroundColor: colors.greenLight,
    ...Platform.select({
      android: {
        width: "auto",
        paddingVertical: 2,
      },
    }),
  },
  representativeLabelText: {
    textAlign: "center",
    color: colors.textColor,
    fontSize: LabelSmallBold.size,
  },
  receptionTogetherLabel: {
    width: 76,
    height: 26,
    borderRadius: 4,
    paddingHorizontal: 8,
    justifyContent: "center",
    gap: 8,
    backgroundColor: colors.bodyBackgroundColor,
    ...Platform.select({
      android: {
        width: "auto",
        paddingVertical: 2,
      },
    }),
  },
  receptionTogetherText: {
    textAlign: "center",
    color: colors.textColor,
    fontSize: LabelSmallBold.size,
  },
  onlyPersonLabel: {
    width: 64,
    height: 26,
    borderRadius: 4,
    paddingHorizontal: 8,
    justifyContent: "center",
    gap: 8,
    backgroundColor: colors.neuralLightBlueColor,
    ...Platform.select({
      android: {
        width: "auto",
        paddingVertical: 2,
      },
    }),
  },
  onlyPersonLabelText: {
    textAlign: "center",
    color: colors.textColor,
    fontSize: LabelSmallBold.size,
  },
  acceptedPersonContainer: {
    width: wp("13.75%"),
    ...Platform.select({
      web: {
        width: wp("11.75%"),
      },
    }),
  },
  acceptedPersonText: {
    textAlign: "left",
    color: colors.primary,
    fontSize: screenWidth < 1194 ? 12 : LabelLargeRegular.size,
  },
  genderContainer: {
    width: wp("5.5%"),
  },
  genderText: {
    width: wp("3.54%"),
    textAlign: "left",
    color: colors.textColor,
    fontSize: screenWidth < 1194 ? 12 : LabelMediumRegular.size,
  },
  dateOfBirthContainer: {
    width: wp("14.41%"),
  },
  dateOfBirthText: {
    width: wp("13.25%"),
    color: colors.textColor,
    fontSize: screenWidth < 1194 ? 12 : LabelMediumRegular.size,
  },
  addressContainer: {
    width: wp("18.1%"),
  },
  addressText: {
    textAlign: "left",
    color: colors.textColor,
    fontSize: screenWidth < 1194 ? 12 : LabelMediumRegular.size,
  },
  tableContainer: {
    backgroundColor: colors.secondary,
    zIndex: -1,
  },
  tableItemContainer: {
    height: 410,
  },
  onChangePageContainer: {
    height: 32,
    gap: 16,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: colors.secondary,
  },
  previousButtonsContainer: {
    height: 32,
    backgroundColor: colors.secondary,
    flexDirection: "row",
    gap: 8,
  },
  skipBackButton: {
    width: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.bodyBackgroundColor,
    opacity: 50,
  },
  chevronLeftButton: {
    width: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.bodyBackgroundColor,
    opacity: 50,
  },
  pageNumberContainer: {
    height: 32,
    gap: 12,
    backgroundColor: colors.secondary,
    flexDirection: "row",
    alignItems: "center",
  },
  numOneButton: {
    borderRadius: 4,
    paddingVertical: 4,
    paddingHorizontal: 8,
  },
  numSGrayButton: {
    height: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    paddingVertical: 4,
    paddingHorizontal: 8,
  },
  threeDots: {
    fontSize: LabelMediumRegular.size,
    lineHeight: LabelMediumRegular.lineHeight,
    color: colors.textColor,
  },
  nextButtonsContainer: {
    height: 32,
    gap: 8,
    backgroundColor: colors.secondary,
    flexDirection: "row",
  },
  rightButtons: {
    height: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.secondary,
    paddingVertical: 4,
    paddingHorizontal: 8,
  },

  dropdown: {
    width: 320,
    position: "absolute",
    top: 30,
    right: 0,
    backgroundColor: "white",
    borderRadius: 6,
    paddingVertical: 8,
    paddingHorizontal: 4,
    ...Platform.select({
      ios: {
        shadowColor: colors.iosShadowColor,
        shadowOffset: { width: -2, height: 4 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
      android: {
        shadowColor: colors.dropdownShadowColor,
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.12,
        shadowRadius: 14,
        elevation: 3,
      },
      web: {
        boxShadow: "0px 3px 14px 0px rgba(8, 18, 26, 0.12)",
      },
    }),
  },
  optionText: {
    padding: 8,
  },
  secondDropdown: {
    width: 320,
    position: "absolute",
    top: 80,
    right: -14,
    backgroundColor: "white",
    borderRadius: 6,
    paddingVertical: 8,
    paddingHorizontal: 4,
    ...Platform.select({
      ios: {
        shadowColor: colors.iosShadowColor,
        shadowOffset: { width: -2, height: 4 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
      android: {
        shadowColor: colors.dropdownShadowColor,
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.12,
        shadowRadius: 14,
        elevation: 3,
      },
      web: {
        boxShadow: "0px 3px 14px 0px rgba(8, 18, 26, 0.12)",
      },
    }),
  },
  filter: {
    width: 320,
    position: "absolute",
    top: 45,
    backgroundColor: "white",
    borderRadius: 6,
    paddingVertical: 8,
    paddingHorizontal: 4,
    ...Platform.select({
      ios: {
        shadowColor: colors.iosShadowColor,
        shadowOffset: { width: -2, height: 4 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
      android: {
        shadowColor: colors.dropdownShadowColor,
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.12,
        shadowRadius: 14,
        elevation: 3,
      },
      web: {
        boxShadow: "0px 3px 14px 0px rgba(8, 18, 26, 0.12)",
      },
    }),
  },
  selectedOption: {
    backgroundColor: colors.selectionColor,
  },
  bodyText: {
    color: colors.textColor,
    fontSize: LabelLargeRegular.size,
    lineHeight: LabelLargeRegular.lineHeight,
  },
  deleteBodyText: {
    color: colors.danger,
    fontSize: LabelLargeRegular.size,
    lineHeight: LabelLargeRegular.lineHeight,
  },
  toastMessage: {
    backgroundColor: colors.toastBackgroundColor,
    height: 40,
    gap: 16,
    paddingHorizontal: 8,
    paddingVertical: 16,
    borderRadius: 4,
    justifyContent: "center",
    ...Platform.select({
      ios: {
        shadowColor: colors.headerFooterShadowColor,
        shadowOffset: {
          width: 0,
          height: 4,
        },
        shadowOpacity: 0.7,
        shadowRadius: 14,
      },
      android: {
        shadowColor: colors.headerFooterShadowColor,
        shadowOffset: {
          width: 0,
          height: 4,
        },
        shadowOpacity: 0.7,
        shadowRadius: 14,
      },
      web: {
        boxshadow: "0px 4px 14px 0px rgba(8, 18, 26, 0.24)",
      },
    }),
  },
  toastText: {
    color: colors.secondary,
    fontSize: LabelLargeRegular.size,
    lineHeight: LabelLargeRegular.lineHeight,
  },
  modalBackground: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    top: "90%",
    alignSelf: "center",
    position: "absolute",
  },
  statusModalBackground: {
    position: "absolute",
    alignSelf: "center",
  },
  statustoastText: {
    color: colors.secondary,
    fontSize: LabelLargeBold.size,
    lineHeight: LabelLargeBold.lineHeight,
  },
  skipBackDisable: {
    width: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.bodyBackgroundColor,
    opacity: 50,
  },
  skipBackEnable: {
    width: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.secondary,
  },
  chevronDisable: {
    width: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.bodyBackgroundColor,
    opacity: 50,
  },
  chevronEnable: {
    width: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.secondary,
    opacity: 50,
  },
  skipForwardDisable: {
    width: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.bodyBackgroundColor,
    opacity: 50,
  },
  skipForwardEnable: {
    width: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.secondary,
  },
  activeButton: {
    height: 32,
    borderRadius: 4,
    paddingVertical: 4,
    paddingHorizontal: 8,
    backgroundColor: colors.primary,
    gap: 8,
  },
  inactiveButton: {
    height: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    paddingVertical: 4,
    paddingHorizontal: 8,
    backgroundColor: colors.secondary,
  },
  activeText: {
    color: colors.secondary,
    lineHeight: ButtonSmallBold.lineHeight,
  },
  inactiveText: {
    color: colors.textColor,
    lineHeight: ButtonSmallBold.lineHeight,
  },
  chevronRightEnable: {
    height: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.secondary,
    paddingVertical: 4,
    paddingHorizontal: 8,
  },
  chevronRightDisable: {
    height: 32,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray,
    backgroundColor: colors.bodyBackgroundColor,
    paddingVertical: 4,
    paddingHorizontal: 8,
  },
  tabFlatList: {
    zIndex: -1,
  },
});

export default styles;
