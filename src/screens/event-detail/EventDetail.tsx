import React, { useEffect, useState } from "react";
import styles from "./EventDetailStyle";
import { Header } from "../../components/basics/header";
import { colors } from "../../styles/color";
import { HiraginoKakuText } from "../../components/StyledText";
import { CustomButton } from "../../components/basics/Button";
import PieChart from "react-native-pie-chart";
import {
  StatusBar,
  SafeAreaView,
  View,
  ScrollView,
  Pressable,
  TouchableWithoutFeedback,
  Platform,
  FlatList,
} from "react-native";
import {
  Entypo,
  MaterialIcons,
  MaterialCommunityIcons,
  AntDesign,
  Feather,
} from "@expo/vector-icons";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { StatusChangeDialog } from "../status-change-dialog/StatusChangeDialog";
import { EventDeleteDialog } from "../event-delete-dialog/EventDeleteDialog";
import { EventDetailParams } from "./EventDetailParams";
import { EventCreateParams } from "../event-create/EventCreateParams";
import { EventListParams } from "../event-list/EventListParams";
import {
  fetchEvent,
  fetchReception,
  fetchVenue,
  insertEventHistory,
  logicalDeleteEvent,
  updateEvent,
} from "./EventDetailService";
import { format } from "date-fns";
import { getCurrentJapanTime } from "../../environments/TimeUtils";
import { Ionicons } from "@expo/vector-icons";
import { ReceptionInfoDetailParams } from "../reception-info-detail/ReceptionInfoDetailParams";
import { EventEditParams } from "../event-edit/EventEditParams";
import { ActivityLogger } from "../../log/ActivityLogger";

type Props = {
  navigation: NavigationProp<any, any>;
};
type Params = {
  eventDetailParams: EventDetailParams;
};
interface FilterSortState {
  receptionType: string;
  member: string;
  rank: string;
  gender: string;
  age: string;
  receptionTimeAscending?: boolean;
  receptionTypeAscending?: boolean;
  rankAscending?: boolean;
  errMsg: string;
}

export const EventDetail = ({ navigation }: Props) => {
  const route = useRoute();
  const { eventDetailParams } = route.params as Params;
  const [クエリ結果_event, setEvent] = useState<{
    name: string;
    startDate: string;
    endDate: string;
    eventStatusCode: string;
  } | null>(null);
  const [クエリ結果_venue, setVenues] = useState<any[]>([]);
  const [クエリ結果_reception, setReceptions] = useState<any[]>([]);
  const [receptionResult, setReceptionResult] = useState<any[]>([]);
  const [venueName, setVenueName] = useState("");
  const [message, setMessage] = useState<string>();
  const [toastMessage, setToastMessage] = useState("");
  const [showDropdown, setShowDropdown] = useState(false);
  const [showSecondDropdown, setShowSecondDropdown] = useState(false);
  const [showToast, setShowToast] = useState(false);
  const [showStatusToast, setShowStatusToast] = useState(false);
  const [dateRange, setDateRange] = useState("");
  const [receptionTypeSeries, setReceptionTypeSeries] = useState<number[]>([]);
  const [ageSeries, setAgeSeries] = useState<number[]>([]);
  const [genderSeries, setGenderSeries] = useState<number[]>([]);
  const [showReceptionTypeFilter, setShowReceptionTypeFilter] = useState(false);
  const [showRankFilter, setShowRankFilter] = useState(false);
  const [showMemberFilter, setShowMemberFilter] = useState(false);
  const [showGenderFilter, setShowGenderFilter] = useState(false);
  const [showAgeFilter, setShowAgeFilter] = useState(false);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(true);
  const [errMsg, setErrMsg] = useState("");
  const [receptionType, setReceptionType] = useState("受付方法");
  const [member, setMember] = useState("受付対象");
  const [rank, setRank] = useState("本人確認");
  const [gender, setGender] = useState("性別");
  const [age, setAge] = useState("年代");
  const [receptionTimeAscending, setReceptionTimeAscending] = useState<boolean | undefined>(true);
  const [receptionTypeAscending, setReceptionTypeAscending] = useState<boolean | undefined>(undefined);
  const [rankAscending, setRankAscending] = useState<boolean | undefined>(undefined);
  const [selectedVenue, setSelectedVenue] = useState(0);
  const [menuItemVisible, setMenuItemVisible] = useState(false);
  const [isStatusChangeModalVisible, setIsStatusChangeModalVisible] =
    useState(false);
  const [selectedStatusName, setSelectedStatusName] = useState("");
  const [hoveredIndex, setHoveredIndex] = useState<number | null>(null);
  const widthAndHeight = 84;
  const sliceColor = [
    colors.skyBlueColor,
    colors.lightBlueColor,
    colors.paleBlueColor,
  ];
  const initialFilterSortState: FilterSortState = {
    receptionType: "受付方法",
    member: "受付対象",
    rank: "本人確認",
    gender: "性別",
    age: "年代",
    receptionTimeAscending: true,
    receptionTypeAscending: undefined,
    rankAscending: undefined,
    errMsg: "",
  };

  const initialState: { [venueId: number]: FilterSortState } = {};
  const [filterSortState, setFilterSortState] = useState<{
    [venueId: number]: FilterSortState;
  }>(initialState);

  useEffect(() => {
    ActivityLogger.insertInfoLogEntry(eventDetailParams.user, 'EventDetail', 'useEffect', 'screen open');
  }, []);

  useEffect(() => {
    setToastMessage(
      eventDetailParams.toastMsg !== ""
        ? eventDetailParams.toastMsg
        : eventDetailParams.statusToastMsg
    );
    if (eventDetailParams.toastMsg !== "") {
      showToastMessage(eventDetailParams.toastMsg, "toast");
    } else if (eventDetailParams.statusToastMsg !== "") {
      showToastMessage(eventDetailParams.statusToastMsg, "statusToast");
    }
    if (!loading) {
      setPage(0);
    }
    getEvent();
    getVenue();
    getReception();
  }, [eventDetailParams, loading]);

  const showToastMessage = (message: string, type: string) => {
    setToastMessage(message);
    if (type === "statusToast") {
      setShowStatusToast(true);
      const timer = setTimeout(() => {
        setToastMessage("");
        setShowStatusToast(false);
      }, 3000);
      return () => clearTimeout(timer);
    } else {
      setShowToast(true);
      const timer = setTimeout(() => {
        setToastMessage("");
        setShowToast(false);
      }, 3000);
      return () => clearTimeout(timer);
    }
  };

  useEffect(() => {
    if (
      クエリ結果_reception.length > 0 &&
      (selectedVenue === 0 ||
        (selectedVenue !== 0 &&
          クエリ結果_reception.filter((item) => item.venue_id === selectedVenue)
            .length > 0))
    ) {
      setReceptionResult(
        selectedVenue !== 0
          ? クエリ結果_reception.filter(
            (item) => item.venue_id === selectedVenue
          )
          : クエリ結果_reception
      );
      setErrMsg("");
      setFilterSortState((prevState) => ({
        ...prevState,
        [selectedVenue]: {
          ...prevState[selectedVenue],
          [errMsg]: "",
        },
      }));
    } else {
      setFilterSortState((prevState) => ({
        ...prevState,
        [selectedVenue]: {
          ...prevState[selectedVenue],
          [errMsg]: "受付した人はいません。",
        },
      }));
      setErrMsg("受付した人はいません。");
    }
  }, [クエリ結果_reception]);

  const getEvent = async () => {
    try {
      const result = await fetchEvent(eventDetailParams.eventId);
      if (result !== null) {
        const message = result.message;
        if (message === "success") {
          setMessage("Obtained Successfully!!");

          const formattedResult = {
            name: result.data[0].name,
            startDate: result.data[0].start_date,
            endDate: result.data[0].end_date,
            eventStatusCode: result.data[0].event_status_code,
          };
          setEvent(formattedResult);

          const formattedStartDate = formattedResult?.startDate
            ? format(new Date(formattedResult?.startDate), "yyyy/MM/dd")
            : "";
          const formattedEndDate = formattedResult?.endDate
            ? format(new Date(formattedResult?.endDate), "yyyy/MM/dd")
            : "";

          const dateRange =
            formattedResult?.startDate || formattedResult?.endDate
              ? `${formattedStartDate} ~ ${formattedEndDate}`
              : "指定なし";
          setDateRange(dateRange);
        } else {
          setMessage(message);
        }
      }
    } catch (error) {
      console.error("Error from EventDetailService:", error);
      setMessage("An error occurred");
    }
  };

  const getVenue = async () => {
    try {
      const result = await fetchVenue(eventDetailParams.eventId);
      if (result !== null) {
        const message = result.message;
        if (message === "success") {
          setMessage("Obtained Successfully!!");
          const formattedVenues = result.data.map((venue: any) => ({
            venueId: venue.venue_id,
            name: venue.name,
          }));

          const venueNames = formattedVenues
            .map((venue: any) => venue.name)
            .join("、");
          setVenueName(venueNames);
          if (formattedVenues.length > 1) {
            const newVenue = { venueId: 0, name: "全て" };
            const updatedVenues = [...formattedVenues, newVenue].sort(
              (a, b) => a.venueId - b.venueId
            );
            setVenues(updatedVenues);
          } else {
            setVenues(formattedVenues);
            setSelectedVenue(formattedVenues[0].venueId);
          }
        } else {
          setMessage(message);
        }
      }
    } catch (error) {
      console.error("Error from EventDetailService:", error);
      setMessage("An error occurred");
    }
  };

  const getReception = async () => {
    try {
      const result = await fetchReception(eventDetailParams.eventId);
      if (result !== null) {
        if (result.data) {
          setLoading(true);
        } else {
          setLoading(false);
          setErrMsg("受付した人はいません。");
        }
        const message = result.message;
        if (message === "success") {
          setMessage("Obtained Successfully!!");

          const reception = result.data.map((reception: any) => ({
            ...reception,
            age: calculateAge(
              reception.date_of_birth,
              reception.accepted_timestamp
            ),
          }));
          setReceptions(reception);
        } else {
          setMessage(message);
        }
      }
    } catch (error) {
      console.error("Error from EventDetailService:", error);
      setMessage("An error occurred");
    }
  };

  const formatUTCDateTime = (dateTime: string) => {
    const date = new Date(dateTime);
    const year = date.getUTCFullYear();
    const month = String(date.getUTCMonth() + 1).padStart(2, "0");
    const day = String(date.getUTCDate()).padStart(2, "0");
    const hours = String(date.getUTCHours()).padStart(2, "0");
    const minutes = String(date.getUTCMinutes()).padStart(2, "0");

    return `${year}/${month}/${day} ${hours}:${minutes}`;
  };

  const calculateAge = (dob: any, accepted_timestamp: any) => {
    let birthDate = new Date(dob);
    let acceptedDate = new Date(accepted_timestamp);
    let age = acceptedDate.getFullYear() - birthDate.getFullYear();
    const monthDifference = acceptedDate.getMonth() - birthDate.getMonth();
    if (
      monthDifference < 0 ||
      (monthDifference === 0 && acceptedDate.getDate() < birthDate.getDate())
    ) {
      age--;
    }
    return age;
  };

  const formatDate = (dateString: string) => {
    const date = new Date(dateString);
    const pad = (number: number) => (number < 10 ? "0" : "") + number;

    const year = date.getFullYear();
    const month = pad(date.getMonth() + 1);
    const day = pad(date.getDate());
    const hours = pad(date.getHours());
    const minutes = pad(date.getMinutes());
    const seconds = pad(date.getSeconds());

    return `${year}${month}${day}${hours}${minutes}${seconds}`;
  };

  const generateCSVContentForPieChart = () => {
    let currentDateTime = getCurrentJapanTime();
    const dotIndex = currentDateTime.indexOf(".");
    currentDateTime = currentDateTime.substring(0, dotIndex);
    let formattedDateTime = currentDateTime.replace(/[- :]/g, "");

    const csvRows = [
      `"""イベント名""","""会場名""","""集計日時"""`,
      `"""${クエリ結果_event?.name}""","""${selectedVenue === 0
        ? "全ての会場"
        : クエリ結果_venue.find((venue) => venue.venueId === selectedVenue)
          .name
      }""","""${formattedDateTime}"""`,
      ``,
      `"""合計人数"""`,
      `"""${クエリ結果_reception.filter(
        (item) =>
          item.venue_id ===
          (selectedVenue === 0 ? item.venue_id : selectedVenue)).length}\"""`,
      ``,
      `"""受付方法"""`,
      `"""アプリ""","""手入力"""`,
      `"""${クエリ結果_reception.filter(
        (item) =>
          item.venue_id ===
          (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
          item.reception_type_code == "1")
        .length
      }""","""${クエリ結果_reception.filter(
        (item) =>
          item.venue_id ===
          (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
          item.reception_type_code == "2")
        .length
      }"""`,
      ``,
      `"""年齢区分"""`,
      `"""成年""","""未成年"""`,
      `"""${クエリ結果_reception.filter(
        (item) =>
          item.venue_id ===
          (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
          item.age > 18).length}""","""${クエリ結果_reception.filter(
            (item) =>
              item.venue_id ===
              (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
              item.age < 18).length
      }"""`,
      ``,
      `"""性別"""`,
      `"""男性""","""女性""","""未入力"""`,
      `"""${クエリ結果_reception.filter(
        (item) =>
          item.venue_id ===
          (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
          item.gender_code == "1").length
      }""","""${クエリ結果_reception.filter(
        (item) =>
          item.venue_id ===
          (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
          item.gender_code == "2").length
      }""","""${クエリ結果_reception.filter(
        (item) =>
          item.venue_id ===
          (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
          item.gender_code == "0").length
      }"""`,
    ];

    return csvRows.join("\r\n");
  };

  const generateCSVContentForReception = () => {
    let currentDateTime = getCurrentJapanTime();
    const dotIndex = currentDateTime.indexOf(".");
    currentDateTime = currentDateTime.substring(0, dotIndex);
    let formattedDateTime = currentDateTime.replace(/[- :]/g, "");

    const csvRows = [];

    const eventHeaders = [`"""イベント名"""`, `"""会場名"""`, `"""集計日時"""`];
    csvRows.push(eventHeaders.join(','));

    const eventData = [`"""${クエリ結果_event?.name}"""`,
    `"""${selectedVenue === 0 ? "全ての会場" : クエリ結果_venue.find((venue) => venue.venueId === selectedVenue).name}"""`,
    `"""${String(formattedDateTime)}"""`];
    csvRows.push(eventData.join(','));
    csvRows.push([]);

    const headers = [`"""イベントID"""`, `"""イベント名"""`, `"""会場ID"""`, `"""会場名"""`, `"""受付ID"""`, 
      `"""メンバーID"""`, `"""LGaP_ID"""`, `"""ユーザーランク"""`, `"""本人確認"""`, `"""受付対象"""`, 
      `"""続柄"""`, `"""姓"""`, `"""名"""`, `"""カナ姓"""`, `"""カナ名"""`, 
      `"""生年月日"""`, `"""受付時点の年齢"""`, `"""受付時点の年代"""`, `"""性別コード"""`, `"""性別"""`, 
      `"""郵便番号"""`, `"""住所"""`, `"""受付方法コード"""`, `"""受付方法"""`, `"""家族の並び順"""`, 
      `"""受付端末ID"""`, `"""受付日時"""`, `"""更新ユーザーID"""`, `"""更新日時"""`];
    csvRows.push(headers.join(','));

    receptionResult.forEach((result) => {
      let ageRange = "";
      if (result.age <= 9) {
        ageRange = "10歳未満";
      } else if (result.age > 9 && result.age <= 19) {
        ageRange = "10代";
      } else if (result.age > 19 && result.age <= 29) {
        ageRange = "20代";
      } else if (result.age > 29 && result.age <= 39) {
        ageRange = "30代";
      } else if (result.age > 39 && result.age <= 49) {
        ageRange = "40代";
      } else if (result.age > 49 && result.age <= 59) {
        ageRange = "50代";
      } else if (result.age > 59 && result.age <= 69) {
        ageRange = "60代";
      } else if (result.age > 69 && result.age <= 79) {
        ageRange = "70代";
      } else if (result.age >= 80) {
        ageRange = "80歳以上";
      }

      const csvRow: any = [];
      csvRow.push(`"""${eventDetailParams.eventId}"""`);
      csvRow.push(`"""${クエリ結果_event?.name}"""`);
      csvRow.push(`"""${selectedVenue == 0 ? null : result.venue_id}"""`);
      csvRow.push(`"""${selectedVenue == 0 ? "全ての会場" : クエリ結果_venue.find((venue) => venue.venueId === selectedVenue).name}"""`);
      csvRow.push(`"""${result.reception_id}"""`);
      csvRow.push(`"""${result.member_id}"""`);
      csvRow.push(`"""${result.lgap_id}"""`);
      csvRow.push(`"""${result.user_rank}"""`);
      csvRow.push(`"""${result.user_rank === 4 ? "確認済み" : (result.lgap_id !== null && result.lgap_id !== "") ? "未確認" : "-"}"""`);
      csvRow.push(`"""${result.member_id === 0 ? (result.member_size_per_group === "1" ? "本人のみ" : "受付代表") : "一緒に受付"}"""`);
      csvRow.push(`"""${result.relationship}"`);
      csvRow.push(`"""${result.lastname}"""`);
      csvRow.push(`"""${result.firstname}"""`);
      csvRow.push(`"""${result.lastname_kana}"""`);
      csvRow.push(`"""${result.firstname_kana}"""`);
      csvRow.push(`"""${result.date_of_birth.substring(0, result.date_of_birth.indexOf("T"))}"""`);
      csvRow.push(`"""${result.age}"""`);
      csvRow.push(`"""${ageRange}"""`);
      csvRow.push(`"""${result.gender_code}"""`);
      csvRow.push(`"""${result.gender_name}"""`);
      csvRow.push(`"""${result.postal_code}"""`);
      csvRow.push(`"""${result.address}"""`);
      csvRow.push(`"""${result.reception_type_code}"""`);
      csvRow.push(`"""${result.short_name}"""`);
      csvRow.push(`"""${result.family_order_number}"""`);
      csvRow.push(`"""${result.accepted_terminal_id}"""`);
      csvRow.push(`"""${result.accepted_timestamp.toString().replace("T", " ").slice(0, -1) + "0"}"""`);
      csvRow.push(`"""${result.modifier_id}"""`);
      csvRow.push(`"""${result.modification_timestamp.toString().replace("T", " ").slice(0, -1) + "0"}"""`);
      csvRows.push(csvRow.join(','));
    });

    return csvRows.join("\r\n");
  };

  const downloadCSVFile = (csvContent: any) => {
    const bom = "\uFEFF";
    const blob = new Blob([bom + csvContent], {
      type: "text/csv;charset=utf-8;",
    });
    const currentDateTime = getCurrentJapanTime();
    const formattedDateTime = formatDate(currentDateTime);

    const fileName =
      クエリ結果_event?.name +
      "_" +
      (selectedVenue === 0
        ? "全ての会場"
        : クエリ結果_venue.find((venue) => venue.venueId === selectedVenue)
          .name) +
      "_" +
      formattedDateTime +
      ".csv";
    const url = URL.createObjectURL(blob);
    const link = document.createElement("a");
    link.setAttribute("href", url);
    link.setAttribute("download", fileName);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);

    return fileName;
  };

  const handleExportToCSV = () => {
    filterClose();
    const csvContent = generateCSVContentForPieChart();
    const downloadedFileName = downloadCSVFile(csvContent);
    ActivityLogger.insertInfoLogEntry(eventDetailParams.user, 'EventDetail', 'handleExportToCSV', 'action', '', null, '', 'Donwload CSV file(chart data):' + downloadedFileName);
  };

  const handleExportReceptionToCSV = () => {
    const csvContent = generateCSVContentForReception();
    const downloadedFileName = downloadCSVFile(csvContent);
    ActivityLogger.insertInfoLogEntry(eventDetailParams.user, 'EventDetail', 'handleExportReceptionToCSV', 'action', '', null, '', 'Donwload CSV file(entrant data):' + downloadedFileName);
  };

  const getSeries = (reception: any) => {
    if (reception) {
      const firstSeries = [
        クエリ結果_reception.filter(
          (item) =>
            item.venue_id ===
            (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
            item.reception_type_code == "1"
        ).length,
        クエリ結果_reception.filter(
          (item) =>
            item.venue_id ===
            (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
            item.reception_type_code == "2"
        ).length,
        0,
      ];
      const secondSeries = [
        クエリ結果_reception.filter(
          (item) =>
            item.venue_id ===
            (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
            item.age > 18
        ).length,
        クエリ結果_reception.filter(
          (item) =>
            item.venue_id ===
            (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
            item.age < 18
        ).length,
        0,
      ];
      const thirdSeries = [
        クエリ結果_reception.filter(
          (item) =>
            item.venue_id ===
            (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
            item.gender_code == "1"
        ).length,
        クエリ結果_reception.filter(
          (item) =>
            item.venue_id ===
            (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
            item.gender_code == "2"
        ).length,
        クエリ結果_reception.filter(
          (item) =>
            item.venue_id ===
            (selectedVenue === 0 ? item.venue_id : selectedVenue) &&
            item.gender_code == "0"
        ).length,
      ];
      const normalizeSeries = (series: any[], field: string) => {
        if (field === "receptionType" || field === "age") {
          return series.every(value => value === 0) ? [1, 1, 0] : series;
        }
        else {
          return series.every(value => value === 0) ? [1, 1, 1] : series;
        }
      };
      setReceptionTypeSeries(normalizeSeries(firstSeries, 'receptionType'));
      setAgeSeries(normalizeSeries(secondSeries, 'age'));
      setGenderSeries(normalizeSeries(thirdSeries, 'gender'));
    }
  };

  useEffect(() => {
    getSeries(receptionResult);
  }, [selectedVenue, receptionResult]);

  useEffect(() => { }, [receptionTypeSeries, ageSeries, genderSeries]);

  const renderPieChart = () => {
    const typeSum = receptionTypeSeries.reduce((a, b) => a + b, 0);
    const ageSum = ageSeries.reduce((a, b) => a + b, 0);
    const genderSum = genderSeries.reduce((a, b) => a + b, 0);
    if (typeSum === 0 || ageSum === 0 || genderSum === 0) {
      return;
    }
    return (
      <View style={styles.ellipseContainer}>
        <View style={styles.pieChartContainer}>
          <PieChart
            widthAndHeight={widthAndHeight}
            series={receptionTypeSeries}
            sliceColor={sliceColor}
            coverRadius={0.45}
            coverFill={"#FFF"}
          />
          <View style={styles.userCountsContainer}>
            <View style={styles.firstCountContainer}>
              <View style={styles.firstCountRectangle} />
              <HiraginoKakuText style={styles.firstCountText}>
                アプリ：
                <HiraginoKakuText style={{ color: colors.primary }}>
                  {
                    クエリ結果_reception.filter(
                      (item) =>
                        item.venue_id ===
                        (selectedVenue === 0
                          ? item.venue_id
                          : selectedVenue) && item.reception_type_code == "1"
                    ).length
                  }
                </HiraginoKakuText>
                人
              </HiraginoKakuText>
            </View>
            <View style={styles.secondCountContainer}>
              <View style={styles.secondCountRectangle} />
              <HiraginoKakuText style={styles.secondCountText}>
                手入力：
                <HiraginoKakuText style={{ color: colors.primary }}>
                  {
                    クエリ結果_reception.filter(
                      (item) =>
                        item.venue_id ===
                        (selectedVenue === 0
                          ? item.venue_id
                          : selectedVenue) && item.reception_type_code == "2"
                    ).length
                  }
                </HiraginoKakuText>
                人
              </HiraginoKakuText>
            </View>
          </View>
        </View>
        <View style={styles.verticalSeparator} />
        <View style={styles.pieChartContainer}>
          <PieChart
            widthAndHeight={widthAndHeight}
            series={ageSeries}
            sliceColor={sliceColor}
            coverRadius={0.45}
            coverFill={"#FFF"}
          />
          <View style={styles.userCountsContainer}>
            <View style={styles.firstCountContainer}>
              <View style={styles.firstCountRectangle} />
              <HiraginoKakuText style={styles.firstCountText}>
                成年：
                <HiraginoKakuText style={{ color: colors.primary }}>
                  {
                    クエリ結果_reception.filter(
                      (item) =>
                        item.venue_id ===
                        (selectedVenue === 0
                          ? item.venue_id
                          : selectedVenue) && item.age > 18
                    ).length
                  }
                </HiraginoKakuText>
                人
              </HiraginoKakuText>
            </View>
            <View style={styles.secondCountContainer}>
              <View style={styles.secondCountRectangle} />
              <HiraginoKakuText style={styles.secondCountText}>
                未成年：
                <HiraginoKakuText style={{ color: colors.primary }}>
                  {
                    クエリ結果_reception.filter(
                      (item) =>
                        item.venue_id ===
                        (selectedVenue === 0
                          ? item.venue_id
                          : selectedVenue) && item.age < 18
                    ).length
                  }
                </HiraginoKakuText>
                人
              </HiraginoKakuText>
            </View>
          </View>
        </View>
        <View style={styles.verticalSeparator} />
        <View style={styles.pieChartContainer}>
          <PieChart
            widthAndHeight={widthAndHeight}
            series={genderSeries}
            sliceColor={sliceColor}
            coverRadius={0.45}
            coverFill={"#FFF"}
          />
          <View style={styles.userCountsContainer}>
            <View style={styles.firstCountContainer}>
              <View style={styles.firstCountRectangle} />
              <HiraginoKakuText style={styles.firstCountText}>
                男性：
                <HiraginoKakuText style={{ color: colors.primary }}>
                  {
                    クエリ結果_reception.filter(
                      (item) =>
                        item.venue_id ===
                        (selectedVenue === 0
                          ? item.venue_id
                          : selectedVenue) && item.gender_code == "1"
                    ).length
                  }
                </HiraginoKakuText>
                人
              </HiraginoKakuText>
            </View>
            <View style={styles.secondCountContainer}>
              <View style={styles.secondCountRectangle} />
              <HiraginoKakuText style={styles.secondCountText}>
                女性：
                <HiraginoKakuText style={{ color: colors.primary }}>
                  {
                    クエリ結果_reception.filter(
                      (item) =>
                        item.venue_id ===
                        (selectedVenue === 0
                          ? item.venue_id
                          : selectedVenue) && item.gender_code == "2"
                    ).length
                  }
                </HiraginoKakuText>
                人
              </HiraginoKakuText>
            </View>
            <View style={styles.secondCountContainer}>
              <View style={styles.thirdCountRectangle} />
              <HiraginoKakuText style={styles.secondCountText}>
                未入力：
                <HiraginoKakuText style={{ color: colors.primary }}>
                  {
                    クエリ結果_reception.filter(
                      (item) =>
                        item.venue_id ===
                        (selectedVenue === 0
                          ? item.venue_id
                          : selectedVenue) && item.gender_code == "0"
                    ).length
                  }
                </HiraginoKakuText>
                人
              </HiraginoKakuText>
            </View>
          </View>
        </View>
      </View>
    );
  };

  const handleFilter = (filter: string) => {
    filterClose();
    switch (filter) {
      case "receptionType":
        setShowReceptionTypeFilter(!showReceptionTypeFilter);
        break;
      case "member":
        setShowMemberFilter(!showMemberFilter);
        break;
      case "rank":
        setShowRankFilter(!showRankFilter);
        break;
      case "gender":
        setShowGenderFilter(!showGenderFilter);
        break;
      case "age":
        setShowAgeFilter(!showAgeFilter);
        break;
    }
    setShowDropdown(false);
    setShowSecondDropdown(false);
  };

  const filterPress = (filter: string, option: string) => {
    let value = "";
    switch (filter) {
      case "receptionType":
        if (option == "1") {
          setReceptionType("アプリ（自己QR）");
          value = "アプリ（自己QR）";
        } else if (option == "2") {
          setReceptionType("手入力");
          value = "手入力";
        } else {
          setReceptionType("受付方法");
          value = "受付方法";
        }
        setShowReceptionTypeFilter(!showReceptionTypeFilter);
        break;
      case "member":
        if (option == "1") {
          setMember("本人のみ受付");
          value = "本人のみ受付";
        } else if (option == "2") {
          setMember("代表して受付");
          value = "代表して受付";
        } else if (option == "3") {
          setMember("一緒に受付");
          value = "一緒に受付";
        } else {
          setMember("受付対象");
          value = "受付対象";
        }
        setShowMemberFilter(!showMemberFilter);
        break;
      case "rank":
        if (option == "1") {
          setRank("本人確認済み");
          value = "本人確認済み";
        } else if (option == "2") {
          setRank("本人未確認");
          value = "本人未確認";
        } else {
          setRank("本人確認");
          value = "本人確認";
        }
        setShowRankFilter(!showRankFilter);
        break;
      case "gender":
        if (option == "1") {
          setGender("男性");
          value = "男性";
        } else if (option == "2") {
          setGender("女性");
          value = "女性";
        } else if (option == "0") {
          setGender("未入力");
          value = "未入力";
        } else {
          setGender("性別");
          value = "性別";
        }
        setShowGenderFilter(!showGenderFilter);
        break;
      case "age":
        if (option == "1") {
          setAge("10歳未満");
          value = "10歳未満";
        } else if (option == "2") {
          setAge("10代");
          value = "10代";
        } else if (option == "3") {
          setAge("20代");
          value = "20代";
        } else if (option == "4") {
          setAge("30代");
          value = "30代";
        } else if (option == "5") {
          setAge("40代");
          value = "40代’";
        } else if (option == "6") {
          setAge("50代");
          value = "50代";
        } else if (option == "7") {
          setAge("60代");
          value = "60代";
        } else if (option == "8") {
          setAge("70代");
          value = "70代";
        } else if (option == "9") {
          setAge("80歳以上");
          value = "80歳以上";
        } else {
          setAge("年代");
          value = "年代";
        }
        setShowAgeFilter(!showAgeFilter);
        break;
    }
    setFilterSortState((prevState) => ({
      ...prevState,
      [selectedVenue]: {
        ...prevState[selectedVenue],
        [filter]: value,
      },
    }));

    setShowDropdown(false);
    setShowSecondDropdown(false);
  };

  const filterData = (
    receptionType: string,
    member: string,
    rank: string,
    gender: string,
    age: string
  ) => {
    return クエリ結果_reception.filter((item) => {
      let receptionTypeFilter = false;
      switch (receptionType) {
        case "受付方法":
          receptionTypeFilter = true;
          break;
        case "アプリ（自己QR）":
          receptionTypeFilter = item.short_name === "アプリ";
          break;
        case "手入力":
          receptionTypeFilter = item.short_name === "手入力";
          break;
        default:
          receptionTypeFilter = false;
      }

      let memberFilter = false;
      switch (member) {
        case "受付対象":
          memberFilter = true;
          break;
        case "本人のみ受付":
          memberFilter =
            item.member_id === 0 && item.member_size_per_group === "1";
          break;
        case "代表して受付":
          memberFilter =
            item.member_id === 0 && item.member_size_per_group !== "1";
          break;
        case "一緒に受付":
          memberFilter = item.member_id !== 0;
          break;
        default:
          memberFilter = false;
      }

      let rankFilter = false;
      switch (rank) {
        case "本人確認":
          rankFilter = true;
          break;
        case "本人確認済み":
          rankFilter = item.user_rank === "4";
          break;
        case "本人未確認":
          rankFilter = item.user_rank !== "4";
          break;
        default:
          rankFilter = false;
      }

      let genderFilter = false;
      switch (gender) {
        case "性別":
          genderFilter = true;
          break;
        case "男性":
          genderFilter = item.gender_code === "1";
          break;
        case "女性":
          genderFilter = item.gender_code === "2";
          break;
        case "未入力":
          genderFilter = item.gender_code === "0";
          break;
        default:
          genderFilter = false;
      }

      let ageFilter = false;
      switch (age) {
        case "年代":
          ageFilter = true;
          break;
        case "10歳未満":
          ageFilter = item.age <= 9;
          break;
        case "10代":
          ageFilter = item.age > 9 && item.age <= 19;
          break;
        case "20代":
          ageFilter = item.age > 19 && item.age <= 29;
          break;
        case "30代":
          ageFilter = item.age > 29 && item.age <= 39;
          break;
        case "40代":
          ageFilter = item.age > 39 && item.age <= 49;
          break;
        case "50代":
          ageFilter = item.age > 49 && item.age <= 59;
          break;
        case "60代":
          ageFilter = item.age > 59 && item.age <= 69;
          break;
        case "70代":
          ageFilter = item.age > 69 && item.age <= 79;
          break;
        case "80歳以上":
          ageFilter = item.age >= 80;
          break;
        default:
          ageFilter = false;
      }
      return (
        (selectedVenue === 0 ||
          (selectedVenue !== 0 && item.venue_id === selectedVenue)) &&
        receptionTypeFilter &&
        memberFilter &&
        rankFilter &&
        genderFilter &&
        ageFilter
      );
    });
  };
  const filterClose = () => {
    setShowReceptionTypeFilter(false);
    setShowMemberFilter(false);
    setShowRankFilter(false);
    setShowGenderFilter(false);
    setShowAgeFilter(false);
  };

  const handleBack = () => {
    const eventListParams = new EventListParams();
    eventListParams.user = eventDetailParams.user;
    navigation.navigate("EventList", {
      eventListParams,
    });
    ActivityLogger.insertInfoLogEntry(eventDetailParams.user, 'EventDetail', 'handleBack', 'transition', 'EventList', eventListParams);
  };

  const handleStatusChangeEvent = (status: string) => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    filterClose();
    setSelectedStatusName(status);
    setIsStatusChangeModalVisible(true);
  };
  const handleCancelButton = () => {
    setIsStatusChangeModalVisible(!setIsStatusChangeModalVisible);
    navigation.navigate("EventDetail", {
      eventDetailParams,
    });
    ActivityLogger.insertInfoLogEntry(eventDetailParams.user, 'EventDetail', 'handleStatusChangeEvent', 'transition', 'EventDetail', eventDetailParams);
  };
  const handleEventEdit = () => {
    filterClose();
    const eventEditParams = new EventEditParams();
    eventEditParams.eventId = eventDetailParams.eventId;
    eventEditParams.user = eventDetailParams.user;
    eventEditParams.editPgType = "EventDetail";
    navigation.navigate("EventEdit", {
      eventEditParams,
    });
    ActivityLogger.insertInfoLogEntry(eventDetailParams.user, 'EventDetail', 'handleEventEdit', 'transition', 'EventDetail', eventDetailParams);
  };
  const handleReceptionInfoDetail = (member: any) => {
    filterClose();
    if (showDropdown) {
      setShowDropdown(false);
      setShowSecondDropdown(false);
    } else {
      setShowDropdown(false);
      setShowSecondDropdown(false);
      const receptionInfoDetailParams = new ReceptionInfoDetailParams();
      receptionInfoDetailParams.user = eventDetailParams.user;
      receptionInfoDetailParams.eventId = eventDetailParams.eventId;
      receptionInfoDetailParams.receptionId = member.reception_id;
      receptionInfoDetailParams.memberId = member.member_id;
      navigation.navigate("ReceptionInfoDetail", {
        receptionInfoDetailParams,
      });
      ActivityLogger.insertInfoLogEntry(eventDetailParams.user, 'EventDetail', 'handleReceptionInfoDetail', 'transition', 'ReceptionInfoDetail', receptionInfoDetailParams);
    }
  };

  const handleReproductionEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    filterClose();
    const eventCreateParams = new EventCreateParams();
    eventCreateParams.user = eventDetailParams.user;
    navigation.navigate("EventCreate", {
      eventCreateParams,
    });
    ActivityLogger.insertInfoLogEntry(eventDetailParams.user, 'EventDetail', 'handleReproductionEvent', 'transition', 'EventCreate', eventCreateParams);
  };

  //delete dialog
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
  const handleDeleteEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    filterClose();
    setIsDeleteModalVisible(true);
  };
  const handleDeleteCancelButton = () => {
    setIsDeleteModalVisible(false);
    navigation.navigate("EventDetail", {
      eventDetailParams,
    });
    ActivityLogger.insertInfoLogEntry(eventDetailParams.user, 'EventDetail', 'handleDeleteCancelButton', 'transition', 'EventDetail', eventDetailParams);
  };
  const handleDeleteButton = () => {
    setIsDeleteModalVisible(false);
    deleteEvent();
  };

  const handleDropDown = () => {
    setShowDropdown(!showDropdown);
    setShowSecondDropdown(false);
    filterClose();
  };

  // Close dropdown when click outside
  const handleCloseDropdown = () => {
    filterClose();
    if ((showDropdown && showSecondDropdown) || showDropdown) {
      setShowDropdown(false);
      setShowSecondDropdown(false);
    }
    setShowReceptionTypeFilter(false);
    setShowMemberFilter(false);
    setShowRankFilter(false);
    setShowGenderFilter(false);
    setShowAgeFilter(false);
  };
  const handleUpdateStatusCode = async () => {
    try {
      const result = await insertEventHistory(eventDetailParams.eventId);

      const message = result.message;
      if (message === "success") {
        const updEventResult = await updateEvent(
          eventDetailParams.eventId,
          selectedStatusName,
          eventDetailParams.user.userId
        );
        let eventId = eventDetailParams.eventId;
        const updmessage = await updEventResult.message;
        if (updmessage === "success") {
          setMessage("Inserted Successfully!!");
          setIsStatusChangeModalVisible(false);
          var getEventName = クエリ結果_event?.name;
          const eventDetailParams = new EventDetailParams();
          eventDetailParams.eventId = eventId;
          eventDetailParams.user = eventDetailParams.user;
          eventDetailParams.statusToastMsg = `＜${getEventName}＞を${selectedStatusName}に変更しました`; //受付可能
          navigation.navigate("EventDetail", { eventDetailParams });
          ActivityLogger.insertInfoLogEntry(eventDetailParams.user, 'EventDetail', 'handleUpdateStatusCode', 'transition', 'EventDetail', eventDetailParams);
        } else {
          setMessage(updmessage);
        }
      } else {
        setMessage(message);
      }
    } catch (error) {
      console.error("Error from EventDetailService:", error);
    }
  };

  //delete event
  const deleteEvent = async () => {
    try {
      const deleteResult = await insertEventHistory(eventDetailParams.eventId);

      const message = deleteResult.message;

      if (message === "success") {
        const updEventResult = await logicalDeleteEvent(
          eventDetailParams.eventId,
          eventDetailParams.user.userId
        );
        const updmessage = await updEventResult.message;
        if (updmessage === "success") {
          setMessage("deleted Successfully!!");
          const eventListParams = new EventListParams();
          eventListParams.user = eventDetailParams.user;
          eventListParams.toastMsg = `イベントを削除しました`;

          navigation.navigate("EventList", { eventListParams });
          ActivityLogger.insertInfoLogEntry(eventDetailParams.user, 'EventDetail', 'deleteEvent', 'transition', 'EventList', eventListParams);
        } else {
          setMessage(updmessage);
        }
      } else {
        setMessage(message);
      }
    } catch (error) {
      console.error("Error from EventDetailService:", error);
    }
  };

  const iconSize = Platform.OS === "web" ? 19 : 20;

  useEffect(() => {
    let sortedReception = sortData();
    setReceptionResult(sortedReception);
  }, [receptionTimeAscending, receptionTypeAscending, rankAscending,
    filterSortState[selectedVenue]]);

  const sortReception = (field: string) => {
    let value = false;
    let undefinedSortField1 = "";
    let undefinedSortField2 = "";
    if (field === "receptionTime") {
      if (receptionTimeAscending === undefined) {
        value = true;
        setReceptionTimeAscending(value);
      } else {
        value = !receptionTimeAscending;
        setReceptionTimeAscending(value);
      }
      setReceptionTypeAscending(undefined);
      setRankAscending(undefined);
      undefinedSortField1 = "receptionTypeAscending";
      undefinedSortField2 = "rankAscending";

    } else if (field === "receptionType") {
      if (receptionTypeAscending === undefined) {
        value = true;
        setReceptionTypeAscending(value);
      } else {
        value = !receptionTypeAscending;
        setReceptionTypeAscending(value);
      }
      setReceptionTimeAscending(undefined);
      setRankAscending(undefined);
      undefinedSortField1 = "receptionTimeAscending";
      undefinedSortField2 = "rankAscending";

    } else if (field === "rank") {
      if (rankAscending === undefined) {
        value = true;
        setRankAscending(value);
      } else {
        value = !rankAscending;
        setRankAscending(value);
      }
      setReceptionTimeAscending(undefined);
      setReceptionTypeAscending(undefined);
      undefinedSortField1 = "receptionTimeAscending";
      undefinedSortField2 = "receptionTypeAscending";

    }
    setFilterSortState((prevState) => ({
      ...prevState,
      [selectedVenue]: {
        ...prevState[selectedVenue],
        [field === "receptionTime"
          ? "receptionTimeAscending"
          : (field === "receptionType"
            ? "receptionTypeAscending"
            : "rankAscending")]: value,
        [undefinedSortField1]: undefined,
        [undefinedSortField2]: undefined,
      },
    }));
  };

  const sortData = () => {

    const sortedData = [...receptionResult].sort((a, b) => {
      let timeComparison: any = 0;

      if (filterSortState[selectedVenue] && filterSortState[selectedVenue].receptionTimeAscending !== undefined) {
        timeComparison = b.accepted_timestamp.localeCompare(
          a.accepted_timestamp
        );
        if (!filterSortState[selectedVenue].receptionTimeAscending) {
          timeComparison *= -1;
        }
      }

      else if (receptionTimeAscending !== undefined) {
        timeComparison = b.accepted_timestamp.localeCompare(
          a.accepted_timestamp
        );
        if (!receptionTimeAscending) {
          timeComparison *= -1;
        }
      }
      if (timeComparison !== 0) return timeComparison;

      let typeComparison: any = 0;
      if (filterSortState[selectedVenue] && filterSortState[selectedVenue].receptionTypeAscending !== undefined) {
        typeComparison = (a.short_name ?? "").localeCompare(
          b.short_name ?? ""
        );
        if (!filterSortState[selectedVenue].receptionTypeAscending) {
          typeComparison *= -1;
        }
      }
      else if (receptionTypeAscending !== undefined) {
        typeComparison = (a.short_name ?? "").localeCompare(
          b.short_name ?? ""
        );
        if (!receptionTypeAscending) {
          typeComparison *= -1;
        }
      }
      if (typeComparison !== 0) return typeComparison;

      let rankComparison: any = 0;
      if (filterSortState[selectedVenue] && filterSortState[selectedVenue].rankAscending !== undefined) {
        rankComparison = (b.user_rank ?? "0")
          .toString()
          .localeCompare(a.user_rank ?? "0".toString());
        if (!filterSortState[selectedVenue].rankAscending) {
          rankComparison *= -1;
        }
      }
      else if (rankAscending !== undefined) {
        rankComparison = (b.user_rank ?? "0")
          .toString()
          .localeCompare(a.user_rank ?? "0".toString());

        if (!rankAscending) {
          rankComparison *= -1;
        }
      }
      if (rankComparison !== 0) return rankComparison;
    });
    return sortedData;
  };

  const renderHeader = () => (
    <View style={styles.header}>
      <Pressable
        style={styles.receptionDateTimeHeader}
        onPress={() => sortReception("receptionTime")}
      >
        <HiraginoKakuText style={styles.headerText}>受付日時</HiraginoKakuText>
        {receptionTimeAscending === undefined ? (
          <MaterialCommunityIcons
            name="swap-vertical"
            size={iconSize}
            color={colors.greyTextColor}
          />
        ) : receptionTimeAscending ? (
          <MaterialIcons
            name="arrow-upward"
            size={24}
            color={colors.greyTextColor}
          />
        ) : (
          <MaterialIcons
            name="arrow-downward"
            size={24}
            color={colors.greyTextColor}
          />
        )}
      </Pressable>
      <Pressable
        style={styles.receptionMethodHeader}
        onPress={() => sortReception("receptionType")}
      >
        <HiraginoKakuText style={styles.headerText}>受付方法</HiraginoKakuText>
        {receptionTypeAscending === undefined ? (
          <MaterialCommunityIcons
            name="swap-vertical"
            size={iconSize}
            color={colors.greyTextColor}
          />
        ) : receptionTypeAscending ? (
          <MaterialIcons
            name="arrow-upward"
            size={24}
            color={colors.greyTextColor}
          />
        ) : (
          <MaterialIcons
            name="arrow-downward"
            size={24}
            color={colors.greyTextColor}
          />
        )}
      </Pressable>
      <Pressable
        style={styles.identificationHeader}
        onPress={() => sortReception("rank")}
      >
        <HiraginoKakuText style={styles.headerText}>本人確認</HiraginoKakuText>
        {rankAscending === undefined ? (
          <MaterialCommunityIcons
            name="swap-vertical"
            size={iconSize}
            color={colors.greyTextColor}
          />
        ) : rankAscending ? (
          <MaterialIcons
            name="arrow-upward"
            size={24}
            color={colors.greyTextColor}
          />
        ) : (
          <MaterialIcons
            name="arrow-downward"
            size={24}
            color={colors.greyTextColor}
          />
        )}
      </Pressable>
      <View style={[styles.receptionTargetHeader]}>
        <HiraginoKakuText style={[styles.headerText]}>
          受付対象
        </HiraginoKakuText>
      </View>
      <View style={styles.acceptedPersonHeader}>
        <HiraginoKakuText style={[styles.headerText]}>
          受付した人
        </HiraginoKakuText>
      </View>
      <View style={styles.genderHeader}>
        <HiraginoKakuText style={[styles.headerText]}>性別</HiraginoKakuText>
      </View>
      <View style={styles.dateOfBirthHeader}>
        <HiraginoKakuText style={[styles.headerText]}>
          生年月日
        </HiraginoKakuText>
      </View>
      <View style={styles.addressHeader}>
        <HiraginoKakuText style={[styles.headerText]}>住所</HiraginoKakuText>
      </View>
    </View>
  );
  useEffect(() => {
    if (
      (selectedVenue !== 0 &&
        !クエリ結果_reception.some((item) => item.venue_id == selectedVenue)) ||
      (selectedVenue == 0 && クエリ結果_reception.length == 0)
    ) {
      const noDataError = "受付した人はいません。";
      setErrMsg(noDataError);
      setFilterSortState((prevState) => ({
        ...prevState,
        [selectedVenue]: {
          ...prevState[selectedVenue],
          errMsg: noDataError,
        },
      }));
      setReceptionResult([]);
    } else {
      let filteredReception = filterData(
        receptionType,
        member,
        rank,
        gender,
        age
      );
      setReceptionResult(filteredReception);
      if (filteredReception.length > 0) {
        setPage(1);
        setErrMsg("");
        setFilterSortState((prevState) => ({
          ...prevState,
          [selectedVenue]: {
            ...prevState[selectedVenue],
            errMsg: "",
          },
        }));
      } else {
        const noMatchError =
          "指定された条件に当てはまるデータがありません。\n 別の条件を指定して再度検索してください。";
        setErrMsg(noMatchError);
        setFilterSortState((prevState) => ({
          ...prevState,
          [selectedVenue]: {
            ...prevState[selectedVenue],
            errMsg: noMatchError,
          },
        }));
      }
    }
  }, [receptionType, member, rank, gender, age, selectedVenue]);

  const handleTabChanged = (venueId: number) => {
    setSelectedVenue(venueId);

    if (venueId !== 0) {
      setReceptionResult(
        クエリ結果_reception.filter((item) => item.venue_id == venueId)
      );
    } else {
      setReceptionResult(クエリ結果_reception);
    }
    setPage(1);
    setReceptionType(
      !filterSortState[venueId] || !filterSortState[venueId].receptionType
        ? initialFilterSortState.receptionType
        : filterSortState[venueId].receptionType
    );
    setMember(
      !filterSortState[venueId] || !filterSortState[venueId].member
        ? initialFilterSortState.member
        : filterSortState[venueId].member
    );
    setRank(
      !filterSortState[venueId] || !filterSortState[venueId].rank
        ? initialFilterSortState.rank
        : filterSortState[venueId].rank
    );
    setGender(
      !filterSortState[venueId] || !filterSortState[venueId].gender
        ? initialFilterSortState.gender
        : filterSortState[venueId].gender
    );
    setAge(
      !filterSortState[venueId] || !filterSortState[venueId].age
        ? initialFilterSortState.age
        : filterSortState[venueId].age
    );
    setReceptionTimeAscending(
      filterSortState[venueId] && (filterSortState[venueId].receptionTypeAscending !== undefined || filterSortState[venueId].rankAscending !== undefined) ?
        filterSortState[venueId].receptionTimeAscending :
        (!filterSortState[venueId] ||
          !filterSortState[venueId].receptionTimeAscending
          ? initialFilterSortState.receptionTimeAscending
          : filterSortState[venueId].receptionTimeAscending)
    );
    setReceptionTypeAscending(
      !filterSortState[venueId]
        ? initialFilterSortState.receptionTypeAscending
        : filterSortState[venueId].receptionTypeAscending
    );
    setRankAscending(
      !filterSortState[venueId]
        ? initialFilterSortState.rankAscending
        : filterSortState[venueId].rankAscending
    );
  };
  useEffect(() => {
    if (
      filterSortState[selectedVenue] &&
      filterSortState[selectedVenue].errMsg
    ) {
      setErrMsg(filterSortState[selectedVenue].errMsg);
    } else {
      setErrMsg("");
    }
  }, [selectedVenue, filterSortState]);

  const renderTabItem = ({ item, index }: { item: any; index: number }) => (
    <Pressable
      style={[
        styles.venueTab,
        selectedVenue === item.venueId ? styles.currentTab : null,
      ]}
      onPress={() => {
        handleTabChanged(item.venueId);
      }}
    >
      <HiraginoKakuText
        style={[
          styles.venueText,
          selectedVenue === item.venueId ? styles.currentText : null,
        ]}
        normal
      >
        {item.name}
      </HiraginoKakuText>
    </Pressable>
  );

  const renderTableItem = ({ item, index }: { item: any; index: number }) => (
    <Pressable
      style={[styles.row, hoveredIndex === index && styles.rowHovered]}
      onPress={() => handleReceptionInfoDetail(item)}
      onHoverIn={() => setHoveredIndex(index)}
      onHoverOut={() => setHoveredIndex(null)}
      onPressIn={() => setHoveredIndex(index)}
      onPressOut={() => setHoveredIndex(null)}
    >
      <View style={[styles.cell, styles.receptionDateTimeContainer]}>
        <HiraginoKakuText style={styles.receptionDateTimeText} normal>
          {item.accepted_timestamp !== null
            ? formatUTCDateTime(item.accepted_timestamp.toString())
            : ""}
        </HiraginoKakuText>
      </View>
      <View style={[styles.cell, styles.receptionMethodContainer]}>
        <HiraginoKakuText style={styles.receptionMethodText} normal>
          {item.short_name}
        </HiraginoKakuText>
      </View>

      <View style={[styles.cell, styles.identificationContainer]}>
        {item.user_rank === "4" ? (
          <View style={styles.identification}>
            <AntDesign
              name="checkcircle"
              size={20}
              color={colors.greenColor}
              style={styles.checkcircleStyle}
            />
          </View>
        ) : (item.lgap_id !== null && item.lgap_id !== "") ? (
          <View style={styles.identification}>
            <AntDesign
              name="checkcircleo"
              size={20}
              color={colors.activeCarouselColor}
              style={styles.checkcircleStyle}
            />
          </View>
        ) : (
          <View style={styles.identification}>
            <HiraginoKakuText style={styles.noneText}>ー</HiraginoKakuText>
          </View>
        )}
      </View>

      <View style={[styles.cell, styles.receptionTargetContainer]}>
        <View
          style={
            item.member_id === 0
              ? item.member_size_per_group === "1"
                ? styles.onlyPersonLabel
                : styles.representativeLabel
              : styles.receptionTogetherLabel
          }
        >
          <HiraginoKakuText
            style={
              item.member_id === 0
                ? item.member_size_per_group === "1"
                  ? styles.onlyPersonLabelText
                  : styles.representativeLabelText
                : styles.receptionTogetherText
            }
          >
            {item.member_id === 0
              ? item.member_size_per_group === "1"
                ? "本人のみ"
                : "受付代表"
              : "一緒に受付"}
          </HiraginoKakuText>
        </View>
      </View>
      <View style={[styles.cell, styles.acceptedPersonContainer]}>
        <HiraginoKakuText style={styles.acceptedPersonText} normal>
          {item.lastname}
          {"　"}
          {item.firstname}
        </HiraginoKakuText>
      </View>
      <View style={[styles.cell, styles.genderContainer]}>
        <HiraginoKakuText style={styles.genderText} normal>
          {item.gender_code === "1"
            ? "男性"
            : item.gender_code === "2"
              ? "女性"
              : "未入力"}
        </HiraginoKakuText>
      </View>
      <View style={[styles.cell, styles.dateOfBirthContainer]}>
        <HiraginoKakuText style={styles.dateOfBirthText} normal>
          {item.date_of_birth !== null
            ? format(new Date(item.date_of_birth), "yyyy/MM/dd") +
            "　(" +
            item.age +
            "歳)"
            : ""}
        </HiraginoKakuText>
      </View>
      <View style={[styles.cell, styles.addressContainer]}>
        <HiraginoKakuText style={styles.addressText} numberOfLines={1} normal>
          {item.address}
        </HiraginoKakuText>
      </View>
    </Pressable>
  );

  const ITEMS_PER_PAGE = 50;
  let totalCount: string = "";
  let newTotalCount: number = 0;
  let totalPages = 0;

  const getPageData = () => {
    let reception = [...receptionResult];
    const startIndex = (page - 1) * ITEMS_PER_PAGE;
    const endIndex = startIndex + ITEMS_PER_PAGE;
    return reception.slice(startIndex, endIndex);
  };

  if (receptionResult == undefined || receptionResult.length == 0) {
    totalCount = "0";
    newTotalCount = 0;
  } else {
    totalCount = receptionResult.length.toLocaleString();
    newTotalCount = receptionResult.length;
    totalPages = Math.ceil(newTotalCount / ITEMS_PER_PAGE);
  }

  const handlePageChange = (pageNum: number) => {
    setPage(pageNum);
  };

  const renderPaginationButtons = () => {
    const maxVisibleButtons = 5;

    const currentPage = page;
    let startPage = 1;
    let endPage = Math.min(totalPages, maxVisibleButtons);

    if (totalPages > maxVisibleButtons) {
      if (currentPage <= Math.floor(maxVisibleButtons / 2) + 1) {
        endPage = maxVisibleButtons;
      } else if (
        currentPage >=
        totalPages - Math.floor(maxVisibleButtons / 2)
      ) {
        startPage = totalPages - maxVisibleButtons + 1;
        endPage = totalPages;
      } else {
        startPage = currentPage - Math.floor(maxVisibleButtons / 2);
        endPage = currentPage + Math.floor(maxVisibleButtons / 2);
      }
    }

    const buttons = [];
    for (let i = startPage; i <= endPage; i++) {
      buttons.push(
        <Pressable
          key={`button-${i}`}
          onPress={() => handlePageChange(i)}
          style={
            i === currentPage ? styles.activeButton : styles.inactiveButton
          }
        >
          <HiraginoKakuText
            style={i === currentPage ? styles.activeText : styles.inactiveText}
          >
            {i}
          </HiraginoKakuText>
        </Pressable>
      );
    }

    if (totalPages > maxVisibleButtons && endPage < totalPages) {
      buttons.push(
        <Pressable
          key={`ellipsis-button`}
          onPress={() => handlePageChange(endPage + 1)}
        >
          <HiraginoKakuText style={styles.inactiveText}>...</HiraginoKakuText>
        </Pressable>
      );
      buttons.push(
        <Pressable
          key={`last-page-button`}
          onPress={() => handlePageChange(totalPages)}
          style={styles.inactiveButton}
        >
          <HiraginoKakuText style={styles.inactiveText}>
            {totalPages}
          </HiraginoKakuText>
        </Pressable>
      );
    }

    return buttons;
  };

  const handleFirstPage = () => {
    setPage(1);
  };

  const handlePrevPage = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };

  const handleNextPage = () => {
    if (page < totalPages) {
      setPage(page + 1);
    }
  };

  const handleLastPage = () => {
    setPage(totalPages);
  };

  const lastPage = Math.ceil(newTotalCount / ITEMS_PER_PAGE);

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        titleName="もどる"
        middleTitleName={クエリ結果_event?.name}
        buttonName=""
        hasButton={false}
        icon={<Entypo name="chevron-left" size={24} color={colors.primary} />}
        iconPosition="front"
        onPressLeft={handleBack}
      ></Header>
      <ScrollView>
        <TouchableWithoutFeedback onPress={handleCloseDropdown}>
          <View>
            <View style={styles.firstParentContainer}>
              {クエリ結果_event?.eventStatusCode === "1" && (
                <View style={styles.warningContainer}>
                  <View style={styles.warningItemContainer}>
                    <Ionicons
                      name="warning"
                      size={24}
                      color={colors.warningColor}
                    />
                    <View style={styles.warningTextContainer}>
                      <HiraginoKakuText style={styles.warningTitle}>
                        準備中のイベントです
                      </HiraginoKakuText>
                      <HiraginoKakuText style={styles.warningText} normal>
                        受付アプリには表示されていません。{"\n"}
                        受付を行うには、受付状況を「受付可能」にしてください。
                      </HiraginoKakuText>
                      <CustomButton
                        text="受付可能にする"
                        onPress={() => {
                          handleStatusChangeEvent("受付可能");
                        }}
                        style={styles.btnMakeAccept}
                        type="ButtonSGray"
                        textWidth={112}
                      />
                    </View>
                  </View>
                </View>
              )}
              <View style={styles.contentsContainer}>
                <View style={styles.btnsContainer}>
                  <View style={styles.eventNameContainer}>
                    <View
                      style={[
                        styles.eventLabel,
                        クエリ結果_event?.eventStatusCode === "3"
                          ? null
                          : クエリ結果_event?.eventStatusCode === "1"
                            ? styles.stillPrepareStyle
                            : クエリ結果_event?.eventStatusCode === "2"
                              ? styles.availableStyle
                              : styles.archiveStyle,
                      ]}
                    >
                      <HiraginoKakuText
                        style={[
                          styles.receptionStatus,
                          クエリ結果_event?.eventStatusCode === "3"
                            ? null
                            : クエリ結果_event?.eventStatusCode === "1"
                              ? { color: colors.warningColor }
                              : { color: colors.secondary },
                        ]}
                      >
                        {クエリ結果_event?.eventStatusCode === "1"
                          ? "準備中"
                          : クエリ結果_event?.eventStatusCode === "2"
                            ? "受付可能"
                            : クエリ結果_event?.eventStatusCode === "3"
                              ? "受付終了"
                              : "アーカイブ"}
                      </HiraginoKakuText>
                    </View>
                    <HiraginoKakuText style={styles.eventTitleText}>
                      {クエリ結果_event?.name}
                    </HiraginoKakuText>
                  </View>

                  <View style={styles.editBtnContainer}>
                    <CustomButton
                      text=""
                      onPress={handleEventEdit}
                      style={styles.pencilIcon}
                      type="ButtonMediumGray"
                      icon={
                        <MaterialCommunityIcons
                          name="pencil"
                          size={16}
                          color="black"
                        />
                      }
                      iconPosition="center"
                    />
                    <CustomButton
                      text=""
                      onPress={handleDropDown}
                      style={styles.moreIcon}
                      type="ButtonMediumGray"
                      icon={
                        <MaterialIcons
                          name="more-horiz"
                          size={16}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="center"
                    />

                    {showDropdown && (
                      <View style={styles.dropdown}>
                        <Pressable
                          onPress={() =>
                            setShowSecondDropdown(!showSecondDropdown)
                          }
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            受付状況を変更
                          </HiraginoKakuText>
                        </Pressable>
                        {menuItemVisible && (
                          <Pressable onPress={handleReproductionEvent}>
                            <HiraginoKakuText
                              normal
                              style={[styles.optionText, styles.bodyText]}
                            >
                              複製
                            </HiraginoKakuText>
                          </Pressable>
                        )}
                        <Pressable onPress={handleDeleteEvent}>
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.deleteBodyText]}
                          >
                            削除
                          </HiraginoKakuText>
                        </Pressable>
                      </View>
                    )}
                    {showSecondDropdown && (
                      <View style={styles.secondDropdown}>
                        {クエリ結果_event?.eventStatusCode !== "1" && (
                          <Pressable
                            onPress={() => handleStatusChangeEvent("準備中")}
                          >
                            <HiraginoKakuText
                              normal
                              style={[styles.optionText, styles.bodyText]}
                            >
                              準備中
                            </HiraginoKakuText>
                          </Pressable>
                        )}
                        {クエリ結果_event?.eventStatusCode !== "2" && (
                          <Pressable
                            onPress={() => handleStatusChangeEvent("受付可能")}
                          >
                            <HiraginoKakuText
                              normal
                              style={[styles.optionText, styles.bodyText]}
                            >
                              受付可能
                            </HiraginoKakuText>
                          </Pressable>
                        )}
                        {クエリ結果_event?.eventStatusCode !== "3" && (
                          <Pressable
                            onPress={() => handleStatusChangeEvent("受付終了")}
                          >
                            <HiraginoKakuText
                              normal
                              style={[styles.optionText, styles.bodyText]}
                            >
                              受付終了
                            </HiraginoKakuText>
                          </Pressable>
                        )}
                        {クエリ結果_event?.eventStatusCode !== "4" && (
                          <Pressable
                            onPress={() =>
                              handleStatusChangeEvent("アーカイブ")
                            }
                          >
                            <HiraginoKakuText
                              normal
                              style={[styles.optionText, styles.bodyText]}
                            >
                              アーカイブ
                            </HiraginoKakuText>
                          </Pressable>
                        )}
                      </View>
                    )}
                  </View>
                </View>
                <View style={styles.showDataContainer}>
                  <View style={styles.eventPeriodContainer}>
                    <HiraginoKakuText style={styles.eventPeriodLabel} normal>
                      イベント期間：
                    </HiraginoKakuText>
                    <HiraginoKakuText style={styles.eventPeriodData} normal>
                      {dateRange}
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.venueContainer}>
                    <HiraginoKakuText style={styles.venueLabel} normal>
                      会場：
                    </HiraginoKakuText>
                    <HiraginoKakuText style={styles.venueData} normal>
                      {venueName}
                    </HiraginoKakuText>
                  </View>
                </View>
              </View>
              <FlatList
                data={クエリ結果_venue}
                renderItem={renderTabItem}
                keyExtractor={(item: any) => String(item.venueId)}
                contentContainerStyle={styles.tabsContainer}
                style={styles.tabFlatList}
              />
            </View>
            <View style={styles.secondParentContainer}>
              <View style={styles.statisticsContainer}>
                <View style={styles.topContainer}>
                  <HiraginoKakuText style={styles.statisticsText}>
                    統計情報
                  </HiraginoKakuText>
                  <CustomButton
                    text="CSVダウンロード"
                    onPress={handleExportToCSV}
                    style={styles.csvDownloadButton}
                    type="ButtonMediumGray"
                    textWidth={133}
                  />
                </View>
                <View style={styles.lineSeparator} />
                <View style={styles.numberOfPeople}>
                  <HiraginoKakuText style={styles.numOfPeopleLabel}>
                    合計人数
                  </HiraginoKakuText>
                  <View style={styles.numOfPeopleContainer}>
                    <HiraginoKakuText style={styles.numOfPeopleValue}>
                      {
                        クエリ結果_reception.filter(
                          (item) =>
                            item.venue_id ===
                            (selectedVenue === 0
                              ? item.venue_id
                              : selectedVenue)
                        ).length
                      }
                    </HiraginoKakuText>
                    <HiraginoKakuText style={styles.hitoLabel}>
                      人
                    </HiraginoKakuText>
                  </View>
                </View>
                {renderPieChart()}
              </View>
              <View style={styles.paginationContainer}>
                <View style={styles.paginationHeaderContainer}>
                  <HiraginoKakuText style={styles.paginationHeaderText}>
                    受付した人
                  </HiraginoKakuText>
                </View>
                <View style={styles.lineSeparator} />
                <View style={styles.dropDownBtnContainer}>
                  <View>
                    <CustomButton
                      text={receptionType}
                      onPress={() => handleFilter("receptionType")}
                      style={styles.dropdownButton}
                      type="ButtonMGray"
                      textWidth={receptionType == "アプリ（自己QR）" ? 137 : 64}
                      icon={
                        <AntDesign
                          name="down"
                          size={14}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="behind"
                      isNormalText
                    />
                    {showReceptionTypeFilter && (
                      <View style={styles.filter}>
                        <Pressable
                          style={
                            receptionType === "受付方法"
                              ? styles.selectedOption
                              : null
                          }
                          onPress={() => filterPress("receptionType", "0")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            全て
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={
                            receptionType === "アプリ（自己QR）"
                              ? styles.selectedOption
                              : null
                          }
                          onPress={() => filterPress("receptionType", "1")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            アプリ（自己QR）
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={
                            receptionType === "手入力"
                              ? styles.selectedOption
                              : null
                          }
                          onPress={() => filterPress("receptionType", "2")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            手入力
                          </HiraginoKakuText>
                        </Pressable>
                      </View>
                    )}
                  </View>
                  <View>
                    <CustomButton
                      text={member}
                      onPress={() => handleFilter("member")}
                      style={styles.dropdownButton}
                      type="ButtonMGray"
                      textWidth={
                        member == "本人のみ受付" || member == "代表して受付"
                          ? 96
                          : member == "一緒に受付"
                            ? 80
                            : 64
                      }
                      icon={
                        <AntDesign
                          name="down"
                          size={14}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="behind"
                      isNormalText
                    />
                    {showMemberFilter && (
                      <View style={styles.filter}>
                        <Pressable
                          style={
                            member === "受付対象" ? styles.selectedOption : null
                          }
                          onPress={() => filterPress("member", "0")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            全て
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={
                            member === "本人のみ受付"
                              ? styles.selectedOption
                              : null
                          }
                          onPress={() => filterPress("member", "1")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            本人のみ受付
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={
                            member === "代表して受付"
                              ? styles.selectedOption
                              : null
                          }
                          onPress={() => filterPress("member", "2")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            代表して受付
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={
                            member === "一緒に受付"
                              ? styles.selectedOption
                              : null
                          }
                          onPress={() => filterPress("member", "3")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            一緒に受付
                          </HiraginoKakuText>
                        </Pressable>
                      </View>
                    )}
                  </View>
                  <View>
                    <CustomButton
                      text={rank}
                      onPress={() => handleFilter("rank")}
                      style={styles.dropdownButton}
                      type="ButtonMGray"
                      textWidth={
                        rank === "本人確認済み"
                          ? 96
                          : rank === "本人未確認"
                            ? 80
                            : 64
                      }
                      icon={
                        <AntDesign
                          name="down"
                          size={14}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="behind"
                      isNormalText
                    />
                    {showRankFilter && (
                      <View style={styles.filter}>
                        <Pressable
                          style={
                            rank === "本人確認" ? styles.selectedOption : null
                          }
                          onPress={() => filterPress("rank", "0")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            全て
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={
                            rank === "本人確認済み"
                              ? styles.selectedOption
                              : null
                          }
                          onPress={() => filterPress("rank", "1")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            本人確認済み
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={
                            rank === "本人未確認" ? styles.selectedOption : null
                          }
                          onPress={() => filterPress("rank", "2")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            本人未確認
                          </HiraginoKakuText>
                        </Pressable>
                      </View>
                    )}
                  </View>
                  <View>
                    <CustomButton
                      text={gender}
                      onPress={() => handleFilter("gender")}
                      style={styles.smallDropdownButton}
                      type="ButtonMGray"
                      textWidth={gender === "未入力" ? 48 : 32}
                      icon={
                        <AntDesign
                          name="down"
                          size={14}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="behind"
                      isNormalText
                    />
                    {showGenderFilter && (
                      <View style={styles.filter}>
                        <Pressable
                          style={
                            gender === "性別" ? styles.selectedOption : null
                          }
                          onPress={() => filterPress("gender", "3")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            全て
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={
                            gender === "男性" ? styles.selectedOption : null
                          }
                          onPress={() => filterPress("gender", "1")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            男性
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={
                            gender === "女性" ? styles.selectedOption : null
                          }
                          onPress={() => filterPress("gender", "2")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            女性
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={
                            gender === "未入力" ? styles.selectedOption : null
                          }
                          onPress={() => filterPress("gender", "0")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            未入力
                          </HiraginoKakuText>
                        </Pressable>
                      </View>
                    )}
                  </View>
                  <View>
                    <CustomButton
                      text={age}
                      onPress={() => handleFilter("age")}
                      style={styles.smallDropdownButton}
                      type="ButtonMGray"
                      textWidth={
                        age === "10歳未満" || age === "80歳以上"
                          ? 69.1
                          : age === "年代"
                            ? 32
                            : 37.1
                      }
                      icon={
                        <AntDesign
                          name="down"
                          size={14}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="behind"
                      isNormalText
                    />
                    {showAgeFilter && (
                      <View style={styles.filter}>
                        <Pressable
                          style={age === "年代" ? styles.selectedOption : null}
                          onPress={() => filterPress("age", "0")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            全て
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={
                            age === "10歳未満" ? styles.selectedOption : null
                          }
                          onPress={() => filterPress("age", "1")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            10歳未満
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={age === "10代" ? styles.selectedOption : null}
                          onPress={() => filterPress("age", "2")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            10代
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={age === "20代" ? styles.selectedOption : null}
                          onPress={() => filterPress("age", "3")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            20代
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={age === "30代" ? styles.selectedOption : null}
                          onPress={() => filterPress("age", "4")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            30代
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={age === "40代" ? styles.selectedOption : null}
                          onPress={() => filterPress("age", "5")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            40代
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={age === "50代" ? styles.selectedOption : null}
                          onPress={() => filterPress("age", "6")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            50代
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={age === "60代" ? styles.selectedOption : null}
                          onPress={() => filterPress("age", "7")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            60代
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={age === "70代" ? styles.selectedOption : null}
                          onPress={() => filterPress("age", "8")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            70代
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          style={
                            age === "80歳以上" ? styles.selectedOption : null
                          }
                          onPress={() => filterPress("age", "9")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            80歳以上
                          </HiraginoKakuText>
                        </Pressable>
                      </View>
                    )}
                  </View>
                </View>
                <View style={styles.topPaginationContainer}>
                  <View style={styles.countContainer}>
                    <HiraginoKakuText style={styles.paginationCount} normal>
                      {page}-{lastPage} / {totalCount} 件中
                    </HiraginoKakuText>
                  </View>
                  <CustomButton
                    text="CSVダウンロード"
                    onPress={handleExportReceptionToCSV}
                    style={styles.csvDownloadButton}
                    type="ButtonMediumGray"
                    textWidth={133}
                  />
                </View>
                <View style={styles.tableContainer}>
                  {/* Render the header */}
                  {renderHeader()}

                  {receptionResult.length === 0 || !loading ? (
                    <View style={styles.noDataContainer}>
                      <View style={styles.noDataChildContainer}>
                        <HiraginoKakuText style={styles.noDataText} normal>
                          {errMsg}
                        </HiraginoKakuText>
                      </View>
                    </View>
                  ) : (
                    <ScrollView style={styles.tableItemContainer}>
                      <View>
                        {getPageData().map((item, index) => (
                          <View key={index}>
                            {renderTableItem({ item, index })}
                          </View>
                        ))}
                      </View>
                    </ScrollView>
                  )}
                </View>
                {receptionResult != undefined &&
                  receptionResult.length > 0 &&
                  loading &&
                  totalPages > 1 && (
                    <View style={styles.onChangePageContainer}>
                      <View style={styles.previousButtonsContainer}>
                        <CustomButton
                          text=""
                          onPress={handleFirstPage}
                          style={
                            page === 1
                              ? styles.skipBackDisable
                              : styles.skipBackEnable
                          }
                          type={page === 1 ? "ButtonSDisable" : "ButtonSGray"}
                          icon={
                            <Feather
                              name="skip-back"
                              size={20}
                              color={colors.greyTextColor}
                            />
                          }
                          iconPosition="center"
                        />
                        <CustomButton
                          text=""
                          onPress={handlePrevPage}
                          style={
                            page === 1
                              ? styles.chevronDisable
                              : styles.chevronEnable
                          }
                          type={page === 1 ? "ButtonSDisable" : "ButtonSGray"}
                          icon={
                            <Feather
                              name="chevron-left"
                              size={20}
                              color={colors.greyTextColor}
                            />
                          }
                          iconPosition="center"
                        />
                      </View>
                      {totalPages > 1 && (
                        <View style={styles.pageNumberContainer}>
                          {renderPaginationButtons()}
                        </View>
                      )}

                      {totalPages > 1 && (
                        <View style={styles.nextButtonsContainer}>
                          <CustomButton
                            text=""
                            onPress={handleNextPage}
                            style={
                              page === lastPage
                                ? styles.chevronDisable
                                : styles.chevronEnable
                            }
                            type={
                              page === lastPage
                                ? "ButtonSDisable"
                                : "ButtonSGray"
                            }
                            icon={
                              <Feather
                                name="chevron-right"
                                size={20}
                                color={colors.textColor}
                              />
                            }
                            iconPosition="center"
                          />
                          <CustomButton
                            text=""
                            onPress={handleLastPage}
                            style={
                              page === lastPage
                                ? styles.skipForwardDisable
                                : styles.skipForwardEnable
                            }
                            type={
                              page === lastPage
                                ? "ButtonSDisable"
                                : "ButtonSGray"
                            }
                            icon={
                              <Feather
                                name="skip-forward"
                                size={20}
                                color={colors.textColor}
                              />
                            }
                            iconPosition="center"
                          />
                        </View>
                      )}
                    </View>
                  )}
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>

      {isStatusChangeModalVisible && (
        <StatusChangeDialog
          statusName={selectedStatusName}
          onCancelButtonPress={handleCancelButton}
          onChangeButtonPress={handleUpdateStatusCode}
        />
      )}
      {isDeleteModalVisible && (
        <EventDeleteDialog
          onCancelButtonPress={handleDeleteCancelButton}
          onDeleteButtonPress={handleDeleteButton}
        />
      )}

      {showStatusToast && (
        <View style={[styles.modalBackground]}>
          <View style={styles.toastMessage}>
            <HiraginoKakuText style={styles.statustoastText}>
              {toastMessage}
            </HiraginoKakuText>
          </View>
        </View>
      )}

      {showToast && (
        <View style={styles.modalBackground}>
          <View style={styles.toastMessage}>
            <HiraginoKakuText style={styles.toastText}>
              {toastMessage}
            </HiraginoKakuText>
          </View>
        </View>
      )}
    </SafeAreaView>
  );
};
