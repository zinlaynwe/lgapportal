import { User } from "../../model/User";

export class EventDetailParams implements Params {
  private _user: User = new User();
  private _eventId: number = 0;
  private _receptionId: number = 0;
  private _eventName: string = "";
  private _eventPeriod: string = "";
  private _toastMsg: string = "";
  private _statusToastMsg: string = "";
  private _editPgType: string = "";

  //getter
  get user(): User {
    return this._user.clone();
  }
  get receptionId(): number {
    return this._receptionId;
  }
  get eventName(): string {
    return this._eventName;
  }
  get eventPeriod(): string {
    return this._eventPeriod;
  }
  get toastMsg(): string {
    return this._toastMsg;
  }
  get statusToastMsg(): string {
    return this._statusToastMsg;
  }
  get eventId(): number {
    return this._eventId;
  }
  get editPgType(): string {
    return this._editPgType;
  }

  //setter
  set user(value: User) {
    this._user = value;
  }
  set eventId(value: number) {
    this._eventId = value;
  }
  set evetName(value: string) {
    this._eventName = value;
  }
  set eventPeriod(value: string) {
    this._eventPeriod = value;
  }
  set toastMsg(value: string) {
    this._toastMsg = value;
  }
  set statusToastMsg(value: string) {
    this._statusToastMsg = value;
  }
  set receptionId(value: number) {
    this._receptionId = value;
  }
  set editPgType(value: string) {
    this._editPgType = value;
  }

  getAllValuesAsString(): string {
    let paramsString = '[ClassName=EventDetailParams';
    paramsString += ',_user=' + this._user.getAllValuesAsString();
    paramsString += ',_eventId=' + this._eventId;
    paramsString += ',_receptionId=' + this._receptionId;
    paramsString += ',_eventName=' + this._eventName;
    paramsString += ',_eventPeriod=' + this._eventPeriod;
    paramsString += ',_toastMsg=' + this._toastMsg;
    paramsString += ',_statusToastMsg=' + this._statusToastMsg;
    paramsString += ',_editPgType=' + this._editPgType;
    paramsString += ']';
    return paramsString
  }
}
