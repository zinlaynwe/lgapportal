import React from "react";
import { StyleSheet, View, Platform } from "react-native";
import { HiraginoKakuText } from "../StyledText";
import { HeadingXxxxSmallBold, ButtonSmallBold } from "../../styles/typography";
import { colors } from "../../styles/color";
import { CustomButton } from "./Button";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

type HeaderProps = {
  titleName?: string;
  middleTitleName?: string;
  buttonName: string;
  buttonWidth?: number;
  icon?: React.ReactNode;
  iconPosition?: "front" | "behind" | "center";
  onPress?: () => void;
  hasButton?: boolean;
  children?: React.ReactNode;
  onPressLeft?: () => void;
};

export const Header = (props: HeaderProps) => {
  const defaultOnPress = () => {};
  const shouldShowButton = props.hasButton !== false;
  const { titleName = "", iconPosition = "center" } = props;

  return (
    <View style={styles.headerContainer}>
      <View style={[styles.frame, styles.leftFrame]}>
        {(props.icon == null || props.iconPosition === "center") && (
          <HiraginoKakuText style={styles.headerText}>
            {props.titleName}
          </HiraginoKakuText>
        )}
        {props.iconPosition === "front" && props.icon != null && (
          <CustomButton
            text={titleName}
            onPress={props.onPressLeft || defaultOnPress}
            style={styles.leftButtonstyle}
            type="ButtonSDefault"
            textSize={ButtonSmallBold.size}
            icon={props.icon}
            iconPosition="front"
          />
        )}
      </View>
      <View style={[styles.frame, styles.middleFrame]}>
        <HiraginoKakuText style={styles.middleFrameText}>
          {props.middleTitleName}
        </HiraginoKakuText>
      </View>
      <View style={[styles.frame, styles.rightFrame]}>
        {props.children ? (
          props.children
        ) : shouldShowButton ? (
          <CustomButton
            text={props.buttonName}
            onPress={props.onPress || defaultOnPress}
            style={styles.buttonStop}
            type="ButtonSGray"
            icon={props.icon}
            iconPosition={"center"}
          />
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    ...Platform.select({
      ios: {
        shadowColor: colors.headerFooterShadowColor,
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 1,
        shadowRadius: 7,
      },
      android: {
        shadowColor: colors.headerFooterShadowColor,
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 1,
        shadowRadius: 7,
        elevation: 6,
      },
      web: {
        boxShadow: "0 1px 7px 0px rgba(8, 18, 26, 0.08)",
      },
    }),
    height: 48,
    backgroundColor: colors.secondary,
    borderWidth: 1,
    borderTopColor: colors.secondary,
    borderRightColor: colors.secondary,
    borderBottomColor: colors.headerFooterShadowColor,
    borderLeftColor: colors.secondary,
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 8,
    paddingHorizontal: 24,
    gap: 16,
  },
  headerText: {
    alignItems: "flex-start",
    justifyContent: "center",
    height: 24,
    fontSize: HeadingXxxxSmallBold.size,
    lineHeight: HeadingXxxxSmallBold.lineHeight,
    color: colors.textColor,
  },
  leftButtonstyle: {
    flexDirection: "row",
    gap: 8,
    paddingVertical: 4,
    paddingHorizontal: 8,
  },
  middleFrameText: {
    height: 24,
    fontSize: HeadingXxxxSmallBold.size,
    lineHeight: HeadingXxxxSmallBold.lineHeight,
    color: colors.textColor,
  },
  buttonStop: {
    width: 96,
    height: 32,
    borderWidth: 1,
    borderColor: colors.gray,
    borderRadius: 4,
    gap: 8,
    paddingVertical: 4,
    fontSize: 16,
  },
  leftFrame: {
    alignItems: "flex-start",
    justifyContent: "center",
  },
  middleFrame: {
    alignItems: "center",
    justifyContent: "center",
  },
  rightFrame: {
    alignItems: "flex-end",
    justifyContent: "center",
  },
  frame: {
    flex: 1,
  },
});
