export const logMode = {
  NORMAL: 0,
  DEVELOP: 1
}
export const logConfig = {
  IS_LOG_OUTPUT: true,
  LOG_MODE: logMode.NORMAL // 0:通常, 1:開発用
}